# Thesis Management Application

A web-based platform for Diploma/Thesis management selection based on the Content Management System Tools
## Requirements for this application
  - Appache server with php 
  - Maria db
  
Set up the application into your server. 
 - Admin panel is located under <your_domain>/admin/index.php
 - Students applicaition is under <your_domain>

In MigrationsScripts folder find and restore **diploma_application_schema.slq** to your server
You can find db credentials in file DBuserCredentials.txt

Then open phpMyAdmin and insert into **usertypes** table these three user types
```sql
INSERT 	INTO 
usertype	(nameType, 
    		colorType) 
VALUES 	('Administrator', '#a7d6b1'),
		('Professor', '#858542'),
		('Supervisor', '#e026e0')
```
After that insert into **users** table the administratior role user 
example 
```sql
INSERT INTO users 
(username, password, photo, nickname, description, user_signature, isActive, type, email) 
VALUES ('admin', 'admin', '6', 'Super-Admin', '<p>Test description 1</p>', '<p>Test Signature</p>', '1', '1', NULL)
```

Go to admin panel and create the users you like

### Email server configuration
- Find **mailSender.php** file into folder source
- Set Up your mail server to be able to send mail after student requests

Comment out **$mail->SMTPOptions** on the production server. Keep it for localy usage (debugging etc)