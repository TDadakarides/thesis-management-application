-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 13 Σεπ 2018 στις 01:23:48
-- Έκδοση διακομιστή: 10.1.25-MariaDB
-- Έκδοση PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `diploma_application`
--
CREATE DATABASE IF NOT EXISTS `diploma_application` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `diploma_application`;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `title` varchar(200) NOT NULL COMMENT 'Ονομασία',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Κατηγορία';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `dynamic_articles`
--

DROP TABLE IF EXISTS `dynamic_articles`;
CREATE TABLE IF NOT EXISTS `dynamic_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `title` varchar(255) NOT NULL COMMENT 'Τίτλος',
  `code` varchar(255) NOT NULL COMMENT 'Κωδικός Άρθρου',
  `html` mediumtext NOT NULL COMMENT 'Html Content',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Δυναμικό Κείμενο';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `keywords`
--

DROP TABLE IF EXISTS `keywords`;
CREATE TABLE IF NOT EXISTS `keywords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `keyword` varchar(50) NOT NULL COMMENT 'Λέξη Κλειδί',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='Λέξεις Κλειδιά';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `keywords_thesis`
--

DROP TABLE IF EXISTS `keywords_thesis`;
CREATE TABLE IF NOT EXISTS `keywords_thesis` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `thesisId` bigint(20) NOT NULL COMMENT 'Id Πτυχιακής',
  `keyword` bigint(20) NOT NULL COMMENT 'Id Keyword',
  PRIMARY KEY (`id`),
  KEY `keyword` (`keyword`),
  KEY `thesisId` (`thesisId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Συσχέτιση Πτυχιακής-Λέξης Κλειδιών  ';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `pictureinfo`
--

DROP TABLE IF EXISTS `pictureinfo`;
CREATE TABLE IF NOT EXISTS `pictureinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `path` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Φωτογραφία',
  `license` bigint(20) DEFAULT NULL COMMENT 'Άδεια',
  `comment` text CHARACTER SET utf8 COMMENT 'Σχόλιο',
  `userid` bigint(20) DEFAULT NULL COMMENT 'User Id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `license` (`license`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COMMENT='Πληροφορίες Φωτογραφιών';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `picturelicense`
--

DROP TABLE IF EXISTS `picturelicense`;
CREATE TABLE IF NOT EXISTS `picturelicense` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `title` varchar(255) NOT NULL COMMENT 'Ονομασία',
  `terms` text COMMENT 'Όροι και Προϋποθέσεις',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Άδειες Φωτογραφιών';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `pictures`
--

DROP TABLE IF EXISTS `pictures`;
CREATE TABLE IF NOT EXISTS `pictures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `tableName` varchar(40) NOT NULL COMMENT 'Πίνακας',
  `itemId` bigint(20) NOT NULL COMMENT 'Κωδικός αντικειμένου',
  `picture` bigint(20) NOT NULL COMMENT 'Φωτογραφία#image',
  `first` int(11) DEFAULT NULL COMMENT 'Αρχική',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`),
  KEY `picture` (`picture`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Φωτογραφίες';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Όνομα',
  `surname` varchar(255) NOT NULL COMMENT 'Επώνυμο',
  `department` varchar(255) NOT NULL COMMENT 'Τμήμα',
  `code` varchar(255) NOT NULL COMMENT 'Αριθμός Μητρώου',
  `email` varchar(40) NOT NULL COMMENT 'Email',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='Σπουδαστές';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `student_comments`
--

DROP TABLE IF EXISTS `student_comments`;
CREATE TABLE IF NOT EXISTS `student_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `thesisId` bigint(20) NOT NULL COMMENT 'Πτυχιακή',
  `studentId` bigint(20) NOT NULL COMMENT 'Σπουδαστής',
  `comment` mediumtext COMMENT 'Σχόλια',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`),
  KEY `studentId` (`studentId`),
  KEY `thesisId` (`thesisId`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COMMENT='Αιτήσεις Σπουδαστών';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `thesis`
--

DROP TABLE IF EXISTS `thesis`;
CREATE TABLE IF NOT EXISTS `thesis` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `title` varchar(255) NOT NULL COMMENT 'Τίτλος',
  `intro` mediumtext COMMENT 'Intro Περιγραφή',
  `description` mediumtext NOT NULL COMMENT 'Περιγραφή',
  `category` bigint(20) NOT NULL COMMENT 'Κατηγορία',
  `professor` bigint(20) NOT NULL COMMENT 'Καθηγητής',
  `minstudent` int(2) NOT NULL COMMENT 'Ελάχιστος αριθμός σπουδαστών',
  `maxstudent` int(11) NOT NULL COMMENT 'Μέγιστος αριθμός σπουδαστών',
  `datePublished` datetime DEFAULT NULL COMMENT 'Ημερομηνία δημοσίευσης',
  `dateStart` datetime DEFAULT NULL COMMENT 'Ημερομηνία ανάθεσης',
  `datePresentation` datetime DEFAULT NULL COMMENT 'Ημερομηνία παρουσίασης',
  `dateDelivery` datetime DEFAULT NULL COMMENT 'Ημερομηνία παράδοσης',
  `has_congres` tinyint(1) DEFAULT '0' COMMENT 'Παρουσίαση σε συνέδριο',
  `congres_title` varchar(255) DEFAULT NULL COMMENT 'Επιθυμητή Σύνδεση Με Δημοσίευση',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας επεξεργασίας',
  `isActive` tinyint(1) NOT NULL COMMENT 'Ενεργό',
  `isCompleted` tinyint(1) NOT NULL COMMENT 'Ολοκληρωμένη Πτυχιακή',
  `abstractEL` longtext NOT NULL COMMENT 'Abstract EL',
  `abstractEN` longtext NOT NULL COMMENT 'Abstract EN',
  `contactPerson` longtext COMMENT 'Contact Person',
  `thesisDbReference` longtext COMMENT 'Σύνδεσμος Ολοκληρωμένης Πτυχιακής',
  `assignedTo` longtext COMMENT 'Ανατέθηκε στους',
  `attachmentId` bigint(20) DEFAULT NULL COMMENT 'Συνημμένο',
  PRIMARY KEY (`id`),
  KEY `professor` (`professor`),
  KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='Πτυχιακές';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `thesis_comments`
--

DROP TABLE IF EXISTS `thesis_comments`;
CREATE TABLE IF NOT EXISTS `thesis_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `thesisId` bigint(20) NOT NULL COMMENT 'Πτυχιακή',
  `studentId` bigint(20) DEFAULT NULL COMMENT 'Σπουδαστής',
  `comment` mediumtext NOT NULL COMMENT 'Σχόλια',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ενεργό',
  PRIMARY KEY (`id`),
  KEY `studentId` (`studentId`),
  KEY `thesisId` (`thesisId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='Σχόλια Πτυχιακής';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) NOT NULL COMMENT 'Όνομα',
  `password` varchar(255) NOT NULL COMMENT 'Κωδικός χρήστη #password',
  `photo` bigint(20) DEFAULT NULL COMMENT 'Φωτογραφία#image',
  `nickname` varchar(255) DEFAULT NULL COMMENT 'Ψευδώνυμο',
  `description` mediumtext COMMENT 'Περιγραφή',
  `user_signature` mediumtext COMMENT 'Υπογραφή',
  `isActive` tinyint(4) NOT NULL COMMENT 'Ενεργό',
  `type` bigint(20) DEFAULT NULL COMMENT 'Τύπος',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-Mail',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `photo` (`photo`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='Διαχειριστές';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `usertype`
--

DROP TABLE IF EXISTS `usertype`;
CREATE TABLE IF NOT EXISTS `usertype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός',
  `nameType` varchar(45) NOT NULL COMMENT 'Ονομασία',
  `colorType` varchar(45) DEFAULT NULL COMMENT 'Χρώμα#color',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='User Types';

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `user_supervisor`
--

DROP TABLE IF EXISTS `user_supervisor`;
CREATE TABLE IF NOT EXISTS `user_supervisor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userid` bigint(20) NOT NULL COMMENT 'user id',
  `supervisorid` bigint(20) NOT NULL COMMENT 'Supervisor Id',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `supervisorid` (`supervisorid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Περιορισμοί για άχρηστους πίνακες
--

--
-- Περιορισμοί για πίνακα `keywords_thesis`
--
ALTER TABLE `keywords_thesis`
  ADD CONSTRAINT `keywords_thesis_ibfk_1` FOREIGN KEY (`keyword`) REFERENCES `keywords` (`id`),
  ADD CONSTRAINT `keywords_thesis_ibfk_2` FOREIGN KEY (`thesisId`) REFERENCES `thesis` (`id`);

--
-- Περιορισμοί για πίνακα `pictureinfo`
--
ALTER TABLE `pictureinfo`
  ADD CONSTRAINT `pictureInfo_ibfk_1` FOREIGN KEY (`license`) REFERENCES `picturelicense` (`id`);

--
-- Περιορισμοί για πίνακα `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`picture`) REFERENCES `pictureinfo` (`id`);

--
-- Περιορισμοί για πίνακα `student_comments`
--
ALTER TABLE `student_comments`
  ADD CONSTRAINT `student_comments_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `student_comments_ibfk_2` FOREIGN KEY (`thesisId`) REFERENCES `thesis` (`id`);

--
-- Περιορισμοί για πίνακα `thesis`
--
ALTER TABLE `thesis`
  ADD CONSTRAINT `thesis_ibfk_1` FOREIGN KEY (`category`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `thesis_ibfk_2` FOREIGN KEY (`professor`) REFERENCES `users` (`id`);

--
-- Περιορισμοί για πίνακα `thesis_comments`
--
ALTER TABLE `thesis_comments`
  ADD CONSTRAINT `thesis_comments_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `thesis_comments_ibfk_2` FOREIGN KEY (`thesisId`) REFERENCES `thesis` (`id`);

--
-- Περιορισμοί για πίνακα `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_picture_fk` FOREIGN KEY (`photo`) REFERENCES `pictureinfo` (`id`),
  ADD CONSTRAINT `users_usertype_fk` FOREIGN KEY (`type`) REFERENCES `usertype` (`id`);

--
-- Περιορισμοί για πίνακα `user_supervisor`
--
ALTER TABLE `user_supervisor`
  ADD CONSTRAINT `user_supervisor_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_supervisor_ibfk_2` FOREIGN KEY (`supervisorid`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
