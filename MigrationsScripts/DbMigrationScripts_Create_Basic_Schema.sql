-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: db59.grserver.gr:3306
-- Generation Time: Oct 14, 2017 at 03:44 PM
-- Server version: 10.2.8-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+02:00";



--
-- Database: `toufesfc`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `title` varchar(200) NOT NULL COMMENT 'Ονομασία',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Κατηγορία';

-- --------------------------------------------------------

--
-- Table structure for table `thesis`
--

CREATE TABLE `thesis` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `title` varchar(255) NOT NULL COMMENT 'Τίτλος',
  `description` mediumtext NOT NULL COMMENT 'Περιγραφή',
  `category` bigint(20) NOT NULL COMMENT 'Κατηγορία',
  `professor` bigint(20) NOT NULL COMMENT 'Καθηγητής',
  `minstudent` int(2) NOT NULL COMMENT 'Ελάχιστος αριθμός σπουδαστών',
  `maxstudent` int(11) NOT NULL COMMENT 'Μέγιστος αριθμός σπουδαστών',
  `datePublished` datetime DEFAULT NULL COMMENT 'Ημερομηνία δημοσίευσης',
  `dateStart` datetime DEFAULT NULL COMMENT 'Ημερομηνία ανάθεσης',
  `datePresentation` datetime DEFAULT NULL COMMENT 'Ημερομηνία παρουσίασης',
  `dateDelivery` datetime DEFAULT NULL COMMENT 'Ημερομηνία παράδοσης',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας επεξεργασίας',
  `isActive` tinyint(1) NOT NULL COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Πτυχιακές';

--
-- Table structure for table `pictureinfo`
--

CREATE TABLE `pictureinfo` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `path` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Φωτογραφία',
  `license` bigint(20) DEFAULT NULL COMMENT 'Άδεια',
  `comment` text CHARACTER SET utf8 DEFAULT NULL COMMENT 'Σχόλιο'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Πληροφορίες Φωτογραφιών';


-- --------------------------------------------------------

--
-- Table structure for table `picturelicense`
--

CREATE TABLE `picturelicense` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `title` varchar(255) NOT NULL COMMENT 'Ονομασία',
  `terms` text DEFAULT NULL COMMENT 'Όροι και Προϋποθέσεις',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Άδειες Φωτογραφιών';

--
-- Dumping data for table `picturelicense`
--

INSERT INTO `picturelicense` (`id`, `title`, `terms`, `dateCreate`, `dateUpdate`, `isActive`) VALUES
(3, 'Free', 'Free...', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(9, 'Google', 'test', '2016-07-01 00:34:41', '2016-06-30 18:34:41', 1),
(10, 'Wikipedia', NULL, '2016-11-05 21:45:49', '2016-11-05 17:45:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `tableName` varchar(40) NOT NULL COMMENT 'Πίνακας',
  `itemId` bigint(20) NOT NULL COMMENT 'Κωδικός αντικειμένου',
  `picture` bigint(20) NOT NULL COMMENT 'Φωτογραφία#image',
  `first` int(11) DEFAULT NULL COMMENT 'Αρχική',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Φωτογραφίες';

-- --------------------------------------------------------

--
-- Table structure for table `student_comments`
--

CREATE TABLE `student_comments` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `thesisId` bigint(20) NOT NULL COMMENT 'Πτυχιακή',
  `studentId` bigint(20) NOT NULL COMMENT 'Σπουδαστής',
  `comment` mediumtext NULL COMMENT 'Σχόλια',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Σχόλια Σπουδαστών#disabled';

-- --------------------------------------------------------

--
-- Table structure for table `thesis_comments`
--

CREATE TABLE `thesis_comments` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `thesisId` bigint(20) NOT NULL COMMENT 'Πτυχιακή',
  `studentId` bigint(20) NULL COMMENT 'Σπουδαστής',
  `comment` mediumtext NOT NULL COMMENT 'Σχόλια',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Σχόλια Πτυχιακής';

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Όνομα',
  `surname` varchar(255) NOT NULL COMMENT 'Επώνυμο',
  `department` varchar(255) NOT NULL COMMENT 'Τμήμα',
  `code` varchar(255) NOT NULL COMMENT 'Αριθμός Μητρώου',
  `dateCreate` datetime NOT NULL COMMENT 'Ημερομηνία δημιουργίας',
  `dateUpdate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ημερομηνία τελευταίας ενημέρωσης',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Ενεργό'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Σπουδαστές';

-- --------------------------------------------------------
--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `username` varchar(255) NOT NULL COMMENT 'Όνομα',
  `password` varchar(255) NOT NULL COMMENT 'Κωδικός χρήστη #password',
  `photo` bigint(20) DEFAULT NULL COMMENT 'Φωτογραφία#image',
  `nickname` varchar(255) DEFAULT NULL COMMENT 'Ψευδώνυμο',
  `description` mediumtext DEFAULT NULL COMMENT 'Περιγραφή',
  `user_signature` mediumtext DEFAULT NULL COMMENT 'Υπογραφή',
  `isActive` tinyint(4) NOT NULL COMMENT 'Ενεργό',
  `type` bigint(20) DEFAULT NULL COMMENT 'Τύπος'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Διαχειριστές';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `photo`, `nickname`,`description`, `user_signature`, `isActive`, `type`) VALUES
(1, 'admin', 'admin', null, 'troufas', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `id` bigint(20) NOT NULL COMMENT 'Κωδικός',
  `nameType` varchar(45) NOT NULL COMMENT 'Ονομασία',
  `colorType` varchar(45) DEFAULT NULL COMMENT 'Χρώμα#color'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User Types';

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`id`, `nameType`, `colorType`) VALUES
(1, 'Administrator', '#00ff37'),
(2, 'Professor', '#FF0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `thesis`
--
ALTER TABLE `thesis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professor` (`professor`);

--
-- Indexes for table `pictureinfo`
--
ALTER TABLE `pictureinfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `license` (`license`);

--
-- Indexes for table `picturelicense`
--
ALTER TABLE `picturelicense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `picture` (`picture`);

--
-- Indexes for table `players`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `photo` (`photo`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pictureinfo`
--
ALTER TABLE `pictureinfo`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός', AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Κωδικός', AUTO_INCREMENT=3;

--
-- Constraints for table `pictureinfo`
--
ALTER TABLE `pictureinfo`
  ADD CONSTRAINT `pictureInfo_ibfk_1` FOREIGN KEY (`license`) REFERENCES `picturelicense` (`id`);

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`picture`) REFERENCES `pictureinfo` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_picture_fk` FOREIGN KEY (`photo`) REFERENCES `pictureinfo` (`id`),
  ADD CONSTRAINT `users_usertype_fk` FOREIGN KEY (`type`) REFERENCES `usertype` (`id`);


