<nav class="navbar navbar-expand-lg navbar-dark my-navbar">
  <a class="navbar-brand" href="/index.php">University Of West Attica</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/index.php">Home<span class="sr-only"></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/search.php">Find your thesis</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/requestthesis.php">Request For Thesis</a>
      </li>
    </ul>
  </div>
</nav>