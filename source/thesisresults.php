<?php 
	include "admin/database_include.php";

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$keywords = isset($_POST["keyword-ids"]) ? $_POST["keyword-ids"] : "" ;
		$category = isset($_POST["category-ids"] ) ? $_POST["category-ids"] : "" ;
		$thesis_fields= ""
					."SELECT DISTINCT	t.*
							FROM thesis t
							INNER JOIN keywords_thesis kt ON t.id = kt.thesisId and t.isActive=1
							INNER JOIN keywords k ON k.id = kt.keyword
							INNER JOIN category c ON c.id = t.category " ;
		$whereClause=""
					."";
		if (strlen($category)>0 && strlen($keywords)==0) {
				$whereClause="WHERE c.id IN (".$category.")" ;

		}elseif (strlen($keywords)>0 && strlen($category)==0) {
			$whereClause="WHERE k.id IN (".$keywords.") " ;
		}elseif (strlen($keywords)>0 && strlen($category)>0) {
			$whereClause="WHERE c.id IN (".$category.") AND k.id IN (".$keywords.")";
		}

		$thesis_results = mysql_query($thesis_fields . $whereClause);
	}

	if (!$thesis_results)
	{
		echo 
			"<div >"
				."<label class=''>Try another combination</label>"
			."</div>";
	}else{
		echo '<div class="list-group">';
		while($row = mysql_fetch_assoc($thesis_results)){
		echo 
		  '<a href="/thesisDetails.php?thesisid='.$row['id'].'" class="list-group-item list-group-item-action flex-column align-items-start">'
		    .'<div class="d-flex w-100 justify-content-between">'
		     . '<h5 class="mb-1">'.$row['title'].'</h5>'
		    .'</div>'
		    .'<p class="mb-1">'.$row['intro'].'</p>'
		    .'<small>Click here for more info..</small>'
		  .'</a>';
		}
		echo "</div>";
	}
?>