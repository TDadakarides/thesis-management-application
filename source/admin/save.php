<?php
include "members.php";
include "fkInit.php";
function isValidURL($url)
{
	if(preg_match('#http\:\/\/[aA-zZ0-9\.]+\.[aA-zZ\.]+#',$url)) return true;
	else return false;
}
function urlMyDecode($url)
{
	if(isValidURL(urldecode($url)))
	{	
		if (strpos($url, "?")==TRUE)
		{
			$parameters = substr(strrchr($url, "?"), 1);
			$domain = substr($url,0,strpos($url, "?"));
			$newUrl=urldecode($domain)."?".$parameters;
		}
		else
		{
			$newUrl=urldecode($url);
		}
		$url=$newUrl;
	}	
	return $url;
}
$lastInsertedId="NULL";
$lastInsertedTranslate=array();
//echo json_encode($_POST);
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
date_default_timezone_set('Europe/Athens');
	$allowedTags='<a><p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
	$allowedTags.='<li><ol><ul><span><br><ins><del><iframe><tr><td><table><tbody>'; 
	$table=$_POST['table'];
	include	'database_include.php';
	$now=date('Y-m-d H:m:s');
	$exp=date('Y-m-d H:m:s', strtotime("+1 years"));

    //////////Βρίσκουμε τα foreign keys αν υπάρχουν////////
    $sql = "select table_name,column_name,constraint_name,referenced_table_name,referenced_column_name from information_schema.key_column_usage where table_name='".$table."' and referenced_column_name<>'';"; //διαλέγουμε πίνακα
    $results_foreign_key = mysql_query($sql);
    $i=0;
    $fk=array();///ΟΛΑ ΤΑ ΔΕΥΤΕΡΕΥΟΝΤΑ ΚΛΕΙΔΙΑ
    /////ΕΑΝ ΥΠΑΡΧΟΥΝ FOREIGN KEYS ΤΑ ΑΠΟΘΗΚΕΥΟΥΜΕ ΣΕ ΠΙΝΑΚΕΣ////////////////
    while($row = mysql_fetch_assoc($results_foreign_key))
    {
        // $fk["table_name"][$i]=$row[0]; //αποθηκεύουμε στο fk["table_name"] το ονομα του πινακα που ειμαστε
        // $fk["column_name"][$i]=$row[1]; //αποθηκεύουμε στο fk["column_name"] το ονομα του πεδιου που σχετιζεται
        // $fk["referenced_table_name"][$i]=$row[3]; //αποθηκεύουμε στο fk["referenced_table_name"] το ονομα του σχετιζόμενου πινακα 
        // $fk["referenced_column_name"][$i]=$row[4];//αποθηκεύουμε στο fk["referenced_table_name"] το ονομα του σχετιζόμενου πινακα 
        // $i++;
        $fk[$row["column_name"]]=$row;
    }
	//echo "<div id='title' align='center' style='display:none;'>{$table}</div>";
	//echo "<div id='title_comment' align='center'>{$table_comment}</div>";
	//Δυναμική εμφάνιση επικεφαλίδων πεδίων
	$results=array();
	$sql = "SHOW FULL COLUMNS FROM `".$table."`"; //διαλέγουμε πίνακα
	$result = mysql_query($sql);
	//print_r($result);
	$i=0;
	$values="(";
	//print_r($_POST);
	$fields=array();
	while($row = mysql_fetch_assoc($result))
	{
		
		$fieldName=$row["Field"];
		$thisFieldInfo=$row;
		$isMediumText=($thisFieldInfo["Type"]=="mediumtext");
		$isWord=strpos($thisFieldInfo["Type"],"varchar")!== false;
		$isText=$thisFieldInfo["Type"]=="text";
		$isInt=strpos($thisFieldInfo["Type"],"int")!== false;
		$isTinyInt=strpos($thisFieldInfo["Type"],"tinyint")!== false;
		$isFloat=strpos($thisFieldInfo["Type"],"float")!== false;
		$isPhoto=strpos($thisFieldInfo["Comment"],"Φωτο")!== false;
		$isCountry=strpos($thisFieldInfo["Comment"],"Χώρα")!== false;
		$isLocation=strpos($thisFieldInfo["Comment"],"Τοποθεσία")!== false;
		$isColor=strpos($thisFieldInfo["Comment"],"(#)")!== false;
		$isDate=strpos($thisFieldInfo["Type"],"datetime")!== false;
		$isTime=strpos($thisFieldInfo["Type"],"timestamp")!== false;
		$isSet=strpos($thisFieldInfo["Type"],"set")!== false;
		$isPK=$thisFieldInfo["Key"]=="PRI"?true:false;
        $isFK=isset($fk[$fieldName])?true:false;
        $isFKPopUp=$isFK?
            in_array($fk[$fieldName]["referenced_table_name"], $translatorTables)?true:false
            :false;
		
		//print_r($row);
		if ($fieldName=="dateUpdate" || $fieldName=="dateCreate") $_POST[$fieldName]=date('Y-m-d H:i:s');
		if ( $isPK ) $pkName=$fieldName;
		if ( (isset($_POST[$fieldName])) && (!$isPK)  )
		{
			$fieldValue=$_POST[$fieldName];
			$fieldValue=str_replace("'", "''", $fieldValue );
			//$fields=$fields.$fieldName.", ";
			array_push($fields,$fieldName);
            if($isFKPopUp){
                ////save title
                $translate_field=array("code","isActive","refTableName","refTableField","refPKValue","dateCreate");
                $translate_values=array("'NULL'","'1'","'{$table}'","'{$fieldName}'","".$lastInsertedId."","'".date('Y-m-d H:m:s')."'");
                foreach($_POST[$fieldName] as $language=>$title){
                    array_push($translate_field,$language);
                    array_push($translate_values,"'".$title."'");
                }
                $sql_translate="INSERT INTO {$fk[$fieldName]["referenced_table_name"]} ( ".(implode(",", $translate_field))." ) VALUES ( ".(implode(",", $translate_values))." )";
//                    echo "<p>".$sql_translate."</p>";
//                $lastInsertedTranslate[$fieldName]=43335;
//                $values=$values."'".$lastInsertedTranslate[$fieldName]."', ";
                if (mysql_query($sql_translate,$con))
                {
                    $lastInsertedTranslate[$fieldName]=mysql_insert_id();
                    $values=$values."'".$lastInsertedTranslate[$fieldName]."', ";
                    $hasSaveTitle=true;
                }
                else
                {
                //    echo json_encode(array("error"=>"Πρόβλημα εγγραφής.")); 
                        echo json_encode(array("error"=>"Πρόβλημα εγγραφής. ".mysql_errno().": ".mysql_error()." | When executing:$sql_translate"));
                    exit;
                }
            }
			else if ($isDate || $isTime) 
			{
				if ( $fieldValue=="" || $fieldValue==null){ 
					$values=$values."NULL, "; 
				}
				else{
					$values=$values."'".date('Y-m-d H:i:s', strtotime($fieldValue))."', ";
				}
			}
			else if ( $isMediumText )
            {
            ///φτιάξιμο εικόνων
                $html=$fieldValue;
                if ($html!='')
                {
                    $html = strip_tags(str_replace("'","''",$fieldValue),$allowedTags);
                    $doc = new DOMDocument();
                    //$doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
                    $doc->loadHTML(($html));
                    $domImg = $doc->getElementsByTagName("img"); 
                    foreach ($domImg as $img) 
                    { 
                        if (!$img->getAttribute('height')) $img->setAttribute('height','auto');
                        if (!$img->getAttribute('width')) $img->setAttribute('width','auto');
                        if ((strpos($img->getAttribute('src'),'http://'))=== false)
                        {
                            $img->setAttribute('src','http://thesis.gr/data_interface/'.$img->getAttribute('src'));
                        }
                        list($real_width, $real_height, $type, $attr) = getimagesize($img->getAttribute('src'));
                        $img->removeAttribute('style');
                        $width=$img->getAttribute('width');
                        $height=$img->getAttribute('height');
                        if ((($height=='0')&&($width=='0'))||(($height=='')&&($width==''))) {$width=$real_width;$height=$real_height;};
                        if ((($height=='auto')&&($width!='auto'))||(($height=='0')&&($width!='auto'))||(($height==null)&&($width!='auto'))) {$height=($width/$real_width)*$real_height;};
                        if ((($height!='auto')&&($width=='auto'))||(($height!='auto')&&($width=='0'))||(($height!='auto')&&($width==null))) {$width=($height/$real_height)*$real_width;};
                        $img->setAttribute('width',$width);
                        $img->setAttribute('height',$height);

                        $html= $doc->saveHTML();
                    }
                    $domA = $doc->getElementsByTagName("a"); 
                    foreach ($domA as $aTag) 
                    {
                        $aTagHref=urldecode($aTag->getAttribute('href'));
                        //echo "<br/>find:".$aTagHref." - ".((strpos($aTagHref,'../'))== 0)."<br/>";
                        if ((strpos($aTagHref,'../'))== 0)
                        {
                            $aTagHref=str_replace("../","http://thesis.gr/",$aTagHref);
                            //echo "aTagHref:{$aTagHref}<br/>";
                            $aTag->setAttribute('href',$aTagHref);
                        }
                        $html= $doc->saveHTML();
                    }
                }
                $values=$values." '".$html."', ";
            }
            else
            {
                $fieldValue=urlMyDecode( $fieldValue );
                $fieldValue = ( $fieldValue )!='' ? "'".$fieldValue."'" :"NULL";
                $values=$values.$fieldValue.", ";
            }
		}
		$i++;
	}
	$sqlAutoIncrement = "SHOW TABLE STATUS LIKE '".$table."' ;"; //διαλέγουμε πίνακα
	$resultAutoIncrement = mysql_query($sqlAutoIncrement);
	$rowAutoIncrement=mysql_fetch_assoc($resultAutoIncrement);
	$valuePK=$rowAutoIncrement['Auto_increment'];
	$fieldsStr="(".implode(", ",$fields).")";
	$values=substr($values,0,(strlen($values)-2)).")";
	$sql="INSERT INTO {$table} {$fieldsStr} VALUES {$values}";
//    echo "<p>".$sql."</p>";
	if (mysql_query($sql,$con))
	{
		$refField=isset($fkViewValues[$table])?$_POST[$fkViewValues[$table]]:"";
		$lastInsertedId=mysql_insert_id();
		echo json_encode(array("success"=>"Success updating",
			"new_id"=>$lastInsertedId,
			"reference_field"=>$refField  )); 
	}
	else
	{
		echo json_encode(array("error"=>"MySQL error ".mysql_errno().": ".mysql_error()." | When executing:$sql")); 
	}
	////save - update translator tables
	$sql_fk_translate = "SELECT table_name,column_name,constraint_name,referenced_table_name,referenced_column_name "
	."FROM information_schema.key_column_usage "
	."WHERE table_name='".$table."' "
	."AND referenced_table_name in ('".(implode("','", $translatorTables))."') "
	."AND referenced_column_name<>'';";
	$results_fk_tranlsate = mysql_query($sql_fk_translate);
	while($row_fk_translate = mysql_fetch_assoc($results_fk_tranlsate))
	{
		$sql_update_translate="UPDATE {$row_fk_translate["referenced_table_name"]} "
		."SET code='{$table}_{$lastInsertedId}_{$row_fk_translate["column_name"]}' ,"
		."  refTableName='{$table}' ,"
		." 	refTableField='{$row_fk_translate["column_name"]}' ,"
		." 	refPKValue={$lastInsertedId} "
		."WHERE id = {$lastInsertedTranslate[$row_fk_translate["column_name"]]} ;";
//		echo "<p>{$sql_update_translate}</p>";
		mysql_query($sql_update_translate,$con);
	}
	
	mysql_close($con);

?>