<?php
	include 'serverhost.php';
	include 'functions_of.php';
	if ($_SERVER["DOCUMENT_ROOT"]=="") $_SERVER["DOCUMENT_ROOT"]=".";
	$errors=0;
	$filename=$_FILES['file']['name'];
	//if it is not empty
	print_r($_FILES['file']);
	$acceptedFiles=array();
	$acceptedFiles= (isset($_GET['acceptedFiles']))?explode(',', str_replace(' ', '', $_GET['acceptedFiles']) ):explode(',','.png,.jpg,.bmp');
	$maxFilesize= (isset($_GET['maxFilesize']))?($_GET['maxFilesize'])*1024:(2048);
	$dir= (isset($_GET['dir']))?($_GET['dir']):"{$siteRoot}/data_interface/images/";
	$createThumbs= (isset($_GET['createThumbs']))?($_GET['createThumbs']):true;

	echo "<p>check</p>";
	if ( $filename!='' ) 
	{
		if (isset($_GET['newname'])) $filename=$_GET['newname'];
		//get the original name of the file from the clients machine
		$filename = stripslashes($filename);
		//get the extension of the file in a lower case format
		$extension = getExtension($filename);
		$extension = strtolower($extension);
		echo "<p>extension:$extension</p>";
		//if it is not a known extension, we will suppose it is an error and will not  upload the file,  
		//otherwise we will do more tests
		if (! in_array('.'.$extension, $acceptedFiles) )
		{
			echo "<p>Invalid file type!</p>";
			$errors=1;
		}
		else
		{
			echo "<p>ok</p>";
			if  ((in_array('.'.$extension, explode(',','.png,.jpg,.bmp')) ) &&($createThumbs==true) ) {$createThumbs=true;} else {$createThumbs=false;} 
			//get the size of the image in bytes
			//$_FILES['image']['tmp_name'] is the temporary filename of the file
			//in which the uploaded file was stored on the server
			$size=filesize($_FILES['file']['tmp_name']);
			echo "<p>file size:{$size}</p>";
			//compare the size with the maxim size we defined and print error if bigger
			if ($size > MAX_SIZE * 50000)
			{
				echo "<p>Invalid file size</p>";
				$errors=1;
			}
			//we will give an unique name, for example the time in unix time format
			$just_filename=getFilename($filename);//ΒILL
			$image_name=$just_filename.'.'.$extension;//ΒILL
			if (!file_exists($dir)) {
				mkdir($dir, 0777, true);
			}			
			$fullname="{$dir}{$image_name}";
			$copied = copy($_FILES['file']['tmp_name'], $fullname);
			echo "<p>Create:{$fullname}</p>";
			if (!$copied) 
			{
				echo "<p>Copy problem!</p>";
				$errors=1;
			}
			else
			{
				if ($createThumbs)
				{
					
					$newdir=str_replace('/images/','/images_thumbs/',$dir);
					$newfullname="{$newdir}{$image_name}";
					if (!file_exists($newdir)) {
						mkdir($newdir, 0777, true);
					}
					if (create_thumb($fullname,$newfullname,150)) echo "<p>createThumbs:{$newfullname}</p>"; else echo "<p>Error createThumbs:{$newfullname}</p>";
				}
			}
		}
	}
?>