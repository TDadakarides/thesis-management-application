<?php
include "members.php";
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Δημιουργία</div>
			<?php
                $isUser=1;
				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$referenceTable=isset($_POST['ref_table'])?$_POST['ref_table']:"";
				$referenceField=isset($_POST['ref_table_field'])?$_POST['ref_table_field']:"";
				$tableClass='';
				echo "<div id='title' style='display:none;'>{$table}</div>";
				echo "<div class='titlePage' ><p>{$table_comment}</p></div>";
				echo "<form action='save.php' method='post' id='form1' name='form1'>";
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; 
				$result = mysql_query($sql);
				$fieldInfo=array();
				$allFields=array();
				while($row = mysql_fetch_assoc($result))
				{
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}
				$sql_usertypes= ""
					."SELECT
						k.id,
						k.nameType,
						k.colorType
						from usertype k";


				 $photoDirectory="";
                                 $photoPath="";
                                    $sql_photo="SELECT path FROM pictureinfo"; //WHERE id={$foreignValue};";
                                    $result_photo=mysql_query($sql_photo);
                                    if ($row_photo=mysql_fetch_assoc($result_photo)){
                                        $photoPath=$row_photo["path"];
                                    }

				//$results_fields = mysql_query($sql_fields);
				$results_usertypes = mysql_query($sql_usertypes);
				$sql = "select table_name,column_name,constraint_name,referenced_table_name,referenced_column_name from information_schema.key_column_usage where table_name='".$table."' and referenced_column_name<>'';"; //διαλέγουμε πίνακα
				$results_foreign_key = mysql_query($sql);
				$i=0;
				$fk=array();
				while($row = mysql_fetch_assoc($results_foreign_key))
				{
					$fk[$row["column_name"]]=$row;
				}
				echo ""
					.
				   "<table align='center' class='table' cellpadding='0' cellspacing='0'>
					         <tbody>
					         	<tr style='display:none;'>
					               <td>
					               <span class='field-name'>Κωδικός*</span>
					               </td>
					               <td class='data_input' style='display:none;'><input  id='id' name='id' type='text' value='' class='max input'>
					               </td>
				            	</tr>
				            	<tr>
					               <td>
					               <span class='field-name'>Όνομα*</span>
					               </td>
					               <td class='data_input'><input data-required id='username' name='username' type='text' value='' class='max input'>
					               </td>
					            </tr>
					            <tr>
									<td>
										<span class='field-name'>Κωδικός χρήστη *</span>
									</td>
									<td class='data_input'>
										<input data-required type='password' id='password' name='password' class='max input' value='' />
									</td>
								</tr>
					            <tr>
									<td>
										<span class='field-name'>Φωτογραφία</span>
									</td>
									<td class='data_input'>
									<input class='max photo' value='1' id='photo' name='photo' type='text'>
										<div class='photoBox'>
											<div id='photo_pic' class='upload_image dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='photo' 
											data-upload-to-element-current-value='' 
											data-upload-to-element-what='background' 
											data-upload-accepted-files='.png,.jpg,.bmp' 
											data-upload-url='uploadFile.php' 
											data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
											data-upload-dirserver='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
											data-upload-create-thumbs='true' 
											data-upload-maxfilesize='2' 
											style=\"background-image:url('".$siteHost.$uploadHost.$imageHost["normal"].$photoPath."');\"></div>
												<div data-picture-id='photo_pic' data-input-id='photo' class='clearButton clear_pic dropzoneGeneralButton'>X</div>
										</div>
										<div style='clear:both'></div>
										</td>
									</tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ψευδώνυμο*</span>
					               </td>
					               <td class='data_input'><input data-required id='nickname' name='nickname' type='text' value='' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>E-Mail</span>
					               </td>
					               <td class='data_input'><input id='email' name='email' type='text' value='' class='max input'>
					               </td>
					            </tr>
					            <tr>
								<td colspan='2'>
								<span class='field-name'>Περιγραφή</span>
								<p></p>
								 	<textarea  style='float:right;' class='mceEditor tinymce' id='description' name='description'  ></textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='description'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='description'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Υπογραφή</span>
								<p></p>
								 	<textarea  style='float:right;' class='mceEditor tinymce' id='user_signature' name='user_signature'></textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='user_signature'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='user_signature'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
									<td>
									<span class='field-name'>Τύπος*</span>
									</td>
									<td class='data_input'>
									<input data-required class='max foreign_value' id='type' name='type' type='text' value=''>
									<div class='foreignBox' data-ref-table='usertype'>
											<span class=''></span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
								</tr>
								<tr class='foreignBoxTr' data-reference-id='usertype' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='category' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Ονομασία</td>
														<td>Χρώμα</td>
													</tr>";
										if (!$results_usertypes)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($usertypesrow = mysql_fetch_assoc($results_usertypes))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$usertypesrow["id"]."</td>
														<td class='click foreignHtml'>".$usertypesrow["nameType"]."</td>
														<td class='click foreignHtml'>
														<div class='color-box' style='background-color:".$usertypesrow["colorType"]."'></div>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr class='name'>
								<td>
									<span class='field-name'>Ενεργό*</span>
								</td>
								<td class='data_input'>
									<select data-required id='isActive' name='isActive'>
										<option selected='selected' value='1'>Ναι</option>
										<option value='0'>Όχι</option>
									</select>
								</td>
							</tr>
								<tr>
								<tr class='foreignBoxTr' data-reference-id='type' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='type' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Ονομασία</td>
														<td>Χρώμα</td>
													</tr>
													<tr class='add-new'>
														<td colspan='5' class='foreign-button' data-input-reference='type' data-button-type='window' data-window-target='_pop_up' data-post-url='new.php' data-post-data-type='data' data-post-data-value='{\"table_name_eng\":\"usertype\",\"table_comment\":\"\"}'>Add New</td>
													</tr>";
											if (!$results_usertypes)
											{
												echo 
													"<tr class='table_name'>"
														."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
													."</tr>";
											}
											else
											{
												while($usertyperow = mysql_fetch_assoc($results_usertypes))
												{
													echo ""
													."
														<tr>
															<td class='click foreignKey'>".$usertyperow["id"]."</td>
															<td class='click foreignHtml'>".$usertyperow["nameType"]."</td>
															<td class='click'>
																<div class='color-box' style=".$usertyperow["colorType"]."></div>
															</td>
														</tr>"
														."";
												}
											}
				echo "</table>";
				echo "<input id='table' name='table' value='{$table}' type='text' style='display:none'/>";
				echo "<input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'/>";
				echo "<input id='field_val' name='field_val' value='' type='text' style='display:none'/>";
			?>
			</table>
		</form>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button save' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση</span></div>
		</div>
	</body>
</html>