<?php
    $languages=array("el"=>"Ελληνικά");
	$userTable="users";
	$userTypeTable="usertype";
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
        
        $uploadRoot="\uploads\\";
        $uploadHost="/uploads/";
        $imageRoot=array("normal"=>"images\\","thumb"=>"images_thumbs\\","thumb_500"=>"images_thumbs_500\\");
        $imageHost=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageSizes=array("normal"=>0,"thumb"=>150,"thumb_500"=>500);
        $fileRoot="files\\";
        $fileHost="files/";
    }else{
        $uploadRoot="/uploads/";
        $uploadHost="/uploads/";
        $imageRoot=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageHost=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageSizes=array("normal"=>0,"thumb"=>150,"thumb_500"=>500);
        $fileRoot="files/";
        $fileHost="files/";    
    }
	$fkViewValues=array(
		// "userType"=>"concat(nameType, ' ', column2)"
		"usertype"=>"nameType",
		"pictureinfo"=>"path",
		"category"=>"title",
		"users"=>"nickname",
		"keywords"=>"keyword",
		"thesis"=>"title",
		"students"=>"surname"
	);
	$translatorTables=array(
	);
?>