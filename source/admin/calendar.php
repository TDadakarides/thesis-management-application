<?php 
	// Get values from query string
	$months=array("Ιανουάριος","Φεβρουάριος","Μάρτιος","Απρίλιος","Μάιος","Ιούνιος","Ιούλιος","Αύγουστος","Σεπτέμβριος","Οκτώβριος","Νοέμβριος","Δεκέμβριος");
	date_default_timezone_set('Europe/Athens');
	$day=(isset($_GET["day"]))?$_GET["day"]:date('j');
	$month=(isset($_GET["month"]))?$_GET["month"]:date('m');
	$year=(isset($_GET["year"]))?$_GET["year"]:date('Y');
	$currentTimeStamp = strtotime("$year-$month-$day"); 
	$monthName = $months[date("m", $currentTimeStamp)-1]; 
	$numDays = date("t", $currentTimeStamp); 
	$counter = 0;
	print_r($_GET);
	echo "day:$day";
?>
<link rel="stylesheet" type="text/css" href="css/calendar.css"> 
<table width='175' border='0' cellspacing='0' cellpadding='0' class="calendar"> 
    <tr> 
        <td width='25' colspan='1'> 
			<div class='button prevMonth'>&lt;</div>
        </td> 
        <td width='125' align="center" colspan='5'> 
			<span class='title'><?php echo $monthName . " " . $year; ?></span><br> 
        </td> 
        <td width='25' colspan='1' align='right'> 
			<div class='button nextMonth'>&gt;</div>
        </td> 
    </tr> 
    <tr> 
        <td class='head' align="center" width='25'>Κ</td> 
        <td class='head' align="center" width='25'>Δ</td> 
        <td class='head' align="center" width='25'>Τ</td> 
        <td class='head' align="center" width='25'>Τ</td> 
        <td class='head' align="center" width='25'>Π</td> 
        <td class='head' align="center" width='25'>Π</td> 
        <td class='head' align="center" width='25'>Σ</td> 
    </tr> 
    <tr> 
<?php 
    for($i = 1; $i < $numDays+1; $i++, $counter++) 
    { 
        $timeStamp = strtotime("$year-$month-$i"); 
        if($i == 1) 
        { 
        // Workout when the first day of the month is 
        $firstDay = date("w", $timeStamp); 
        
        for($j = 0; $j < $firstDay; $j++, $counter++) 
        echo "<td> </td>"; 
        } 
        
        if($counter % 7 == 0) 
        echo "</tr><tr>"; 
        
        if(date("w", $timeStamp) == 0) 

        $class = "class='weekend'"; 
        else 
        if($i == date("j") && $month == date("m") && $year == date("Y")) 
        $class = "class='today'"; 
        else if($i ==$day ) 
        $class = "class='selected'"; 
        else
        $class = "class='normal'"; 
        
        echo "<td class='tr dates' align='center' width='25'><a class='buttonbar' ><font $class>$i</font></a></td>"; 
    } 
?> 
    </tr> 
</table>