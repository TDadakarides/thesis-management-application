<?php
	include "fkInit.php";
	include "functions_database.php";
	$table="regions";
	$pkValue=$_GET['region'];
	include	'database_include.php';
	$json=array();
	getJson($table,$pkValue);
	//getJson($table);
	$countJson=0;
	$priotity=0;
	function getJson($table,$pk="id",$pkValue = NULL,$refFromTable = NULL){
		global $json;
		global $con;
		global $translatorTables;
		global $siteHost,$uploadHost,$imageHost;
		global $countJson;
		global $priotity;
		$priotity++;
		$countJson++;
		//echo "<p>$countJson)TABLE:$table | PK:$pk | $pk:$pkValue | REF:$refFromTable </p>";
		//if ($countJson>=1000) return;
		$sqlValues = is_null($pkValue) ? "SELECT *  FROM {$table} ;": "SELECT *  FROM {$table} WHERE {$pk}=".$pkValue." ;";
		$resultValues =mysql_query($sqlValues);
		//echo"<p>$sqlValues</p>";
		
		////////Βρίσκουμε τα foreign keys αν υπάρχουν////////
		$sql_foreign_key = ""
				."SELECT table_name"
				.", column_name "
				.", constraint_name "
				.", referenced_table_name "
				.", referenced_column_name "
				."FROM information_schema.key_column_usage "
				."WHERE table_name='".$table."' "
				."AND referenced_column_name<>'';"; //διαλέγουμε πίνακα
		$results_foreign_key = mysql_query($sql_foreign_key);
		$i=0;
		$fk=array();
		while($row_foreign_key = mysql_fetch_assoc($results_foreign_key))
		{
			$fk[$row_foreign_key["column_name"]]=$row_foreign_key;
		}
		//////////////////////////////////////////////////////
		
		////////Βρίσκουμε τα ref foreign keys αν υπάρχουν////////
		$sql_ref_fk = ""
				."SELECT table_name"
				.", column_name "
				.", constraint_name "
				.", referenced_table_name "
				.", referenced_column_name "
				."FROM information_schema.key_column_usage "
				."WHERE referenced_table_name='".$table."' "
				."AND referenced_column_name<>'';"; //διαλέγουμε πίνακα
		$results_ref_fk = mysql_query($sql_ref_fk);
		$i=0;
		$ref_fk=array();
		while($row_ref_fk = mysql_fetch_assoc($results_ref_fk))
		{
			$ref_fk[$row_ref_fk["constraint_name"]]=$row_ref_fk;
		}
		//////////////////////////////////////////////////////
		$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; //διαλέγουμε πίνακα
		$result = mysql_query($sql);
		$row_fields =array();
		while($row = mysql_fetch_assoc($result))
		{
			array_push($row_fields,$row);
		}
		if(!isset($json[$table])) $json[$table]=$priotity;
		//$json[$table]["fk"]=$fk;
		//$json[$table]["ref_fk"]=$ref_fk;
		//print_r($row_fields);
		while( $rowValues=mysql_fetch_assoc($resultValues) )
		{
			$temp=array();
			foreach($row_fields as $row_field)
			{
				$fieldKey=$row_field["Field"];
				$isTinyInt=strpos($row_field["Type"],"tinyint")!== false;
				$isPhoto=strpos($row_field["Comment"],"Φωτο")!== false;
				$isFK=isset($fk[$fieldKey])?true:false;
				$isFKPopUp=$isFK?
					in_array($fk[$fieldKey]["referenced_table_name"], $translatorTables)?true:false
					:false;
				if ($isFK){
					$thisFieldFK=$fk[$fieldKey];
					if($refFromTable!=$thisFieldFK["referenced_table_name"] && !$isFKPopUp) {
						getJson($thisFieldFK["referenced_table_name"],$thisFieldFK["referenced_column_name"],$rowValues[$row_field["Field"]],$table);
					}
					$fkFieldView=getForeignName($thisFieldFK["referenced_table_name"],$thisFieldFK["referenced_column_name"],$rowValues[$row_field["Field"]]);
				}else{
					if ($isTinyInt){
						$fkFieldView=$rowValues[$row_field["Field"]]==1?true:false;					
					}else if($isPhoto){
						$fkFieldView=$siteHost.$uploadHost.$imageHost["normal"].$rowValues[$row_field["Field"]];
					}
					else{
						$fkFieldView=$rowValues[$row_field["Field"]];
					}
				}
				$temp[$row_field["Field"]]=$fkFieldView;
			}
			//array_push( $json[$table],$temp);
			
			foreach($ref_fk as $ref)
			{
				if($refFromTable!=$ref["table_name"]) getJson($ref["table_name"],$ref["column_name"],$rowValues["id"],$table);
			}
		}
		$priotity--;
	}
	asort($json);
	echo json_encode($json);
	mysql_close($con);
?>