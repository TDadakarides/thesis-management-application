<?php
	
	function getForeignName($table,$field,$fieldValue){
		if (!isset($fkViewValues) ) include "fkInit.php";
		if (array_key_exists($table,$fkViewValues) && $fieldValue!="")
		{
		$sqlFK = "
		SELECT table_name,
		column_name,constraint_name,
		referenced_table_name,
		referenced_column_name 
		FROM information_schema.key_column_usage 
		WHERE table_name='".$table."' and column_name='".$fkViewValues[$table]."';"; //διαλέγουμε πίνακα
		
		$resultsFk = mysql_query($sqlFK);
		// if (mysql_num_rows($resultsFk)>0){
			$rowFk=mysql_fetch_assoc($resultsFk);
			//echo "----------".PHP_EOL;
			$sqlValue="SELECT ".$fkViewValues[$table]." FROM $table WHERE id=$fieldValue ;";
			//echo"<p>$sqlValue</p>";
			$resultsValue = mysql_query($sqlValue);
			$rowValue=mysql_fetch_assoc($resultsValue);
			$realname=getForeignName($rowFk["referenced_table_name"],$rowFk["referenced_column_name"],$rowValue[$fkViewValues[$table]]);
			//echo"realname:$realname|";
			return $realname;
		}
		//echo"fieldValue:$fieldValue|";
		return $fieldValue;

	}
    $cachedImaged=array();
	function getImageGallery($table,$id){
        global $con;
        global $siteHost;
        global $uploadHost;
        global $imageHost;
        global $cachedImaged;
//        $siteHost="http://elate.gr";
        //echo "siteHost:$siteHost";
        //$result=new stdClass();
        $result=array();
        $sql_model = ""
            ."SELECT p.id AS Id "
            .",p.first AS IsFirst "
            .",pi.path AS Image "
//            .",l.title AS License "
//            .",l.terms AS LicenseTerms "
//            .",pi.comment AS LicenseLink "
            ."FROM pictures p "
            ."LEFT JOIN pictureInfo pi ON pi.id=p.picture "
//            ."LEFT JOIN pictureLicense l ON pi.license=l.id "
            ."WHERE p.isActive=1 AND p.itemId={$id} AND p.tableName='{$table}' "
            ."ORDER BY p.first Asc";
        //echo "sql_model:$sql_model";
        $results_model = mysql_query($sql_model);
        while($row_model = mysql_fetch_assoc($results_model)){
            $image=$row_model["Image"]?$row_model["Image"]:"";
            $temp=new stdClass();
            $temp->Id = $row_model["Id"];
            $temp->IsFirst = $row_model["IsFirst"]==1;
            $temp->Image = $image;
//            $temp->License = ($row_model["License"]);
//            $temp->LicenseTerms = ($row_model["LicenseTerms"]);
//            $temp->ImageLicenseLink = ($row_model["LicenseLink"]);
            array_push($result,$temp);
            unset($temp);
            if ( !in_array($image,$cachedImaged) && $image!="" ){
                array_push($cachedImaged,$image);
            }
        }
        return $result;
    }
    function generateUrl($model,$id,$title){
        $result = "";
        switch($model){
            case "agenda":
                $result = $id==""
                    ? "/agenda/"
                    : "/agenda/".(greeklish($title))."/{$id}/";
                
            case "agendaperday":
                $result ="/agenda/day/";
            case "agendapertype":
                $result ="/agenda/type/";
                
        }
    }
    function greeklish($conv_path)
    {
        $textEnglishUpper=str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        $textEnglishLower=str_split(strtolower("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
        $validChars=str_split("1234567890-");
        $textGreekLow=		array("α","β","γ","δ","ε","ζ","η","θ","ι","κ","λ","μ","ν","ξ","ο","π","ρ","σ","τ","υ","φ","χ","ψ","ω","ά","έ","ή","ϊ","ί","ΐ","ό","ϋ","ύ","ΰ","ώ","ς");
        $textEnglishLow=	array("a","b","g","d","e","z","i","th","i","k","l","m","n","x","o","p","r","s","t","u","f","ch","ps","o","a","e","i","i","i","i","o","u","u","u","o","s");
        $conv_path=str_replace($textGreekLow,$textEnglishLow,$conv_path);
        $textGreekUp=		array("Α","Β","Γ","Δ","Ε","Ζ","Η","Θ","Ι","Κ","Λ","Μ","Ν","Ξ","Ο","Π","Ρ","Σ","Τ","Υ","Φ","Χ","Ψ","Ω","Ά","Έ","Ή","Ϊ","Ί","Ί","Ό","Ϋ","Ύ","Ύ","Ώ","«","»");
        $textEnglishUp=	array("A","B","C","D","E","Z","I","Th","I","K","L","M","N","X","O","P","R","S","T","U","F","Ch","Ps","O","A","E","I","I","I","I","O","U","U","U","O","-","-");
        $conv_path=str_replace($textGreekUp,$textEnglishUp,$conv_path);
        $textOrigin=	array("-","","?","&","%","Ά","'","\"","�","®","ύ"," / ");
        $textReplaceTo=	array("-","", "","-","-","Α","-", "-","-","-","υ","/");
        $conv_path=str_replace($textOrigin,$textReplaceTo,$conv_path);
        $allValidChars=array_merge($textGreekLow,$textGreekUp,$textOrigin,$textEnglishUpper,$textEnglishLower,$validChars);
        //print_r($allValidChars);
        //echo"<b>$conv_path</b> to ";
        $string=str_split($conv_path,1);
        //print_r($string);
        $conv_path='';
        foreach($string as $char)
        {		
            //echo"<b>$char</b> to ";
            $newChar=(in_array($char,$allValidChars))?$char:'-';
            //echo"<b>$newChar</b>,";
            $conv_path.=$newChar;
        }
        if (strlen($conv_path)>246)	{$conv_path = substr($conv_path, 0, 246);$conv_path=$conv_path.'/';}
        //echo"<b>$conv_path</b><br/>";
        return $conv_path;
    }
?>