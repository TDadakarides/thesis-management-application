<?php
include "members.php";
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Δημιουργία</div>
			<?php
				$count_cell=16;
                $isUser=1;
				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				//$field_val=$_POST['field_val'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$referenceTable=isset($_POST['ref_table'])?$_POST['ref_table']:"";
				$referenceField=isset($_POST['ref_table_field'])?$_POST['ref_table_field']:"";
				$tableClass='';
				echo "<div id='title' style='display:none;'>{$table}</div>";
				echo "<div class='titlePage' ><p>{$table_comment}</p></div>";
				echo "<form action='save.php' method='post' id='form1' name='form1'>";
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; 
				$result = mysql_query($sql);
				$fieldInfo=array();
				$allFields=array();
				while($row = mysql_fetch_assoc($result))
				{				
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}
				$sql = "select table_name,column_name,constraint_name,referenced_table_name,referenced_column_name from information_schema.key_column_usage where table_name='".$table."' and referenced_column_name<>'';"; 
				$results_foreign_key = mysql_query($sql);
				$i=0;
				$fk=array();
				while($row = mysql_fetch_assoc($results_foreign_key))
				{
					$fk[$row["column_name"]]=$row;
				}

				$sql_student =""
				."SELECT	s.id,
							CONCAT( s.name, ' ', s.surname) as name,
					        s.department,
					        s.code,
					        s.dateCreate,
					        s.dateUpdate,
					        s.email,
							(CASE WHEN s.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive
					FROM students s";

					$result_student = mysql_query($sql_student);
				echo ""
					.
				   "<table align='center' class='table' cellpadding='0' cellspacing='0'>
					         <tbody>
					         	<tr style='display:none;'>
					               <td>
					               <span class='field-name'>Κωδικός*</span>
					               </td>
					               <td class='data_input' style='display:none;'><input id='id' name='id' type='text' value='' class='max input'>
					               </td>
				            	</tr>
				            	<tr class='name' style='display:none;'>
									<td>
										<span class='field-name'>Πτυχιακή*</span>
									</td>
									<td class='data_input'>
										<input data-required='' class='max foreign_value' id='thesisId' name='thesisId' type='text' value='{$foreignValue}'>
										<div class='foreignBox' data-ref-table='thesis' data-src='show_foreign.php?hasNull=NO&table=thesis&table_pk=id&ref_table=thesis_comments&ref_field=thesisId'>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
										</div>
									</td>
								</tr>
				            	<tr>
									<td>
									<span class='field-name'>Σπουδαστής*</span>
									</td>
									<td class='data_input'>
									<input data-required='' class='max foreign_value' id='studentId' name='studentId' type='text' value>
									<div class='foreignBox' data-ref-table='students'>
											<span class=''></span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
								</tr>
								<tr class='foreignBoxTr' data-reference-id='id' style='display: none;'>
									<td colspan='5'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='students' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Όνοματεπώνυμο</td>
														<td>Τμήμα</td>
														<td>Αριθμός Μητρώου</td>
														<td>Ημερομηνία δημιουργίας</td>
														<td>Ημερομηνία τελευταίας ενημέρωσης</td>
														<td>Email</td>
														<td>Ενεργό</td>
													</tr>";
										if (!$result_student)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($studentrow = mysql_fetch_assoc($result_student))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$studentrow["id"]."</td>
														<td class='click foreignHtml'>".$studentrow["name"]."</td>
														<td class='click foreignHtml'>".$studentrow["department"]."</td>
														<td class='click foreignHtml'>".$studentrow["code"]."</td>
														<td class='click foreignHtml'>".$studentrow["dateCreate"]."</td>
														<td class='click foreignHtml'>".$studentrow["dateUpdate"]."</td>
														<td class='click foreignHtml'>".$studentrow["email"]."</td>
														<td class='click foreignHtml'>".$studentrow["isActive"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Σχόλιο*</span>
								<p></p>
								 	<textarea  style='float:right;' class='mceEditor tinymce' id='comment' name='comment'  ></textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='comment'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='comment'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								 <tr class='name'>
								<td>
									<span class='field-name'>Ενεργό*</span>
								</td>
								<td class='data_input'>
									<select data-required='' id='isActive' name='isActive'>
										<option selected='selected' value='1'>Ναι</option>
										<option value='0'>Όχι</option>
									</select>
								</td>
							</tr>
					         </tbody>
					      </table>
					      <input id='table' name='table' value='{$table}' type='text' style='display:none'>
					      <input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'>
					   </form>
					</div>"
				."";
				echo "</table>";
				echo "<input id='table' name='table' value='{$table}' type='text' style='display:none'/>";
				echo "<input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'/>";
				echo "<input id='field_val' name='field_val' value='' type='text' style='display:none'/>";
			?>
			</table>
		</form>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button save' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση</span></div>
		</div>
	</body>
</html>