<?php
include "members.php";
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Δημιουργία</div>
			<?php
                $isUser=1;
				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				//$field_val=$_POST['field_val'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$referenceTable=isset($_POST['ref_table'])?$_POST['ref_table']:"";
				$referenceField=isset($_POST['ref_table_field'])?$_POST['ref_table_field']:"";
				$tableClass='';
				echo "<div id='title' style='display:none;'>{$table}</div>";
				echo "<div class='titlePage' ><p>{$table_comment}</p></div>";
				echo "<form action='save.php' method='post' id='form1' name='form1'>";
				//Δυναμική εμφάνιση επικεφαλίδων πεδίων
				/////////////Περνάμε σε πίνακες τα στοιχεία των πεδίων/////////////////////
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; //διαλέγουμε πίνακα
				$result = mysql_query($sql);
				$fieldInfo=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΕ ΤΙΣ ΠΛΗΡΟΦΟΡΙΕΣ
				$allFields=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΟΝΟ ΟΝΟΜΑΣΙΕΣ
				while($row = mysql_fetch_assoc($result))
				{
					// if ($row["Key"]=='PRI') $field_key=$row["Field"];
					// $row_type[$i]=$row["Type"]; //αποθηκεύουμε στο row_type τους τυπους πεδιων
					// $row_null[$i]=$row["Null"]; //αποθηκεύουμε στο row_null τα NULL (YES - NO ) των πεδιων
					// $row_extra[$i]=$row["Extra"]; //αποθηκεύουμε στο row_extra τα εξτρα των πεδιων
					// $row_name[$i]=$row["Comment"]; //αποθηκεύουμε στο row_name τις ταμπέλες (comment) των πεδιων
					// $row_field_name[$i]=$row["Field"];
					// $i++;					
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}

				$sql_supervisors= ""
					."SELECT 
							u.id,
							u.username
							FROM users u
							INNER JOIN usertype us on u.type=us.id
							Where us.nameType= 'Supervisor'";

				$results_supervisors = mysql_query($sql_supervisors);

				//////////Βρίσκουμε τα foreign keys αν υπάρχουν////////
				$sql = "select table_name,column_name,constraint_name,referenced_table_name,referenced_column_name from information_schema.key_column_usage where table_name='".$table."' and referenced_column_name<>'';"; //διαλέγουμε πίνακα
				$results_foreign_key = mysql_query($sql);
				$i=0;
				$fk=array();///ΟΛΑ ΤΑ ΔΕΥΤΕΡΕΥΟΝΤΑ ΚΛΕΙΔΙΑ
				/////ΕΑΝ ΥΠΑΡΧΟΥΝ FOREIGN KEYS ΤΑ ΑΠΟΘΗΚΕΥΟΥΜΕ ΣΕ ΠΙΝΑΚΕΣ////////////////
				while($row = mysql_fetch_assoc($results_foreign_key))
				{
					// $fk["table_name"][$i]=$row[0]; //αποθηκεύουμε στο fk["table_name"] το ονομα του πινακα που ειμαστε
					// $fk["column_name"][$i]=$row[1]; //αποθηκεύουμε στο fk["column_name"] το ονομα του πεδιου που σχετιζεται
					// $fk["referenced_table_name"][$i]=$row[3]; //αποθηκεύουμε στο fk["referenced_table_name"] το ονομα του σχετιζόμενου πινακα 
					// $fk["referenced_column_name"][$i]=$row[4];//αποθηκεύουμε στο fk["referenced_table_name"] το ονομα του σχετιζόμενου πινακα 
					// $i++;
					$fk[$row["column_name"]]=$row;
				}
				echo ""
					.
				   "<table align='center' class='table' cellpadding='0' cellspacing='0'>
							<tbody>
								<tr class='name' style='display:none;'>
									<td>
										<span class='field-name'>id*</span>
									</td>
									<td class='data_input' style='display:none;'>
										<input id='id' name='id' type='text' value='' class='max input'>
									</td>
								</tr>
								<tr class='name' style='display:none;'>
									<td>
										<span class='field-name'>Id Καθηγητή*</span>
									</td>
									<td class='data_input'>
									<input data-required='' class='max foreign_value' id='{$foreignKey}' name='{$foreignKey}' type='text' value='{$foreignValue}'><div class='foreignBox' data-ref-table='users' data-src='show_foreign.php?hasNull=NO&amp;table=users&amp;table_pk=id&amp;ref_table={$table}&amp;ref_field=users'>
										<div class='foreignButton' style='cursor:pointer;'>+</div>
										<div style='clear:both;display:block;'></div>
									</div>
								</td>
							</tr>
							<tr class='name'>
								<td>
									<span class='field-name'>Βοηθός*</span>
								</td>
								<td class='data_input'><input data-required='' class='max foreign_value' id='supervisorid' name='supervisorid' type='text' value=''>
									<div class='foreignBox' data-ref-table='users' data-src='show_foreign.php?hasNull=NO&amp;table=users&amp;table_pk=id&amp;ref_table=user_supervisor&amp;ref_field=supervisorid'>
										<span class=''></span>
										<div class='foreignButton' style='cursor:pointer;'>+</div>
										<div style='clear:both;display:block;'></div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<input id='table' name='table' value='user_supervisor' type='text' style='display:none'>
					<input id='table_comment' name='table_comment' value='Λέξεις κλειδιά' type='text' style='display:none'><input id='field_val' name='field_val' value='' type='text' style='display:none'>
					</form>
					</div>"
				."";
				echo "</table>";
				echo "<input id='table' name='table' value='{$table}' type='text' style='display:none'/>";
				echo "<input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'/>";
				echo "<input id='field_val' name='field_val' value='' type='text' style='display:none'/>";
			?>
			</table>
		</form>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button save' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση</span></div>
		</div>
	</body>
</html>