<?php
include "members.php";
include "type_of_tables.php";
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<?php
            $isUser=1;
			$plithos_pediwn=8;
			$count_cell=16;

			include	'functions.php';
			include	'url_functions.php';

			$table=$_POST['table'];
			include	'database_include.php';
			$table_comment=$_POST['table_comment'];
			$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
			$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
			$sqlWhere=($foreignKey!="" && $foreignValue!="")?" WHERE {$foreignKey}={$foreignValue} ":"";
			if (isset($_POST['sort_asc'])) {$sort_asc=$_POST['sort_asc'];} else {$sort_asc="ASC";}
			if (isset($_POST['sorted'])) {$sort_new="ORDER BY {$_POST['sorted']} $sort_asc";$sort_by=$_POST['sorted'];} else {$sort_new="";$sort_by="";}
			if ($table=='agenda') $sort_new=" ORDER BY t.dateStart DESC,t.id ASC ";
			

			echo "<div id='title' align='center' style='display:none;'>{$table}</div>";
			echo "<div class='titlePage'><p>{$table_comment}</p></div>";
			echo "<table  class='table' cellpadding='0' cellspacing='0'>";
            echo "".
            "<tr>".
            "<td>Α/Α</td>
            <td>Διαγραφή</td>
            <td>Επεξεργασία</td>
            <td data-field-sort='t.id' data-sort-by='asc' style='display:none;' class='sorted id'>Κωδικός</td>
            <td data-field-sort='t.title' data-sort-by='asc' class='sorted title'>Ονομασία</td>
            <td data-field-sort='t.dateCreate' data-sort-by='asc' class='sorted dateCreate'>Ημερομηνία δημιουργίας</td>
            <td data-field-sort='t.dateUpdate' data-sort-by='asc' class='sorted dateUpdate'>Ημερομηνία τελευταίας ενημέρωσης</td>
            <td data-field-sort='t.isActive' data-sort-by='asc' class='sorted isActive'>Ενεργό</td>"
            ."</tr>";
            ////////////////////////////////////////////////////
        	$testsqlfinal= ""
            ."SELECT 
              t.id AS id,
              t.title AS title,
              t.dateCreate AS dateCreate,
              t.dateUpdate AS dateUpdate,
              (CASE WHEN t.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive 
              FROM category t "
            ." {$sqlWhere} {$sort_new} ";
            
            $results_field = mysql_query($testsqlfinal);
            $pagerTotalPages=7;
				//$fields_num = mysql_num_fields($results_field);
				$fields_rows = mysql_num_rows($results_field);
				$rowsPerPageValues=array(4,8,12,24,50);
				$page=(isset($_POST["page"])?$_POST["page"]:1);
				$viewall=($page?$page==0:false);
				$rowsCount=$fields_rows;
				$rowsPerPage=(isset($_POST["rowsPerPage"])?$_POST["rowsPerPage"]:$rowsPerPageValues[1]);
				$pageCount=ceil($rowsCount/$rowsPerPage);
				$sql="SELECT * FROM {$table} t {$sqlWhere} {$sort_new} LIMIT ".(($page-1)*$rowsPerPage).", {$rowsPerPage} ;";
				$testsqlfinal= $testsqlfinal ." LIMIT ".(($page-1)*$rowsPerPage).", {$rowsPerPage} ;";

        	
            $results_field = mysql_query($testsqlfinal);

			if (!$results_field)
			{
				echo 
					"<tr class='table_name'>"
						."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
					."</tr>";
			}
			else
			{
				$indexRow=0;
				while($row = mysql_fetch_assoc($results_field))
				{
					$indexRow++;
					echo ""
					."<tr class='table_name'>
						<td class='a-a'>{$indexRow}</td>
						<td class='delete'>
							<div class='jsbutton delete icon-delete red-button' data-button-type='action' data-post-url='delete.php' data-post-data-type='data' data-post-data-value='{\"field_val\":\"".$row["id"]."\",\"table_name_eng\":\"category\",\"table_comment\":\"Διαγραφή\",\"foreign_key\":\"\",\"foreign_value\":\"\"}'></div>
						</td>
						<td class='edit'>
							<div class='jsbutton edit icon-pencil green-button' data-button-type='window' data-window-target='_this' data-post-url='edit_category.php' data-post-data-type='data' data-post-data-value='{\"field_val\":\"".$row["id"]."\",\"table_name_eng\":\"category\",\"table_comment\":\"Επεξεργασία\",\"foreign_key\":\"\",\"foreign_value\":\"\"}'></div>
						</td>
						<td class='click'>".$row["title"]."</td>
						<td class='click'>".$row["dateCreate"]."</td>
						<td class='click'>".$row["dateUpdate"]."</td>
						<td class='click'>".$row["isActive"]."</td>
					</tr>"
					."";
				}			
				if ($fields_rows==0) {
					echo "<tr class='table_name'>";
					echo "<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές..</td>";
					echo "</tr>";
				}
				if ($rowsCount>0){
					echo ""
					."<tr>"
					."<td colspan='{$count_cell}'>";
					echo ""
					."<span class='rows-total'>Εγγραφές {$indexRow}/{$rowsCount} | </span>"
					."<span class='page-span'>Σελίδα {$page}/{$pageCount}</span>";
					if( $pageCount<=$pagerTotalPages ){
						$pagerFirstPage=1;
						$pagerLastPage=$pageCount;
					}else if($page-floor($pagerTotalPages/2)<1 || $page+floor($pagerTotalPages/2)>$pageCount ){
						if($page-floor($pagerTotalPages/2)<1){
							$pagerFirstPage=1;
							$pagerLastPage=$pagerTotalPages;
						}else if($page+floor($pagerTotalPages/2)>$pageCount){
							$pagerFirstPage=$pageCount-$pagerTotalPages+1;
							$pagerLastPage=$pageCount;
						}
					}
					else{
						$pagerFirstPage=$page-floor($pagerTotalPages/2);
						$pagerLastPage=$page+floor($pagerTotalPages/2);
					}
					
					echo ""
					."<div class='pager-box right'>"
					."<select class='page-select input' style='float:none;'>";
					foreach($rowsPerPageValues as $perPage){
					echo ""
						."<option".($perPage==$rowsPerPage?" selected='selected'":"")." value='{$perPage}'>{$perPage}</option>";
					}
					echo ""
					."</select>";
					if(1!=$pagerFirstPage){
						echo ""
						."<span data-page='1' class='input page-index'>1</span>";						
					}
					for($pageIndex=$pagerFirstPage;$pageIndex<=$pagerLastPage;$pageIndex++){
						echo ""
						."<span data-page='{$pageIndex}' class='input page-index".($pageIndex==$page?" active":"")."'>{$pageIndex}</span>";
					}
					if($pageCount!=$pagerLastPage){
						echo ""
						."<span data-page='{$pageCount}' class='input page-index'>{$pageCount}</span>";						
					}
					echo ""
					."</div>"
					."</td>"
					."</tr>"
					;
				}
				mysql_free_result($results_field);
			}
		?>
		</table>		
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button new' data-button-type='window' data-window-target='_this' data-post-url='new_category.php'data-post-data-type='data' data-post-data-value='{"table_name_eng":"<?=$table;?>","table_comment":"<?=$table_comment;?>","foreign_key":"<?=$foreignKey;?>","foreign_value":"<?=$foreignValue;?>"}'><div class='innerbutton'></div><div class='image'></div><span>Προσθήκη</span></div>
		</div>
	</body>
</html>