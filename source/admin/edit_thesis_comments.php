<?php
include "members.php"; 
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Σχόλια Πτυχιακής</div>
			<?php
                $isUser=1; 
				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				$field_val=$_POST['field_val'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$tableClass='';
				
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; //διαλέγουμε πίνακα
				$result = mysql_query($sql);
				$fieldInfo=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΕ ΤΙΣ ΠΛΗΡΟΦΟΡΙΕΣ
				$allFields=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΟΝΟ ΟΝΟΜΑΣΙΕΣ
				while($row = mysql_fetch_assoc($result))
				{				
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}
				
				$results_field=array();

				$sql_fields= ""
				."SELECT 	tc.id,
							t.title,
							tc.studentId,
					        CONCAT(s.name,' ',s.surname) AS student_name,
					        tc.comment,
					        tc.isActive
					FROM  thesis_comments tc
					INNER JOIN thesis t ON tc.thesisId = t.id
					INNER JOIN students s on tc.studentId = s.id
					WHERE tc.id=" ."{$field_val}";

				$sql_thesis =""
				."SELECT 	t.id,
							t.title,
					        c.title as category,
					        u.nickname,
					        t.minstudent,
					  		t.maxstudent,
					  		t.datePublished,
					        t.dateStart,
					        t.datePresentation,
					        t.dateDelivery,
					        (CASE WHEN t.has_congres=1 THEN 'Ναι' ELSE 'Όχι' END) AS has_congres,
					        (CASE WHEN t.has_congres=1 THEN t.congres_title ELSE '' END) AS congres_title,
					        t.dateUpdate,
					        (CASE WHEN t.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive
					FROM thesis t
					INNER JOIN users u on t.professor=u.id
					INNER JOIN category c on t.category=c.id
					WHERE u.username = " . "'{$username}'";

				$sql_student =""
				."SELECT	s.id,
							CONCAT( s.name, ' ', s.surname) as name,
					        s.department,
					        s.code,
					        s.email,
							(CASE WHEN s.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive
					FROM students s";

				$results_fields = mysql_query($sql_fields);
				$result_thesis = mysql_query($sql_thesis);
				$result_student = mysql_query($sql_student);
				//echo $sql_fields;
				if (!$results_fields) {
					echo ("Δεν υπάρχουν εγγραφές");
				}
				else
				{
					$rtb_num=0;
					$row = mysql_fetch_assoc($results_fields);
					echo ""
					.
				   "<form action='save.php' method='post' id='form1' name='form1'>
					      <table align='center' class='table' cellpadding='0' cellspacing='0'>
					         <tbody>
					         	<tr style='display:none;'>
					               <td>
					               <span class='field-name'>Κωδικός*</span>
					               </td>
					               <td class='data_input' style='display:none;'><input data-required='' id='id' name='id' type='text' value='".$row["id"]."' class='max input'>
					               </td>
				            	</tr>
					             <tr>
									<td>
									<span class='field-name'>Πτυχιακή*</span>
									</td>
									<td class='data_input'>
									<input data-required='' class='max foreign_value' id='title' name='title' type='text' value='".$row["title"]."'>
									<div class='foreignBox' data-ref-table='thesis'>
											<span class=''>".$row["title"]."</span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
								</tr>
								<tr class='foreignBoxTr' data-reference-id='thesis' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='category' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Τίτλος</td>
														<td>Κατηγορία</td>
														<td>Καθηγητής</td>
														<td>Ελάχιστος αριθμός σπουδαστών</td>
														<td>Μέγιστος αριθμός σπουδαστών</td>
														<td>Ημερομηνία δημοσίευσης</td>
														<td>Ημερομηνία ανάθεσης</td>
														<td>Ημερομηνία παρουσίασης</td>
														<td>Ημερομηνία παράδοσης</td>
														<td>Παρουσίαση σε συνέδριο</td>
														<td>Τίτλος συνεδρίου</td>
														<td>Ημερομηνία τελευταίας επεξεργασίας</td>
														<td>Ενεργό</td>
													</tr>";
										if (!$result_thesis)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($thesisrow = mysql_fetch_assoc($result_thesis))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$thesisrow["id"]."</td>
														<td class='click foreignHtml'>".$thesisrow["title"]."</td>
														<td class='click foreignHtml'>".$thesisrow["category"]."</td>
														<td class='click foreignHtml'>".$thesisrow["nickname"]."</td>
														<td class='click foreignHtml'>".$thesisrow["minstudent"]."</td>
														<td class='click foreignHtml'>".$thesisrow["maxstudent"]."</td>
														<td class='click foreignHtml'>".$thesisrow["datePublished"]."</td>
														<td class='click foreignHtml'>".$thesisrow["dateStart"]."</td>
														<td class='click foreignHtml'>".$thesisrow["datePresentation"]."</td>
														<td class='click foreignHtml'>".$thesisrow["dateDelivery"]."</td>
														<td class='click foreignHtml'>".$thesisrow["has_congres"]."</td>
														<td class='click foreignHtml'>".$thesisrow["congres_title"]."</td>
														<td class='click foreignHtml'>".$thesisrow["dateUpdate"]."</td>
														<td class='click foreignHtml'>".$thesisrow["isActive"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td>
									<span class='field-name'>Σπουδαστής*</span>
									</td>
									<td class='data_input'>
									<input data-required='' class='max foreign_value' id='studentId' name='studentId' type='text' value='".$row["studentId"]."'>
									<div class='foreignBox' data-ref-table='students'>
											<span class=''>".$row["student_name"]."</span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
								</tr>
								<tr class='foreignBoxTr' data-reference-id='students' style='display: none;'>
									<td colspan='5'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='students' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Όνοματεπώνυμο</td>
														<td>Τμήμα</td>
														<td>Αριθμός Μητρώου</td>
														<td>Email</td>
														<td>Ενεργό</td>
													</tr>";
										if (!$result_student)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($studentrow = mysql_fetch_assoc($result_student))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$studentrow["id"]."</td>
														<td class='click foreignHtml'>".$studentrow["name"]."</td>
														<td class='click foreignHtml'>".$studentrow["department"]."</td>
														<td class='click foreignHtml'>".$studentrow["code"]."</td>
														<td class='click foreignHtml'>".$studentrow["email"]."</td>
														<td class='click foreignHtml'>".$studentrow["isActive"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Σχόλια*</span>
								<p></p>
								 	<textarea data-required='' style='float:right;' class='mceEditor tinymce' id='comment' name='comment'  >".$row["comment"]."</textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='comment'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='intro'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
					               <td>
					               <span class='field-name'>Ενεργό*</span>
					               </td>
					               <td class='data_input'>
					                  <select data-required='' id='isActive' name='isActive'>";
				                     	echo "<option ";if ($row["isActive"]==0)echo "selected='selected' ";echo "value='0'>Όχι</option>";		
										echo "<option ";if ($row["isActive"]==1)echo "selected='selected' ";echo "value='1'>Ναι</option>";
										echo "".
					                  "</select>
					               </td>
					            </tr>
					         </tbody>
					      </table>
					      <input id='table' name='table' value='{$table}' type='text' style='display:none'>
					      <input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'>
					      <input id='field_val' name='field_val' value='{$field_val}' type='text' style='display:none'>
					   </form>
					</div>"
				."";
				}
			?>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button update' data-button-type='action' data-post-url='update.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Ενημέρωση</span></div>
<!--			<div class='button saveas' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση ως</span></div>-->
		</div>
	</body>
</html>