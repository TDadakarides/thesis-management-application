<?php
	//--------------------------------
	// CREATE WATERMARK FUNCTION
	//--------------------------------

	define( 'WATERMARK_OVERLAY_IMAGE', 'watermark.png' );
	define( 'WATERMARK_OVERLAY_OPACITY', 100 );
	define( 'WATERMARK_OUTPUT_QUALITY', 90 );

	if (!function_exists('create_watermark'))
	{
		function create_watermark( $source_file_path, $output_file_path )
		{
			list( $source_width, $source_height, $source_type ) = getimagesize( $source_file_path );

			if ( $source_type === NULL )
			{
			  return false;
			}

			switch ( $source_type )
			{
			  case IMAGETYPE_GIF:
				$source_gd_image = imagecreatefromgif( $source_file_path );
				break;
			  case IMAGETYPE_JPEG:
				$source_gd_image = imagecreatefromjpeg( $source_file_path );
				break;
			  case IMAGETYPE_PNG:
				$source_gd_image = imagecreatefrompng( $source_file_path );
				break;
			  default:
				return false;
			}

			$overlay_gd_image = imagecreatefrompng( WATERMARK_OVERLAY_IMAGE );
			$overlay_width = imagesx( $overlay_gd_image );
			$overlay_height = imagesy( $overlay_gd_image );
			$scale=0.2;
			$scalex=$overlay_width/($source_width*$scale);
			$overlay_width=$source_width*$scale;
			$overlay_height=$overlay_height*$scalex;

			imagecopy(
			  $source_gd_image,
			  $overlay_gd_image,
			  $source_width - $overlay_width,
			  $source_height - $overlay_height,
			  0,
			  0,
			  $overlay_width,
			  $overlay_height
			);

			imagejpeg( $source_gd_image, $output_file_path, WATERMARK_OUTPUT_QUALITY );

			imagedestroy( $source_gd_image );
			imagedestroy( $overlay_gd_image );
		}
	}
	
	
	if (!function_exists('create_thumb'))
	{
		function create_thumb( $source_file_path,$thumb_file_path,$thumb_size )
		{
			if ( file_exists($source_file_path) )
			{
				list( $source_width, $source_height, $source_type ) = getimagesize( $source_file_path );

				if ( $source_type === NULL )
				{
					echo "source_type:$source_type<br/>";
					return false;
				}

				switch ( $source_type )
				{
				  case IMAGETYPE_GIF:
					$source_gd_image = imagecreatefromgif( $source_file_path );
					break;
				  case IMAGETYPE_JPEG:
					$source_gd_image = imagecreatefromjpeg( $source_file_path );
					break;
				  case IMAGETYPE_PNG:
					$source_gd_image = imagecreatefrompng( $source_file_path );
					break;
				  default:
					echo "invalid source_type:$source_type<br/>";
					return false;
				}
				
				$scale=$source_width/$source_height;
				if ($scale>=1)
				{
					$new_width=$thumb_size;
					$new_height=floor($new_width/$scale);
				}
				else
				{
					$new_height=$thumb_size;
					$new_width=floor($new_height*$scale);
				}
				$tmp_image=imagecreatetruecolor($new_width,$new_height);
				imagecopyresized($tmp_image,$source_gd_image,0,0,0,0,$new_width,$new_height,$source_width,$source_height);
				imagejpeg( $tmp_image,$thumb_file_path);

				imagedestroy( $tmp_image );
				imagedestroy( $source_gd_image );
				echo "copy done<br/>";
				return true;
			}
			echo "invalid source_file_path:$source_file_path<br/>";
			return false;
		}
	}
	define ("MAX_SIZE","100"); 
	//This function reads the extension of the file. It is used to determine if the file  is an image by checking the extension.
	if (!function_exists('getExtension'))
	{
		function getExtension($str)
		{
			 $i = strrpos($str,".");
			 if ($i<0) { return ""; }
			 $l = strlen($str) - $i;
			 $ext = substr($str,$i+1,$l);
			 return $ext;
		}
	}
	if (!function_exists('getFilename'))
	{
		function getFilename($str)
		{
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,0,$i);
			return $ext;
		}
	}


 ?>

 