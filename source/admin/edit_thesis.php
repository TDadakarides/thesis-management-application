<?php
include "members.php"; 
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Επεξεργασία Πτυχιακης</div>
			<?php
                $isUser=1; 
 				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				$field_val=$_POST['field_val'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$tableClass='';
				// echo "<div id='title' style='display:none;'>{$table}</div>";
				// echo "<div class='titlePage' ><p>{$table_comment}</p></div>";
				// echo "<form action='save.php' method='post' id='form1' name='form1'>";
				//Δυναμική εμφάνιση επικεφαλίδων πεδίων
				/////////////Περνάμε σε πίνακες τα στοιχεία των πεδίων/////////////////////
				$count_cell=16;
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; //διαλέγουμε πίνακα
				$result = mysql_query($sql);
				$fieldInfo=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΕ ΤΙΣ ΠΛΗΡΟΦΟΡΙΕΣ
				$allFields=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΟΝΟ ΟΝΟΜΑΣΙΕΣ
				while($row = mysql_fetch_assoc($result))
				{				
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}
				//////////ΔΥΝΑΜΙΚΗ ΕΜΦΑΝΙΣΗ ΠΕΔΙΩΝ ΓΙΑ ΕΠΕΞΕΡΓΑΣΙΑ////////////////////////
				//echo "<table align='center' class='table{$tableClass}' cellpadding='0' cellspacing='0'>";
				//echo "<tr><td>Όνομα πεδίου</td><td>Τιμή</td></tr>";
				$results_field=array();
				//$sql_fields="SELECT ".implode(",",$allFields)." FROM {$table} where id={$field_val} ;";

				$sql_fields= ""
				."SELECT t.id,
					t.title,
					t.intro,
					t.description,
					t.abstractEL,
        			t.abstractEN,
					t.category,
					c.title as categoryTitle,
					t.professor,
					u.username as professorName,
					t.minstudent,
					t.maxstudent,
					t.datePublished,
					t.dateStart,
					t.datePresentation,
					t.dateDelivery,
					t.has_congres,
					t.congres_title,
					t.dateCreate,
					t.dateUpdate,
					t.isActive, 
					t.isCompleted,
					t.contactPerson,
					t.thesisDbReference,
					t.assignedTo,
					t.attachmentId,
					pi.path
					FROM thesis t 
					left join category c on c.id=t.category
					left join users u on u.id=t.professor
					left join pictureinfo pi on pi.id=t.attachmentId
					where t.id=" ."{$field_val}";

					$sql_categories = ""
					."SELECT
						c.id,
						c.title, 
						c.dateCreate, 
						c.dateUpdate,
						(CASE WHEN c.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive
					FROM category c";

					$sql_users= ""
					."SELECT 
					u.id,
					u.username ,
					u.nickname 
					from users u
					inner join usertype ut on ut.id=u.type 
					where u.isActive=1 and ut.nameType='Professor' ";

				$results_fields = mysql_query($sql_fields);
				$results_categories = mysql_query($sql_categories);
				$results_users = mysql_query($sql_users);
				//echo $sql_fields;
				if (!$results_fields) {
					echo ("Δεν υπάρχουν εγγραφές");
				}
				else
				{
					$rtb_num=0;
					$row = mysql_fetch_assoc($results_fields);
					echo ""
					.
				   "<form action='save.php' method='post' id='form1' name='form1'>
					      <table align='center' class='table' cellpadding='0' cellspacing='0'>
					         <tbody>
					         	<tr style='display:none;'>
					               <td>
					               <span class='field-name'>Κωδικός*</span>
					               </td>
					               <td class='data_input' style='display:none;'><input data-required='' id='id' name='id' type='text' value='".$row["id"]."' class='max input'>
					               </td>
				            	</tr>
								 <tr>
					               <td>
					               <span class='field-name'>Τίτλος*</span>
					               </td>
					               <td class='data_input'><input data-required='' id='title' name='title' type='text' value='".$row["title"]."' class='max input'>
					               </td>
					            </tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Intro Περιγραφή</span>
								<p></p>
								 	<textarea  style='float:right;' class='mceEditor tinymce' id='intro' name='intro'  >".$row["intro"]."</textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='intro'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='intro'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Περιγραφή*</span>
								<p></p>
								 	<textarea data-required='' style='float:right;' class='mceEditor tinymce' id='description' name='description'  >".$row["description"]."</textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='description'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='intro'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Abstract EL*</span>
								<p></p>
								 	<textarea data-required='' style='float:right;' class='mceEditor tinymce' id='abstractEL' name='abstractEL'  >".$row["abstractEL"]."</textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='abstractEL'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='intro'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
								<tr>
								<td colspan='2'>
								<span class='field-name'>Abstract EN*</span>
								<p></p>
								 	<textarea data-required='' style='float:right;' class='mceEditor tinymce' id='abstractEN' name='abstractEN'  >".$row["abstractEN"]."</textarea> 
									<div style='display:none;float:right;width:400px;text-align:right;'>
										<div class='dropzoneButton dropzoneGeneralButton' 
												data-upload-to-element-id='abstractEN'
												data-upload-to-element-what='img'
												data-upload-accepted-files='.png,.jpg,.bmp'
												data-upload-url='uploadFile.php'
												data-upload-dir='{$siteRoot}{$uploadRoot}{$imageRoot["normal"]}'
												data-upload-dirServer='{$siteHost}{$uploadHost}{$imageHost["normal"]}'
												data-upload-create-thumbs='true'
												data-upload-maxFilesize='2'>
												Επιλογή εικόνας
										</div>
										<div class='dropzoneButton dropzoneGeneralButton' 
											data-upload-to-element-id='intro'
											data-upload-to-element-what='href'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\data_interface\attachments\'
											data-upload-dirServer='{$siteHost}/data_interface/attachments/'
											data-upload-create-thumbs='true'
											data-upload-maxFilesize='5'>
											Επιλογή συννημένου
										</div>
									</div>
								</tr>
					            <tr>
									<td>
									<span class='field-name'>Κατηγορία*</span>
									</td>
									<td class='data_input'>
									<input data-required='' class='max foreign_value' id='category' name='category' type='text' value='".$row["category"]."'>
									<div class='foreignBox' data-ref-table='category'>
											<span class=''>".$row["categoryTitle"]."</span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
								</tr>
								<tr class='foreignBoxTr' data-reference-id='category' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='category' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Ονομασία</td>
														<td>Ημερομηνία δημιουργίας</td>
														<td>Ημερομηνία τελευταίας ενημέρωσης</td>
														<td>Ενεργό</td>
													</tr>";
										if (!$results_categories)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($categoryrow = mysql_fetch_assoc($results_categories))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$categoryrow["id"]."</td>
														<td class='click foreignHtml'>".$categoryrow["title"]."</td>
														<td class='click foreignHtml'>".$categoryrow["dateCreate"]."</td>
														<td class='click foreignHtml'>".$categoryrow["dateUpdate"]."</td>
														<td class='click foreignHtml'>".$categoryrow["isActive"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
					            <tr>
					               <td>
					               <span class='field-name'>Καθηγητής*</span>
					               </td>
					              <td class='data_input'>
									<input data-required='' class='max foreign_value' id='professor' name='professor' type='text' value='".$row["professor"]."'>
									<div class='foreignBox' data-ref-table='users'>
											<span class=''>".$row["professorName"]."</span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
					            </tr>
					            <tr class='foreignBoxTr' data-reference-id='professor' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='users' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Όνομα</td>
														<td>Τύπος</td>
													</tr>";
										if (!$results_users)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($usersrow = mysql_fetch_assoc($results_users))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$usersrow["id"]."</td>
														<td class='click foreignHtml'>".$usersrow["username"]."</td>
														<td class='click foreignHtml'>".$usersrow["nickname"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ελάχιστος αριθμός σπουδαστών*</span>
					               </td>
					               <td class='data_input'><input data-required='' id='minstudent' name='minstudent' type='text' value='".$row["minstudent"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Μέγιστος αριθμός σπουδαστών*</span>
					               </td>
					               <td class='data_input'>
					               <input data-required='' id='maxstudent' name='maxstudent' type='text' value='".$row["maxstudent"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ημερομηνία δημοσίευσης</span>
					               </td>
					               <td class='data_input'>
					                  <div class='dateButton input' data-return='datePublished'>Ημερομηνία</div>
					                  <input class='input right' value='".$row["datePublished"]."' id='datePublished' name='datePublished'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ημερομηνία ανάθεσης</span>
					               </td>
					               <td class='data_input'>
					                  <div class='dateButton input' data-return='dateStart'>Ημερομηνία</div>
					                  <input class='input right' value='".$row["dateStart"]."' id='dateStart' name='dateStart'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ημερομηνία παρουσίασης</span>
					               </td>
					               <td class='data_input'>
					                  <div class='dateButton input' data-return='datePresentation'>Ημερομηνία</div>
					                  <input class='input right' value='".$row["datePresentation"]."' id='datePresentation' name='datePresentation'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ημερομηνία παράδοσης</span>
					               </td>
					               <td class='data_input'>
					                  <div class='dateButton input' data-return='dateDelivery'>Ημερομηνία</div>
					                  <input class='input right' value='".$row["dateDelivery"]."' id='dateDelivery' name='dateDelivery'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Παρουσίαση σε συνέδριο</span>
					               </td>
					               <td class='data_input'>
					                  <select id='has_congres' name='has_congres'>";
				                     	echo "<option ";if ($row["has_congres"]==0)echo "selected='selected' ";echo "value='0'>Όχι</option>";		
										echo "<option ";if ($row["has_congres"]==1)echo "selected='selected' ";echo "value='1'>Ναι</option>";
										echo "".
					                  "</select>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Επιθυμητή Σύνδεση Με Δημοσίευση</span>
					               </td>
					               <td class='data_input'><input id='congres_title' name='congres_title' type='text' value='".$row["congres_title"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Συνημμένο</span>
					               </td>
					               <td class='data_input'>
					               <input style='display:none;' id='attachmentId' name='attachmentId' type='text' value='".$row["attachmentId"]."' class='max input'/>
					               <div class='attachmentId-filename'>
					               	<input class='max input' value='{$row["path"]}' disabled />
					               </div>
					               <div style='float:right;text-align:right;'>
					               <div class='dropzoneButton dropzoneGeneralButton '
											data-upload-to-element-id='attachmentId'
											data-upload-to-element-what='file'
											data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx, .zip'
											data-upload-url='uploadFile.php'
											data-upload-dir='{$siteRoot}\attachments\\".$userId."\'
											data-upload-dirServer='{$siteHost}/attachments/{$userId}/'
											data-upload-create-thumbs='false'
											data-upload-maxFilesize='10'>
											Επιλογή συννημένου
										</div>
									<div class='clear-attachment dropzoneGeneralButton".(empty($row["path"]) ? " hidden" :"")."' 
									data-inputid='attachmentId' 
									data-filename='attachmentId-filename'>Διαγραφή</div>	
									</div>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ενεργό*</span>
					               </td>
					               <td class='data_input'>
					                  <select data-required='' id='isActive' name='isActive'>";
				                     	echo "<option ";if ($row["isActive"]==0)echo "selected='selected' ";echo "value='0'>Όχι</option>";		
										echo "<option ";if ($row["isActive"]==1)echo "selected='selected' ";echo "value='1'>Ναι</option>";
										echo "".
					                  "</select>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ανατέθηκε στους</span>
					               </td>
					               <td class='data_input'><input id='assignedTo' name='assignedTo' type='text' value='".$row["assignedTo"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Contact Person</span>
					               </td>
					               <td class='data_input'><input id='contactPerson' name='contactPerson' type='text' value='".$row["contactPerson"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Σύνδεσμος Ολοκληρωμένης Πτυχιακής</span>
					               </td>
					               <td class='data_input'><input id='thesisDbReference' name='thesisDbReference' type='text' value='".$row["thesisDbReference"]."' class='max input'>
					               </td>
					            </tr>
					            <tr>
					               <td>
					               <span class='field-name'>Ολοκληρωμένη Πτυχιακή*</span>
					               </td>
					               <td class='data_input'>
					                  <select data-required='' id='isCompleted' name='isCompleted'>";
				                     	echo "<option ";if ($row["isCompleted"]==0)echo "selected='selected' ";echo "value='0'>Όχι</option>";		
										echo "<option ";if ($row["isCompleted"]==1)echo "selected='selected' ";echo "value='1'>Ναι</option>";
										echo "".
					                  "</select>
					               </td>
					            </tr>
					         </tbody>
					      </table>
					      <input id='table' name='table' value='{$table}' type='text' style='display:none'>
					      <input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'>
					      <input id='field_val' name='field_val' value='{$field_val}' type='text' style='display:none'>
					   </form>
					</div>"
				."";
				}
			?>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button update' data-button-type='action' data-post-url='update.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Ενημέρωση</span></div>
<!--			<div class='button saveas' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση ως</span></div>-->
		</div>
	</body>
</html>