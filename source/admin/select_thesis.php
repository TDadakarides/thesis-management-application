<?php
include "members.php";
include "type_of_tables.php";
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<?php
            $isUser=1;
            $whereUser= $isAdmin ?
            "LEFT JOIN users join_professor ON join_professor.id=t.professor " :
            "INNER JOIN users join_professor ON join_professor.id=t.professor AND join_professor.id={$userId}";
			$plithos_pediwn=8;
			$count_cell=16;

			include	'functions.php';
			include	'url_functions.php';

			$table=$_POST['table'];
			include	'database_include.php';
			$table_comment=$_POST['table_comment'];
			$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
			$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
			$sqlWhere=($foreignKey!="" && $foreignValue!="")?" WHERE {$foreignKey}={$foreignValue} ":"";
			if (isset($_POST['sort_asc'])) {$sort_asc=$_POST['sort_asc'];} else {$sort_asc="ASC";}
			if (isset($_POST['sorted'])) {$sort_new="ORDER BY {$_POST['sorted']} $sort_asc";$sort_by=$_POST['sorted'];} else {$sort_new="";$sort_by="";}
			

			echo "<div id='title' align='center' style='display:none;'>{$table}</div>";
			echo "<div class='titlePage'><p>{$table_comment}</p></div>";
			echo "<table  class='table' cellpadding='0' cellspacing='0'>";
			echo "".
			"<tr>".
			"
				<td>Α/Α</td>
				<td>Διαγραφή</td>
				<td>Επεξεργασία</td>
				<td>Φωτογραφίες</td>
				<td>Σχόλια Καθηγητή</td>
				<td>Αιτήσεις Σπουδαστών</td>
				<td>Λέξεις κλειδιά</td>
				<td data-field-sort='t.id' data-sort-by='asc' style='display:none;' class='sorted id'>Κωδικός</td>
				<td data-field-sort='t.title' data-sort-by='asc' class='sorted title'>Τίτλος</td>
				<td data-field-sort='join_category.title' data-sort-by='asc' class='sorted category'>Κατηγορία</td>
				<td data-field-sort='join_professor.nickname' data-sort-by='asc' class='sorted professor'>Καθηγητής</td>
				<td data-field-sort='t.minstudent' data-sort-by='asc' class='sorted minstudent'>Ελάχιστος αριθμός σπουδαστών</td>
				<td data-field-sort='t.maxstudent' data-sort-by='asc' class='sorted maxstudent'>Μέγιστος αριθμός σπουδαστών</td>
				<td data-field-sort='t.datePublished' data-sort-by='asc' class='sorted datePublished'>Ημερομηνία δημοσίευσης</td>
				<td data-field-sort='t.dateStart' data-sort-by='asc' class='sorted dateStart'>Ημερομηνία ανάθεσης</td>
				<td data-field-sort='t.datePresentation' data-sort-by='asc' class='sorted datePresentation'>Ημερομηνία παρουσίασης</td>"
			."</tr>";
			///////////////////////////////////////////////////
			$testsqlfinal= ""
			."SELECT 
				t.id AS id,
				(
					SELECT count(join_thesis_comments.id) AS professorCommentsCount 
					FROM thesis_comments join_thesis_comments 
					WHERE join_thesis_comments.thesisId=t.id
				)  AS professorCommentsCount,
				(
					SELECT count(join_student_comments.id) AS studentCommentsCount
					FROM student_comments join_student_comments 
					WHERE join_student_comments.thesisId=t.id
				)  AS studentCommentsCount,
				(
					SELECT count(join_keywords_thesis.id) AS keywordsCount 
					FROM keywords_thesis join_keywords_thesis 
					WHERE join_keywords_thesis.thesisId=t.id
				)  AS keywordsCount,
				t.title AS title, 
				join_category.title AS category, 
				join_professor.nickname AS professor,
				t.minstudent AS minstudent,
				t.maxstudent AS maxstudent,
				t.datePublished AS datePublished,
				t.dateStart AS dateStart,
				t.datePresentation AS datePresentation 
				FROM thesis t 
				LEFT JOIN category join_category ON join_category.id=t.category 
				{$whereUser}
				"
				." {$sqlWhere} {$sort_new} ";


			$results_field = mysql_query($testsqlfinal);
            $pagerTotalPages=7;
				//$fields_num = mysql_num_fields($results_field);
				$fields_rows = mysql_num_rows($results_field);
				$rowsPerPageValues=array(4,8,12,24,50);
				$page=(isset($_POST["page"])?$_POST["page"]:1);
				$viewall=($page?$page==0:false);
				$rowsCount=$fields_rows;
				$rowsPerPage=(isset($_POST["rowsPerPage"])?$_POST["rowsPerPage"]:$rowsPerPageValues[1]);
				$pageCount=ceil($rowsCount/$rowsPerPage);
				$sql="SELECT * FROM {$table} t {$sqlWhere} {$sort_new} LIMIT ".(($page-1)*$rowsPerPage).", {$rowsPerPage} ;";
				$testsqlfinal= $testsqlfinal ." LIMIT ".(($page-1)*$rowsPerPage).", {$rowsPerPage} ;";

        	
            $results_field = mysql_query($testsqlfinal);
			echo $testsqlfinal;
			if (!$results_field)
			{
				echo 
					"<tr class='table_name'>"
						."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
					."</tr>";
			}
			else
			{
				$indexRow=0;
				while($row = mysql_fetch_assoc($results_field))
				{
					$indexRow++;
					echo ""
					."<tr class='table_name'>
						<td class='a-a'>{$indexRow}</td>
						<td class='delete'>
							<div class='jsbutton delete icon-delete red-button' data-button-type='action' data-post-url='delete.php' data-post-data-type='data' data-post-data-value='{\"field_val\":\"".$row["id"]."\",\"table_name_eng\":\"thesis\",\"table_comment\":\"Διαγραφή\",\"foreign_key\":\"\",\"foreign_value\":\"\"}'></div>
						</td>
						<td class='edit'>
							<div class='jsbutton edit icon-pencil green-button' data-button-type='window' data-window-target='_this' data-post-url='edit_thesis.php' data-post-data-type='data' data-post-data-value='{\"field_val\":\"".$row["id"]."\",\"table_name_eng\":\"thesis\",\"table_comment\":\"Επεξεργασία\",\"foreign_key\":\"\",\"foreign_value\":\"\"}'></div>
						</td>
							<td class='photo minWidth'>
								<div class='jsbutton photo icon-image green-button' data-button-type='window' data-window-target='_this' data-post-url='edit_pic.php' data-post-data-type='data' data-post-data-value='{\"itemID\":\"".$row["id"]."\",\"table_name_eng\":\"thesis\",\"table_comment\":\"Φωτογραφίες\"}'></div>
								<span>".$row["picturesSum"]."</span>
							</td>
								<td class='minWidth'>
									<div class='jsbutton green-button icon-cog' data-button-type='window' data-window-target='_this' data-post-url='select_thesis_comments.php' data-post-data-type='data' data-post-data-value='{\"table\":\"thesis_comments\",\"table_comment\":\"Σχόλια Καθηγητή\",\"foreign_key\":\"thesisId\",\"foreign_value\":\"".$row["id"]."\"}'></div>
									<span>".$row["professorCommentsCount"]."</span>
								</td>
							</td>
								<td class='minWidth'>
									<div class='jsbutton green-button icon-cog' data-button-type='window' data-window-target='_this' data-post-url='select_student_comments.php' data-post-data-type='data' data-post-data-value='{\"table\":\"student_comments\",\"table_comment\":\"Αιτήσεις Σπουδαστών\",\"foreign_key\":\"thesisId\",\"foreign_value\":\"".$row["id"]."\"}'></div>
									<span>".$row["studentCommentsCount"]."</span>
								</td>
								<td class='minWidth'>
									<div class='jsbutton green-button icon-cog' data-button-type='window' data-window-target='_this' data-post-url='select_keywords_thesis.php' data-post-data-type='data' data-post-data-value='{\"table\":\"keywords_thesis\",\"table_comment\":\"Λέξεις κλειδιά\",\"foreign_key\":\"thesisId\",\"foreign_value\":\"".$row["id"]."\"}'></div>
									<span>".$row["keywordsCount"]."</span>
								</td>
									<td class='click' style='display:none;'>".$row["id"]."</td>
									<td class='click'>".$row["title"]."</td>
									<td class='click'>".$row["category"]."</td>
									<td class='click'>".$row["professor"]."</td>
									<td class='click'>".$row["minstudent"]."</td>
									<td class='click'>".$row["maxstudent"]."</td>
									<td class='click'>".$row["datePublished"]."</td>
									<td class='click'>".$row["dateStart"]."</td>
									<td class='click'>".$row["datePresentation"]."</td>
								</tr>"
					."";
				}

				
				if ($fields_rows==0) {
					echo "<tr class='table_name'>";
					echo "<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές..</td>";
					echo "</tr>";
				}
				if ($rowsCount>0){
					echo ""
					."<tr>"
					."<td colspan='{$count_cell}'>";
					echo ""
					."<span class='rows-total'>Εγγραφές {$indexRow}/{$rowsCount} | </span>"
					."<span class='page-span'>Σελίδα {$page}/{$pageCount}</span>";
					if( $pageCount<=$pagerTotalPages ){
						$pagerFirstPage=1;
						$pagerLastPage=$pageCount;
					}else if($page-floor($pagerTotalPages/2)<1 || $page+floor($pagerTotalPages/2)>$pageCount ){
						if($page-floor($pagerTotalPages/2)<1){
							$pagerFirstPage=1;
							$pagerLastPage=$pagerTotalPages;
						}else if($page+floor($pagerTotalPages/2)>$pageCount){
							$pagerFirstPage=$pageCount-$pagerTotalPages+1;
							$pagerLastPage=$pageCount;
						}
					}
					else{
						$pagerFirstPage=$page-floor($pagerTotalPages/2);
						$pagerLastPage=$page+floor($pagerTotalPages/2);
					}
					
					echo ""
					."<div class='pager-box right'>"
					."<select class='page-select input' style='float:none;'>";
					foreach($rowsPerPageValues as $perPage){
					echo ""
						."<option".($perPage==$rowsPerPage?" selected='selected'":"")." value='{$perPage}'>{$perPage}</option>";
					}
					echo ""
					."</select>";
					if(1!=$pagerFirstPage){
						echo ""
						."<span data-page='1' class='input page-index'>1</span>";						
					}
					for($pageIndex=$pagerFirstPage;$pageIndex<=$pagerLastPage;$pageIndex++){
						echo ""
						."<span data-page='{$pageIndex}' class='input page-index".($pageIndex==$page?" active":"")."'>{$pageIndex}</span>";
					}
					if($pageCount!=$pagerLastPage){
						echo ""
						."<span data-page='{$pageCount}' class='input page-index'>{$pageCount}</span>";						
					}
					echo ""
					."</div>"
					."</td>"
					."</tr>"
					;
				}
				mysql_free_result($results_field);
			}
		?>
		</table>		
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button new' data-button-type='window' data-window-target='_this' data-post-url='new_thesis.php'data-post-data-type='data' data-post-data-value='{"table_name_eng":"<?=$table;?>","table_comment":"<?=$table_comment;?>","foreign_key":"<?=$foreignKey;?>","foreign_value":"<?=$foreignValue;?>"}'><div class='innerbutton'></div><div class='image'></div><span>Προσθήκη</span></div>
		</div>
	</body>
</html>