    <?php
    include_once "members.php";
    ?>
    <html>
        <head>
            <?=$company["shortHead"];?>
        </head>
        <body>
            <div class='bodyBackground'></div>
            <?php
                include	'functions.php';
                include	'database_include.php';
                echo ""
                ."<div class='titleGeneral'>"
                    ."<span></span>"
                    ."<div class='clearDiv'></div>"
                ."</div>"
                    ."<div class='mainMenu' >";
                        if ($isAdmin){
                            echo ""
                            ."<div class='button-like icon-user'><span>Λογαριασμοί<span/>"
                            ."<ul>"
                            ."<li>"
                            ."<div class='userTables button icon-user' data-button-type='window' data-window-group-name='userTablesGroup' data-window-target='_this' data-post-url='select_users.php' data-post-data-type='data' data-post-data-value='{\"ai\":\"id\",\"ai_value\":\"{$userId}\",\"table\":\"{$userTable}\",\"table_comment\":\"Λογαριασμοί Καθηγητών\"}'>Καθηγητές</div>"
                            ."</li>"
                            ."<li>"
                            ."<div class='userTypeTables button icon-user' data-button-type='window' data-window-group-name='userTypeTablesGroup' data-window-target='_this' data-post-url='select_usertypes.php' data-post-data-type='data' data-post-data-value='{\"table\":\"{$userTypeTable}\",\"table_comment\":\"Κατηγορίες λογαριασμών Καθηγητών\"}'>Ρόλοι</div>"
                            ."</li>"
                            ."</ul>"
                            ."</div>"

                            ."<div class='regionThesis button icon-cog' data-button-type='window' data-window-group-name='regionThesis' data-window-target='_this' data-post-url='select_thesis.php' data-post-data-type='data' data-post-data-value='{\"table\":\"thesis\",\"table_comment\":\"Πτυχιακή\"}'>Πτυχιακή</div>"

                            ."<div class='regionCategories button icon-cog' data-button-type='window' data-window-group-name='regionCategories' data-window-target='_this' data-post-url='select_category.php' data-post-data-type='data' data-post-data-value='{\"table\":\"category\",\"table_comment\":\"Positions\"}'>Κατηγορίες Θεμάτων</div>"

                            ."<div class='regionArticles button icon-cog' data-button-type='window' data-window-group-name='regionArticles' data-window-target='_this' data-post-url='select_articles.php' data-post-data-type='data' data-post-data-value='{\"table\":\"dynamic_articles\",\"table_comment\":\"Στατικές Σελίδες\"}'>Στατικές Σελίδες</div>"

                            ."<div class='regionKeywords button icon-cog' data-button-type='window' data-window-group-name='regionKeywords' data-window-target='_this' data-post-url='select_keywords.php' data-post-data-type='data' data-post-data-value='{\"table\":\"keywords\",\"table_comment\":\"Λέξεις Κλειδιά\"}'>Λέξεις Κλειδιά</div>";
                        }
                        else{
                            echo "<div class='regionThesis button icon-cog' data-button-type='window' data-window-group-name='regionThesis' data-window-target='_this' data-post-url='select_thesis.php' data-post-data-type='data' data-post-data-value='{\"table\":\"thesis\",\"table_comment\":\"Πτυχιακή\"}'>Πτυχιακή</div>"

                            ."<div class='regionCategories button icon-cog' data-button-type='window' data-window-group-name='regionCategories' data-window-target='_this' data-post-url='select_category.php' data-post-data-type='data' data-post-data-value='{\"table\":\"category\",\"table_comment\":\"Positions\"}'>Κατηγορίες Θεμάτων</div>"

                            ."<div class='regionKeywords button icon-cog' data-button-type='window' data-window-group-name='regionKeywords' data-window-target='_this' data-post-url='select_keywords.php' data-post-data-type='data' data-post-data-value='{\"table\":\"keywords\",\"table_comment\":\"Λέξεις Κλειδιά\"}'>Λέξεις Κλειδιά</div>";
                        }
                        echo ""
                        ."<ul class='userBox'>"
                            ."<li class='userNickname icon-user'>{$nickname}</li>"
                            ."<li>"
                                ."<ul>"
                                    ."<li><div class='logout button' data-button-type='window' data-window-target='_self' data-post-url='index.php' data-post-data-type='data' data-post-data-value='{ \"logout\":\"1\" }'>Αποσύνδεση</div></li>"
                                ."</ul>"
                            ."</li>"
                        ."</ul>"
                    ."</div>"
                ."<div class='allwindowBoxes'></div>"
                ."";
            ?>
       </body>
    </html>