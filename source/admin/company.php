<?php
	$userTable="users";
	$userTypeTable="usertype";
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
        
        $uploadRoot="\uploads\\";
        $uploadHost="/uploads/";
        $imageRoot=array("normal"=>"images\\","thumb"=>"images_thumbs\\","thumb_500"=>"images_thumbs_500\\");
        $imageHost=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageSizes=array("normal"=>0,"thumb"=>150,"thumb_500"=>500);
        $fileRoot="files\\";
        $fileHost="files/";
    }else{
        $uploadRoot="/uploads/";
        $uploadHost="/uploads/";
        $imageRoot=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageHost=array("normal"=>"images/","thumb"=>"images_thumbs/","thumb_500"=>"images_thumbs_500/");
        $imageSizes=array("normal"=>0,"thumb"=>150,"thumb_500"=>500);
        $fileRoot="files/";
        $fileHost="files/";    
    }
	$company=array();
	$company["name"]="Diloma Application";
	$company["title"]="Welcome to Diploma Select Application | TEI of PEIRAEUS";
	$company["logo"]="sima.png";
	$company["icon"]="sima.gif";
	$company["fullHead"]="Welcome to Diploma Select Application | TEI of PEIRAEUS";
	$company["shortHead"]="".
	"<META name='GENERATOR' Content='Microsoft FrontPage 4.0'></META>".
	"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'></meta>".
	"<title>{$company['title']}</title>".
	"<link rel='shortcut icon' href='webImages/{$company['icon']}'/>".
	"<link rel='stylesheet' type='text/css' href='css/datain.css'>".
	"<link rel='stylesheet' type='text/css' href='js/colocPicker/css/colpick.css'>".
	"<script src='js/jquery.min.js'></script>".
	"<script src='js/jquery-ui.js'></script>".
	"<script src='js/functions.js'></script>".
	"<script src='js/dropzone.js'></script>".
	"<script src='js/dropzonePlugin.js'></script>".
	"<script src='js/googleMap.js'></script>".
	"<script src='js/buttons.js?v=2'></script>".
	"<script src='js/tinymce/js/tinymce/tinymce.min.js'></script>".
	"<script src='js/colocPicker/js/colpick.js'></script>".
	"<script src='js/data-interface.js'></script>".
	"<script src='js/picture.js'></script>".
	"<script src='js/newsletter.js'></script>".
	//"<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyB6VvkAVpJLEkpYfcJNCZRYmSP5y8EMVMA&sensor=false&location='></script>".
//	"<script async defer src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyANoCcAl5oKts1f2Le3h83Bce5ODKW7u3Q'></script>".
	//"<script src='https://maps.gstatic.com/maps-api-v3/api/js/20/4/intl/el_ALL/main.js'></script>".
	//"<script src='https://maps.gstatic.com/maps-api-v3/api/js/20/4/intl/el_ALL/places.js'></script>".
	"<script>$(document).ready(function () { runDrop();dropzoneInit('{$serverHost}') });</script>".
	"<script>var serverHost='{$serverHost}';</script>".
	"";
?>
