<?php
include "members.php"; 
include "fkInit.php";
include "functions_database.php";
?>
<html>
	<head>
	</head>
	<body>
		<div class='windowTitle' >Επεξεργασία Πτυχιακης</div>
			<?php
                $isUser=1; 
				include	'functions.php';
				include	'database_include.php';
				date_default_timezone_set('Europe/Athens');
				$table=$_POST['table_name_eng'];
				$table_comment=$_POST['table_comment'];
				$field_val=$_POST['field_val'];
				$foreignKey=isset($_POST['foreign_key'])?$_POST['foreign_key']:"";
				$foreignValue=isset($_POST['foreign_value'])?$_POST['foreign_value']:"";
				$tableClass='';
				// echo "<div id='title' style='display:none;'>{$table}</div>";
				// echo "<div class='titlePage' ><p>{$table_comment}</p></div>";
				// echo "<form action='save.php' method='post' id='form1' name='form1'>";
				//Δυναμική εμφάνιση επικεφαλίδων πεδίων
				/////////////Περνάμε σε πίνακες τα στοιχεία των πεδίων/////////////////////
				$count_cell=16;
				$sql = "SHOW FULL COLUMNS FROM ".$table." ;"; //διαλέγουμε πίνακα
				$result = mysql_query($sql);
				$fieldInfo=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΕ ΤΙΣ ΠΛΗΡΟΦΟΡΙΕΣ
				$allFields=array();///ΟΛΑ ΤΑ ΠΕΔΙΑ ΜΟΝΟ ΟΝΟΜΑΣΙΕΣ
				while($row = mysql_fetch_assoc($result))
				{				
					$fieldInfo[$row["Field"]]=$row;
					array_push($allFields,$row["Field"]);
				}
				//////////ΔΥΝΑΜΙΚΗ ΕΜΦΑΝΙΣΗ ΠΕΔΙΩΝ ΓΙΑ ΕΠΕΞΕΡΓΑΣΙΑ////////////////////////
				//echo "<table align='center' class='table{$tableClass}' cellpadding='0' cellspacing='0'>";
				//echo "<tr><td>Όνομα πεδίου</td><td>Τιμή</td></tr>";
				$results_field=array();
				//$sql_fields="SELECT ".implode(",",$allFields)." FROM {$table} where id={$field_val} ;";

				$sql_fields= ""
				."SELECT 
					t.id,
					t.thesisId,
					tt.title ,
					t.keyword as keywordId,
					k.keyword 
					FROM keywords_thesis t
					LEFT JOIN keywords k on t.keyword=k.id
					LEFT JOIN thesis tt ON tt.id=t.thesisId 
					WHERE t.id=" ."{$field_val}";

					$sql_thesis = ""
					."SELECT 
							t.id,
							t.title,
							t.intro,
							t.description,
                            t.category,
							c.title as categoryTitle,
							t.professor,
                            u.nickname,
							t.minstudent,
							t.maxstudent,
							t.datePublished,
							t.dateStart,
							t.datePresentation,
							t.dateDelivery,
							(CASE WHEN t.has_congres=1 THEN 'Ναι' ELSE 'Όχι' END) AS hasCongres,
							t.congres_title,
							t.dateCreate,
							t.dateUpdate,
							(CASE WHEN t.isActive=1 THEN 'Ναι' ELSE 'Όχι' END) AS isActive
							FROM thesis t
                            LEFT JOIN category c on t.category = c.id
                            LEFT JOIN users u on  t.professor = u.id";

					$sql_keywords= ""
					."SELECT
						k.id,
						k.keyword
						from keywords k";

				$results_fields = mysql_query($sql_fields);
				$results_thesis = mysql_query($sql_thesis);
				$results_keywords = mysql_query($sql_keywords);
				//echo $sql_fields;
				if (!$results_fields) {
					echo ("Δεν υπάρχουν εγγραφές");
				}
				else
				{
					$rtb_num=0;
					$row = mysql_fetch_assoc($results_fields);
					echo ""
					.
				   "<form action='save.php' method='post' id='form1' name='form1'>
					      <table align='center' class='table' cellpadding='0' cellspacing='0'>
					         <tbody>
					         	<tr style='display:none;'>
					               <td>
					               <span class='field-name'>Κωδικός*</span>
					               </td>
					               <td class='data_input' style='display:none;'><input data-required='' id='id' name='id' type='text' value='".$row["id"]."' class='max input'>
					               </td>
				            	</tr>
					            
					            <tr>
					               <td>
					               <span class='field-name'>Λέξη Κλειδί*</span>
					               </td>
					              <td class='data_input'>
									<input data-required='' class='max foreign_value' id='keyword' name='keyword' type='text' value='".$row["keywordId"]."'>
									<div class='foreignBox' data-ref-table='keyword' >
											<span class=''>".$row["keyword"]."</span>
											<div class='foreignButton' style='cursor:pointer;'>+</div>
											<div style='clear:both;display:block;'></div>
									</div>
									</td>
					            </tr>
					            <tr class='foreignBoxTr' data-reference-id='keyword' style='display: none;'>
									<td colspan='2'>
										<div class='searchForeignBox' style='display:block;'>
											<input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'>
											<div style='clear:both;'></div>
										</div>
										<div data-ref-table='keyword' class='foreignHtmlBox'>
										 	<table class='table' cellpadding='0' cellspacing='0'>
												<tbody>
													<tr class='heading'>
														<td class='hidden'>Κωδικός</td>
														<td>Λέξη Κλειδί</td>
													</tr>";
										if (!$results_keywords)
										{
											echo 
												"<tr class='table_name'>"
													."<td class='' colspan='{$count_cell}' >Δεν υπάρχουν εγγραφές</td>"
												."</tr>";
										}
										else
										{
											while($keywordsrow = mysql_fetch_assoc($results_keywords))
											{
												echo ""
												."
													<tr>
														<td class='click foreignKey'>".$keywordsrow["id"]."</td>
														<td class='click foreignHtml'>".$keywordsrow["keyword"]."</td>
													</tr>"
													."";
											}
										}
											echo "".
												"</tbody>
											</table>
										</div>
									</td>
								</tr>
					         </tbody>
					      </table>
					      <input id='table' name='table' value='{$table}' type='text' style='display:none'>
					      <input id='table_comment' name='table_comment' value='{$table_comment}' type='text' style='display:none'>
					      <input id='field_val' name='field_val' value='{$field_val}' type='text' style='display:none'>
					   </form>
					</div>"
				."";
				}
			?>
		<div class="submenu" >
			<div class='button back' data-button-type='back' ><div class='innerbutton'></div><div class='image'></div><span>Πίσω</span></div>
			<div class='button update' data-button-type='action' data-post-url='update.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Ενημέρωση</span></div>
<!--			<div class='button saveas' data-button-type='action' data-post-url='save.php' data-post-data-type='form' ><div class='innerbutton'></div><div class='image'></div><span>Αποθήκευση ως</span></div>-->
		</div>
	</body>
</html>