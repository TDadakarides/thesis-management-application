$('.searchDropzone').on('keyup',function ()
{
	var searchVal=$(this).val();
	$.each( $('.thumbs_f .thumbName'), function ()
	{
		if ( $(this).text().toLowerCase().replace(/\s+/g, '').indexOf(searchVal.replace(/\s+/g, '').toLowerCase()) == -1)
			$(this).closest('.thumbs_f').hide();
		else
			$(this).closest('.thumbs_f').show();
	});
});
$('.thumbs_f,.zoomBoxDropzone').disableSelection();
$('.commentTextarea.input').on('keyup',function ()
{
	$('.thumbs_f.last_active').attr('data-comment',$(this).val());
});
$('.license-picture.input').on('change',function ()
{
	$('.thumbs_f.last_active').attr('data-license',$(this).val());
});
///small normal medium large
$('.zoomBoxDropzone .zooms.zoomin').on('click',function ()
{
	if ($(this).hasClass('disable')) return;
	if ( $('.thumbs_f').first().hasClass('small') ) { $('.thumbs_f').addClass('normal').removeClass('small');$(this).removeClass('disable');$('.zoomBoxDropzone .zooms.zoomout').removeClass('disable'); }
	else if ( $('.thumbs_f').first().hasClass('normal') ) { $('.thumbs_f').addClass('medium').removeClass('normal');$(this).removeClass('disable');$('.zoomBoxDropzone .zooms.zoomout').removeClass('disable'); }
	else if ( $('.thumbs_f').first().hasClass('medium') ) { $('.thumbs_f').addClass('large').removeClass('medium');$(this).addClass('disable');$('.zoomBoxDropzone .zooms.zoomout').removeClass('disable'); }
});
$('.zoomBoxDropzone .zooms.zoomout').on('click',function ()
{
	if ($(this).hasClass('disable')) return;
	if ( $('.thumbs_f').first().hasClass('large') ) { $('.thumbs_f').addClass('medium').removeClass('large');$(this).removeClass('disable');$('.zoomBoxDropzone .zooms.zoomin').removeClass('disable'); }
	else if ( $('.thumbs_f').first().hasClass('medium') ) { $('.thumbs_f').addClass('normal').removeClass('medium');$(this).removeClass('disable');$('.zoomBoxDropzone .zooms.zoomin').removeClass('disable'); }
	else if ( $('.thumbs_f').first().hasClass('normal') ) { $('.thumbs_f').addClass('small').removeClass('normal');$(this).addClass('disable');$('.zoomBoxDropzone .zooms.zoomin').removeClass('disable'); }
});
$('.thumbs_f').on('click', function ()
{
	if (!$(this).hasClass('active')){
		$('.thumbs_f.last_active').removeClass('last_active');
		$(this).addClass('last_active');
		$('.commentTextarea.input').val($(this).attr('data-comment'));
		$('.license-picture.input').val($(this).attr('data-license'));
		
		if (returnElementType=='background' ){
			$('.thumbs_f').removeClass('active');
		}
		$(this).addClass('active');
		$('.addPhoto').removeClass('disable');
		$('.updateImageInfoText').text("");
		
	}
	else{
		$('.thumbs_f.last_active').removeClass('last_active');
		$(this).addClass('last_active');
		$('.commentTextarea.input').val($(this).attr('data-comment'));
		$('.license-picture.input').val($(this).attr('data-license'));
		$(this).removeClass('active');
		if ($('.thumbs_f.active').length==0)
		{
			$('.addPhoto').addClass('disable');
		}
	}
	setSelectedThumbs();
});
$('.cancelAll').on('click', function ()
{
	if ($(this).hasClass('disable')) return;
	//dropzone = parent.$('.dropzone-previews').first();
	//dropzone.hide().appendTo( parent.$('#userUpdate') );
	//parent.$('.blackbox').hide();
	//parent.$('body,html').css('position','');
	parent.$('.blackbox.upload').remove();

});
$('.addPhoto').on('click', function ()
{
	if ($(this).hasClass('disable')) return;
	$('.thumbs_file .thumbs_f').hide();
	loadingBox=$("<div class='loadingBox'><img src='webImages/loading.gif' /></div>");
	$('.thumbs_file').append( loadingBox );
	$('.addPhoto,.cancelAll').addClass('disable');
	_url=[];
	comment=[];
	$('.thumbs_f.active').each(function (){
		//_url.push( $(this).attr('data-src') );
		_url.push( $(this).attr('data-id') );
		comment.push( $(this).attr('data-comment') );
	});
	if (returnElementType=='background' ){
		var selectedId=$('.thumbs_f.active').attr('data-id');
		var selectedSrc=$('.thumbs_f.active').attr('data-src');
		returnElement.val(selectedId);
		returnElement.closest(".data_input").find(".upload_image")
		.css('background-image' ,"url('"+uploadHostImage+selectedSrc+"')" )
		.addClass("hasPhoto");
		// windowBox.find('form').remove();
		//top.loadWindow(indexWindow) ;
		//reloadWindows();
		top.$('.blackbox.upload').remove();
	}
	if (returnElementType=='file' ){
		var selectedId=$('.thumbs_f.active').attr('data-id');
		var selectedSrc=$('.thumbs_f.active').attr('data-src');
		var selectElement=top.$('.'+ returnElement.attr('id') + '-filename input');
		selectElement.val(selectedSrc);
		returnElement.val(selectedId);
		returnElement.closest(".data_input").find(".upload_image")
		.addClass("hasPhoto");
		// windowBox.find('form').remove();
		//top.loadWindow(indexWindow) ;
		//reloadWindows();

		returnElement.closest(".data_input").find(".clear-attachment").removeClass('hidden');
		top.$('.blackbox.upload').remove();
	}
	if (returnElementType=='gallery' ){
		var windowBox=returnElement.closest('.windowBox');
		indexWindow=windowBox.attr('window-index');
		values=JSON.parse( windowBox.attr('data-post-data-value') );
		urlPost='save_pic.php';
		$.post( urlPost , { _url: _url, itemID : values.itemID, table_name_eng : values.table_name_eng , table_comment:values.table_comment } )
		.done( function ( dataResponse )
		{
			//returnElement.find().attr('src' ,returnElement.attr('src') );
				//windowBox.find('form').remove();
				//top.loadWindow(indexWindow) ;
				var dataResponseJson=$.parseJSON(dataResponse);
				var isSuccessed=(dataResponseJson.success!=null);
				var dataMsg;
				if(isSuccessed){
					dataMsg=dataResponseJson.success;
				}else{
					dataMsg=dataResponseJson.error;
				}
				if (top.$('.bgblack').length>0){top.$('.bgblack').remove();}
				top.$('.dataMsg .loading').remove();
				top.$('.dataMsg .msgBox').html( "<p>"+dataMsg+"</p>" ).prepend("<div class='closeButton'>X</div>");
				top.reloadWindows();
				top.$('.dataMsg').fadeOut(6000,function () { top.$('.dataMsg').remove(); });
				top.$('.blackbox.upload').remove();
		});
	}
});
$('.updateImageInfo').on("click",function(){
	var id=$('.thumbs_f.last_active').data("license");
	var comment=$('.thumbs_f.last_active').data("comment");
	var license=$('.thumbs_f.last_active').data("license");
	var path=$('.thumbs_f.last_active').data("src");
	var urlPost='save_pic_info.php';
	$('.updateImageInfoText').show().text("Saving..");
	$.post( urlPost , {  path:path, comment:comment, id : id, license : license } )
	.done( function ( dataResponse )
	{
		var dataResponseJson=$.parseJSON(dataResponse);
		var isSuccessed=(dataResponseJson.success!=null);
		var dataMsg;
		if(isSuccessed){
			dataMsg=dataResponseJson.success;
		}else{
			dataMsg=dataResponseJson.error;
		}
		if(isSuccessed) {
			$('.updateImageInfoText').addClass("green").text(dataMsg).delay( 2200 ).fadeOut( 400,function(){$(this).removeClass("green");} );
		}else{
			$('.updateImageInfoText').addClass("red").text(dataMsg).delay( 4000 ).fadeOut( 400,function(){$(this).removeClass("red");}  );
		}
	})
	.fail( function ( dataResponse )
	{
		$('.updateImageInfoText').addClass("red").text("Error Saving").delay( 4000 ).fadeOut( 400,function(){$(this).removeClass("red");}  );
	});
});

function setSelectedThumbs()
{
	selectedThumbs=$('.thumbs_f.active').length;
	allThumbs=$('.thumbs_f').length;
	if (selectedThumbs<=0) { selectedThumbsInfo='0 / '+allThumbs+' picture selected '; }
	else if (selectedThumbs==1) { selectedThumbsInfo='1 / '+allThumbs+' picture selected '; }
	else { selectedThumbsInfo=selectedThumbs+' / '+allThumbs+' pictures selected '; }
	$('.selectedThumbsInfo').html(selectedThumbsInfo)
}
function initFileList(){
	$('.thumbs_f').first().addClass('last_active');
	setSelectedThumbs();
	//parent.$('input.dz-hidden-input').remove();
}