function dropzoneInit(serverHost)
{
	$('.dropzone').on('hover',
	function ()
	{
		$(this).addClass('hover');
	},
	function ()
	{
		$(this).removeClass('hover');
	}
	);
	$('.dropzoneButton').on('click',function ()
	{
		if ( $('.blackbox.upload').length>0) $('.blackbox.upload').remove();
		returnToElementId=$(this).attr('data-upload-to-element-id');
		returnToElementWhat=$(this).attr('data-upload-to-element-what');//////background - valuePicture -valueFile - img για mediumtext - href για mediumtext
		elementCurrentValue=$(this).attr('data-upload-to-element-current-value');//////background - valuePicture -valueFile - img για mediumtext - href για mediumtext
		galleryFileUrl=serverHost+'/fileList.php';
		acceptedFiles=$(this).attr('data-upload-accepted-files');
		uploadUrl=$(this).attr('data-upload-url');
		dir=$(this).attr('data-upload-dir');
		createThumbs=$(this).attr('data-upload-create-thumbs');
		uploadImageDir=$(this).attr('data-upload-dirServer');
		dirShortPath=dir;
		dirShortPath=uploadImageDir.replace(serverHost+'/', '');
		return_name=( typeof $(this).attr('data-upload-return-name')!='undefined')?$(this).attr('data-upload-return-name'):'pic_return_name';
		maxFilesize=( typeof $(this).attr('data-upload-maxFilesize')!='undefined')?$(this).attr('data-upload-maxFilesize'):6;
		maxFilesize=6;
		if (uploadImageDir=='') uploadImageDir=serverHost+"\data_interface\images\\";
		blackbox=$("<div class='blackbox upload'>"
					+"<div class='closeButton'>&#10005;</div>"
					+"<div class='uploadbox-container'>"
					+"<iframe class='upload-box' data-upload-to-element-id='"+returnToElementId+"' id='emailBody_iframe' src='"+galleryFileUrl+"?field="+elementCurrentValue+'&'+return_name+"="+returnToElementId+"&returnToElementWhat="+returnToElementWhat+"&dir="+dir+"&acceptedFiles="+acceptedFiles+"'></iframe>"
					+"</div>"
					+"</div>");
		//blackbox=$("<div class='blackbox upload'></div>");
		if ( $('#userDropzone').length>0 ) $('#userDropzone').remove();
		dropzone = $("<form id='userDropzone' class='dropzone' enctype='multipart/form-data' ></form>");
		dropzone.dropzone(
		{ // The camelized version of the ID of the form element

			// The configuration we've talked about above
			//previewsContainer: '.img-preview',
			url:uploadUrl,
			dir:dir,
			createThumbs:createThumbs,
			thumbnailWidth: '280',
			thumbnailHeight: null,
			maxThumbnailFilesize: '50',
			uploadMultiple: true,
			parallelUploads: 100,
			maxFilesize: maxFilesize,
			acceptedFiles:acceptedFiles,
			autoProcessQueue:false,
			dictDefaultMessage:'<p>Σύρεται τη φωτογραφία σας μέσα στο κουτί για μεταφόρτωση ή πατήστε εδώ.</p><p style="word-wrap: break-word;"><b>τύπος αρχείου</b><br/>'+acceptedFiles+'<br/><b>μέγιστο μέγεθος</b><br/>'+maxFilesize+' ΜΒ</p>' ,
			dictFileTooBig: 'Το μέγεθος του αρχείου είναι πολύ μεγάλο.Επιλέξτε άλλο αρχείο.',
			dictInvalidFileType:'Μόνο αρχεία '+acceptedFiles+' επιτρέπονται.' ,
			dictMaxFilesExceeded:'Μόνο ένα αρχείο μπορεί να μεταφορτωθεί.' ,
			dictRemoveFile:'Διαγραφή',

			// The setting up of the dropzone
			init: function() 
			{
				myDropzone = this;
				myDropzone.options.url=uploadUrl+'?acceptedFiles='+acceptedFiles+'&maxFilesize='+maxFilesize+'&dir='+dir+'&createThumbs='+createThumbs;
				myDropzone.fileIndex=0;
				myDropzone.on('dragover', function(file)
				{
					$('.dropzone').addClass('hover');
				});
				myDropzone.on('dragleave', function(file)
				{
					$('.dropzone').removeClass('hover');
				});
				myDropzone.on('drop', function(e)
				{
					myDropzone.fileIndex=0;
					this.files=[];
				});
				myDropzone.on('error', function(file)
				{
					if ( $('.dz-error').length > 1 ) $('.dz-error:not(:last-child)').remove();
					this.droppedFiles=0;
				});
				myDropzone.on('addedfile', function(file)
				{
					$('.dropzone').removeClass('hover');
					filename=file.name;
					newNames=$("<input class='newnames' type='text' data-index='"+myDropzone.fileIndex+"' name='newnames[]' value='"+filename+"' />");
					dropzone.append(newNames);
					if ( $('.dz-preview').length > 1 ) $('.dz-preview:not(:last-child)').remove();
					myDropzone.fileIndex++;
					console.log('myDropzone.fileIndex:'+myDropzone.fileIndex);
					console.log('(myDropzone.files).length:'+(myDropzone.files).length);
					if (myDropzone.fileIndex==(myDropzone.files).length)
					{
						//run function
						console.log('run checkFile');
						checkFile(myDropzone,0);
					}
				});
				myDropzone.on('processing', function(file)
				{
					//alert('processing');
					$('.dz-upload').width('0').css('display','block').show();
					$('.dz-progress').show();
					if ( $('.dz-preview').length > 1 ) $('.dz-preview:not(:last-child)').remove();
				});
				myDropzone.on('success', function(file)
				{
					myDropzone.fileIndex=0;
					dropzone.find('.newnames').remove();
					//alert('success');
					returnToElementId=$('#emailBody_iframe').attr('data-upload-to-element-id');
					$('.dz-upload').css('display','block').hide();
					$('.dz-progress').hide();
					$('#logotype').val( $.trim(event.target.responseText) );
					$('.logotypePreview').attr('src','' );						
					$('.logotypePreview').attr('src', $.trim(event.target.responseText)+'?'+Math.floor((Math.random() * 10000) + 1) );
					$('#emailBody_iframe').attr('src', galleryFileUrl+"?newField="+filename+'&'+return_name+"="+returnToElementId+"&returnToElementWhat="+returnToElementWhat+"&dir="+dir +"&acceptedFiles="+acceptedFiles);
					elementCurrentValue=filename;
				});
			}
		}											
		);
		$('body,html').css('position','relative');
		dropzone.appendTo( $('.uploadbox-container',blackbox) ).show();
		blackbox.appendTo( 'body' );
		blackbox.show();
		///////////INITIALIZE DROPZONE  /////////////
		var filename;
		function checkFile(myDropzone,index)
		{
		
					console.log('index:'+index);
					console.log('(myDropzone.files).length:'+(myDropzone.files).length);
			myDropzone.checkFile=true;
			filename=myDropzone.files[index].name;
			/////τσεκαρω αν υπαρχει το αρχειο με αυτη την ονομασια
			$.ajax({type: 'HEAD',url: uploadImageDir+filename,async:false,
				/////το αρχειο υπαρχει
				success: function()
				{
					if ( $('.blackbox.confirm').length>0) $('.blackbox.confirm').remove();
					blackbox=$("<div class='blackbox confirm'><div id='confirmBox'><div class='message'></div><span class='yes confirmButton'>Αντιγραφή</span><span class='cancel confirmButton center'>Ακύρωση</span><span class='no confirmButton right'>Μετονομασία</span></div></div>");
					blackbox.appendTo('body').show();													
					doConfirm("<p>Το αρχείο &#60; "+uploadImageDir+filename+" &#62; που επιλέξατε να ανεβάσετε υπάρχει ήδη στο server.</p>",
					function yes()
					{
						if (index<(myDropzone.files).length-1) {checkFile(myDropzone,++index);}else { setTimeout(function () { myDropzone.processQueue(); },500); }
					},
					function cancel()
					{
						dropzone.find("input[data-index='"+index+"']").val("");
						if ( $('.blackbox.confirm').length>0) $('.blackbox.confirm').remove();
						if (index<(myDropzone.files).length-1) {checkFile(myDropzone,++index);}else { setTimeout(function () { myDropzone.processQueue(); },500); }
					},
					function no()
					{
						newFilename=(checkName(filename,1));
						console.log("newFilename("+(index)+"):"+newFilename);
						dropzone.find("input[data-index='"+index+"']").val(newFilename);
						if (index<(myDropzone.files).length-1) {checkFile(myDropzone,++index);}else { setTimeout(function () { myDropzone.processQueue(); },500); }
						
					});
				}
				,error: function()
				{
					if (index<(myDropzone.files).length-1) {checkFile(myDropzone,++index);}else { setTimeout(function () { myDropzone.processQueue(); },500); }													
				}
			});

		}
		function checkName(filename,num)
		{
			if (typeof num=='undefined') {extraFilename="";num=1;} else {extraFilename="("+num+")";}
			var fileExt = filename.split('.').pop();
			var fileWithoutExt = filename.substr(0, filename.lastIndexOf('.')) || filename;
			tempFilename=uploadImageDir+fileWithoutExt+extraFilename+"."+fileExt;
			$.ajax({type: 'HEAD',url: tempFilename,async:false,success: function(){ num++;newFilename=checkName(filename,num); },error: function(){ newFilename=fileWithoutExt+"("+num+")"+"."+fileExt; }});
			return (newFilename);
		}
		function doConfirm(msg, yesFn, cancelFn, noFn)
		{
			var confirmBox = $("#confirmBox");
			var blackBox = confirmBox.closest(".blackbox");
			confirmBox.find(".message").html(msg);
			confirmBox.find(".yes,.cancel,.no").unbind().click(function()
			{
				confirmBox.hide();
				blackBox.hide();
			});
			confirmBox.find(".yes").click(yesFn);
			confirmBox.find(".cancel").click(cancelFn);
			confirmBox.find(".no").click(noFn);
			confirmBox.show();
			blackBox.show();
		}
		 $('.search').on('keyup',function () {
			value=$(this).val();
			$('#emailBody_iframe').attr('src', galleryFileUrl+"?newField="+elementCurrentValue+'&'+return_name+"="+returnToElementId+"&returnToElementWhat="+returnToElementWhat+"&dir="+dirShortPath +"&acceptedFiles="+acceptedFiles+'&search='+value);
		});
		// $('.blackbox').on('mouseup',function (e)
		// {
			// if ($('#other_att').length>0) $('#other_att').hide();
			// if ( ( $(e.target).is('.blackbox') )&& ( parent.$('.blackbox').length>0)  )
			// {
				// dropzone = parent.$('.dropzone-previews').first();
				// dropzone.hide().appendTo( parent.$('#userUpdate') );
				// parent.$('.blackbox').hide();
				// parent.$('body,html').css('position','');
				// parent.$('.blackbox').remove();
			// }
		// });
		$('.img-delete').click(function ()
		{ 
			$('#logotype').val( '' );
			$('.logotypePreview, .dz-details img').remove(); 
			$('.img-delete').hide();
			$("<img class='logotypePreview' alt='logo.png' src='"+uploadImageDir+"/no_image-small.jpg' />").insertAfter('.dz-default.dz-message');
		});
	});
}