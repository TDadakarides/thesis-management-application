function newsletterSend()
{
	maxEmailSendPerHour=200;
	newsletterallCount=parseInt( $('.button.emailSend.emailSendButton').attr('data-newsletterallcount') );
	receiverTotal=parseInt( $('input.cust_id:checked:not(.hasSendBefore)').length );
	allreceiverTotal=parseInt( $('input.cust_id').length );
	//console.log('.button.emailSend.emailSendButton:'+( $('.button.emailSend.emailSendButton').length ) );
	//console.log('newsletterallCount:'+newsletterallCount);
	//console.log('allreceiverTotal:'+allreceiverTotal);
	//console.log('receiverTotal:'+receiverTotal);
	if ( ( newsletterallCount+receiverTotal )<= maxEmailSendPerHour )
	{
		if ($('.loadingBox.messageBox').length>0) $('.loadingBox.messageBox').closest('.blackbox').remove();
		blackbox=$("<div></div>");
		loading=$("<div class='loadingBox messageBox'><p class='loading'><img src='webImages/loading.gif' width='20' height='auto' style='display:inline-block;margin-right:20px;'/>Αποστολή email!!!</p></div>");
		result=$("<div class='checkEmailResult'><p>Επιτυχημένα <span class='emailResultOKNum'>0</span>/<span class='emailResultTotal'></span> (<span class='emailOKAmount'></span>%)</p><p>Αποτυχημένα <span class='emailResultNotNum'>0</span>/<span class='emailResultTotal'></span> (<span class='emailNotAmount'></span>%)</p></div>");
		loadingMsg=$("<div class='loadingMsg'></div>").hide();
		bar=$("<div class='bar'><div class='ok'></div><div class='Not'></div></div>");
		details=$("<div class='details opened'>Λεπτομέρειες</div>");
		loading.append(result).append(bar).append(details).append(loadingMsg);
		close=$("<div class='closeButton returnBack'>X</div>");
		blackbox.addClass("blackbox").append(loading).append(close);
		$('body').append(blackbox);
		$('.blackbox').hide();
		blackbox.show();					
		//////////ΝΑ ΒΑΛΩ AJAX POST ΕΝΑΝ ΕΝΑΝ ΤΟΥΣ ΣΥΝΕΡΓΑΤΕΣ ΝΑ ΣΤΕΛΝΕΙ EMAIL
		$('.emailResultTotal').text( parseInt( $('input.cust_id:checked').length ) ) ;
		$('.cust_id:checked').each(function ()
		{
			$.ajax(
			{
				async: true,
				type: "POST",
				data: {cust_id: $(this).val() , EmailSubject: $('#EmailSubject').val(),emailBody: $('#emailBody').val(),news_id: $('#news_id').val()  },
				url: "newsletterSend2.php"
			})
			.always(function( data )
			{
				//console.log('-----------');
				//console.log( data );
				el=$(data);
				$('.loadingMsg').append( el );
				checkEmailResult();
			});
		});
	}
	else
	{
		blackbox=$("<div></div>");
		loading=$("<div class='loadingBox messageBox'><div class='loadingMsg'>Έχετε υπερβεί το όριο ανά ώρα. Μέγιστη αποστολή 200 emails ανα ώρα.</div></div>");
		close=$("<div class='closeButton'>X</div>");
		blackbox.addClass("blackbox").append(loading).append(close);
		$('body').append(blackbox);
		blackbox.show();
		//$('.blackbox .close').one('click',function () { $('.blackbox').fadeOut(600,function (){ $(this).remove();}) });					
	}
}
$('.blackbox .loadingBox .details.opened').live('click',function () { $(this).removeClass('opened').addClass('closed');$('.loadingMsg').slideDown( "slow" ); });
$('.blackbox .loadingBox .details.closed').live('click',function () { $(this).removeClass('closed').addClass('opened');$('.loadingMsg').slideUp( "slow" ); });

function checkEmailResult()
{
	totalOK=parseFloat($('.emailResultOK').length);
	totalNot=parseFloat($('.emailResultNot').length);
	totalSend=totalOK+totalNot;
	widthOk= parseFloat( (totalOK/totalSend)*$('.blackbox .messageBox .bar').width() ).toFixed(0);
	widthNot= parseFloat( (totalNot/totalSend)*$('.blackbox .messageBox .bar').width() ).toFixed(0);
	widthTotal= parseFloat( widthOk )+parseFloat( widthNot );
	$('.emailResultOKNum').text( totalOK ) ;
	$('.emailResultNotNum').text( totalNot ) ;
	$('.emailOKAmount').text( (totalOK/totalSend*100).toFixed(0) ) ;
	$('.emailNotAmount').text( (totalNot/totalSend*100).toFixed(0) ) ;
	$('.blackbox .messageBox .bar .Ok').width( widthOk );
	$('.blackbox .messageBox .bar .Not').width( widthNot );
	if ( totalOK+totalNot==totalSend ) $('p.loading').remove();
	//console.log(totalOK+'<'+widthOk +'px> + '+totalOK+'<'+widthNot+'px> = '+(widthTotal)+'px');
}