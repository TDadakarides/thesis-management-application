		////////////ΕΠΟΜΕΝΗ ΕΙΚΟΝΑ//////////
		var canContinue=true;
		var canContinue2=true;
		var canPressButton=true;
		//////ΠΑΤΗΜΑ ΠΛΗΚΤΡΟΥ ΔΕΞΙΑ ΑΡΙΣΤΕΡΑ ΩΣΤΕ ΝΑ ΑΛΛΑΖΕΙ Η ΦΩΤΟΓΡΑΦΙΑ
		function pictureActivate( activeThumb )
		{
			pictureBox=activeThumb.closest('.pictureBox');
			pictureBox.find('.thumbs.active_photo').removeClass('active_photo');
			activeThumb.addClass('active_photo');
			pictureSrc=activeThumb.attr('data-src');
			pictureSrcLarge=( pictureSrc ).replace('/images_thumbs/','/images/');
			picID=( activeThumb.attr('id') ).replace("thumbs","");
			pictureBox.find('.pic_ID').val( picID );//δίνουμε τιμη pic_ID για αρχική φωτογραφια;
			pictureBox.find('.image_large_view').css("display", "none");
			pictureBox.find('.image_large_view').css("background-image", "url('"+pictureSrcLarge+"')");
			pictureBox.find('.image_large_view').fadeIn(500, function () { pictureBox.find(".image_large_view").clearQueue();canContinue2=true;});
			$('.fullscreen .photo').css("display", "none");
			$('.fullscreen .photo').attr("src", pictureSrcLarge);
			$('.fullscreen .photo').fadeIn(500, function () { pictureBox.find('.photo').clearQueue();canContinue=true; });
			if ( activeThumb.next('.thumbs').length<=0 )
			{ $(".fullscreen .photo_right").fadeOut(600);}
			else
			{ $(".fullscreen .photo_right").fadeIn(600);}
			if ( activeThumb.prev('.thumbs').length<=0 )
			{ $(".fullscreen .photo_left").fadeOut(600);}
			else
			{ $(".fullscreen .photo_left").fadeIn(600);}
			selectedPic=activeThumb.index()+1;
			pictureBox.find('.selectedPic').text( selectedPic );
			$('.fullscreen .photoInfo').text( $('.selectedPic').closest('div').text() );			
		}
		function pictureInit()
		{
			//console.log('pictureInit');
			/////ΑΡΧΙΚΟΠΟΙΗΣΗ ΕΙΚΟΝΩΝ
			$('.pictureBox').each(function ()
			{
				pictureBox=$(this);
				thumbWidth=parseInt( pictureBox.find('.thumbs').height( ) );
				thumbMargin=parseInt( thumbWidth/10 );
				//pictureBox.find('.thumbs').width( thumbWidth ).css('display','inline-block').css('margin-right',thumbMargin).css('display','inline-block');
				if ( pictureBox.find('.thumbs.active_photo').length>0 ) thumbActive=pictureBox.find('.thumbs.active_photo'); else thumbActive=pictureBox.find('.thumbs').first();
				pictureActivate( thumbActive );
				thumbsWidth=0;
				pictureBox.find('.thumbs').each(function ()
				{
					thisThumbsWidth=0;
					thisThumbsWidth+= $(this).outerWidth( true );
					//thisThumbsWidth+=4;
					thumbsWidth+=thisThumbsWidth;
				});
				//thumbsWidth+=4;
				thumbsWidth=
				//pictureBox.find('.thumb_slide_window').width(thumbsWidth);
				thumbsWidthAfter=0;
				checkVisibleSlideButtons( pictureBox );
				////ΑΝ ΤΑ THUMBS ΕΙΝΑΙ ΠΕΡΙΣΣΟΤΕΡΑ ΚΑΙ ΔΕ ΦΑΙΝΟΝΤΑΙ
			});
			canPressButton=true;
			if ( $('.fullscreen').lenght==0 )
			{
				fullscreenPhoto=$("<div class='fullscreen'></div>");
				loadingDiv=$("<div class='loadingBox'></div>");
				photo=$("<img class='photo'/>");
				photo_right=$("<img class='photo_right'/>").attr('src','/webImages/right.png');
				photo_left=$("<img class='photo_left'/>").attr('src','/webImages/left.png');
				close_photo=$("<img class='close_photo'/>").attr('src','/webImages/x21.png');
				photoInfo=$("<img class='photoInfo'/>");
				loadingDiv.append(photo).append(photo_right).append(photo_left).append(close_photo).append(photoInfo);
				fullscreenPhoto.append(loadingDiv);
				fullscreenPhoto.appendTo('body');
			}
		}
        $(".pictureBox .right_slide").live('click',function ()
		{
			pictureBox=$(this).closest('.pictureBox');
			if (canPressButton)
			{
				canPressButton=false;
				thumbsTotalWidth=pictureBox.find('.thumb_slide_window').width();
				leftThumbs=parseInt( pictureBox.find('.thumb_slide_window').css('left') );
				thumbsVisibleWindowWidth=pictureBox.find('.thumb_container').width();
				thumbNowView=thumbsTotalWidth+leftThumbs;
				if (thumbNowView>thumbsVisibleWindowWidth )
				{			
					pictureBox.find('.thumb_slide_window').animate({
						"left": "-=" + thumbsVisibleWindowWidth + "px"
					}, 600, function () { pictureBox.find(".thumb_container").clearQueue(); checkVisibleSlideButtons( pictureBox );canPressButton=true;});
				}
			}				
        });

        $(".pictureBox .left_slide").live('click',function ()
		{
			pictureBox=$(this).closest('.pictureBox');
			if (canPressButton)
			{
				canPressButton=false;
				thumbsTotalWidth=pictureBox.find('.thumb_slide_window').width();;
				leftThumbs=parseInt( pictureBox.find('.thumb_slide_window').css('left'));
				thumbsVisibleWindowWidth=pictureBox.find('.thumb_container').width();
				thumbNowView=thumbsTotalWidth+leftThumbs;
				if (leftThumbs<0 )
				{
					if ((leftThumbs+thumbsVisibleWindowWidth)>0 )
					{
						movePX=leftThumbs;
					}
					else
					{
						movePX=(-1)*thumbsVisibleWindowWidth;
					}
					pictureBox.find('.thumb_slide_window').animate({
						"left": "-=" + movePX + "px"
					}, 600, function () { pictureBox.find(".thumb_container").clearQueue();checkVisibleSlideButtons( pictureBox );canPressButton=true; });
				}
			}
        });
		function checkVisibleSlideButtons( pictureBox )
		{
			//console.log('checkVisibleSlideButtons');
			thumbsTotalWidth=pictureBox.find('.thumb_slide_window').width();
			leftThumbs=parseInt( pictureBox.find('.thumb_slide_window').css('left') );
			thumbsVisibleWindowWidth=pictureBox.find('.thumb_container').width();
			thumbNowView=thumbsTotalWidth+leftThumbs;			
			if (leftThumbs<0 )
			{
				pictureBox.find(".left_slide").show();
			}
			else
			{
				pictureBox.find(".left_slide").hide();
			}
			if (thumbNowView>thumbsVisibleWindowWidth )
			{
				 pictureBox.find(".right_slide").show();
			}
			else
			{
				pictureBox.find(".right_slide").hide();
			}			
		}
		//////ΕΠΙΛΟΓΗ ΚΑΠΟΙΟΥ THUMBS
        $(".pictureBox .thumbs").live('click',function ()
		{
			if (canContinue2)
			{
				canContinue2=false;
				thumbActive=$(this);
				pictureActivate( thumbActive );
			}
       });
		//////ΑΝΟΙΓΜΑ ΤΗΣ ΜΕΓΑΛΗΣ ΕΙΚΟΝΑΣ
		$(".pictureBox .image_large_view,.pictureBox .zoom").live('click',function ()
		{
			pictureBox=$(this).closest('.pictureBox');
			pictureBoxId=pictureBox.attr('id');
			if ( pictureBox.find('.thumbs.active_photo').length>0 ) activeThumb=pictureBox.find('.thumbs.active_photo'); else activeThumb=pictureBox.find('.thumbs').first();
			//$(".photo_left").css('opacity',0.1);
			pictureActivate( activeThumb );
			resize_images();
			$('.fullscreen').attr('data-pictureBox',pictureBoxId)
			$('.fullscreen .photo').attr("tabindex",-1).focus();		
        });
		/////ΔΕΞΙΑ ΦΩΤΟΓΡΑΦΙΑ
		$(".fullscreen .photo_left").live('click',function()
		{
			pictureBoxId=$(this).closest('.fullscreen').attr('data-pictureBox');
			pictureBox=$('#'+pictureBoxId);
			prevThumbExist=pictureBox.find('.thumbs.active_photo').prev('.thumbs').length>0;
			if (prevThumbExist)
			{
				if (canContinue)
				{
					canContinue=false;
					prevThumb=pictureBox.find('.thumbs.active_photo').prev('.thumbs');
					pictureActivate( prevThumb );
				}
			}
		});
		////ΑΡΙΣΤΕΡΗ ΦΩΤΟΓΡΑΦΙΑ
		$(".fullscreen .photo_right").live('click',function()
		{
			pictureBoxId=$(this).closest('.fullscreen').attr('data-pictureBox');
			pictureBox=$('#'+pictureBoxId);
			nextThumbExist=pictureBox.find('.thumbs.active_photo').next('.thumbs').length>0;
			if (nextThumbExist)
			{
				if (canContinue)
				{
					canContinue=false;
					nextThumb=pictureBox.find('.thumbs.active_photo').next('.thumbs');
					pictureActivate( nextThumb );
				}
			}
		});
	   /////////ΠΑΤΗΜΑ ΔΕΞΙΑ - ΑΡΙΣΤΕΡΑ ΓΙΑ ΦΩΤΟΓΡΑΦΙΕΣ
		$("body").live('keydown',function(e)
		{
			if ( $('.fullscreen:visible').length>0)
			{
				pictureBoxId=$('.fullscreen').attr('data-pictureBox');
				pictureBox=$('#'+pictureBoxId);
				if (e.keyCode == 37) { $(".fullscreen .photo_left").trigger('click');}
				else if(e.keyCode == 39) { $(".fullscreen .photo_right").trigger('click');}
			}
		});
		/////ΑΝ ΓΙΝΕΙ ΚΛΙΚ ΣΤΗ ΜΕΓΑΛΗ ΦΩΤΟΓΡΑΦΙΑ, ΑΝΑΛΟΓΩΣ ΠΟΥ ΒΡΙΣΚΕΤΑΙ ΔΕΞΙΑ 70% ΑΡΙΣΤΕΡΑ 30% ΕΚΤΕΛΕΙ ΤΗΝ ΑΝΑΛΟΓΗ ΕΝΕΡΓΕΙΑ
		$(".fullscreen .photo").live('mousedown',function(e)
		{
			pictureBoxId=$(this).closest('.fullscreen').attr('data-pictureBox');
			pictureBox=$('#'+pictureBoxId);
			x=(e.pageX); 
			y=(e.pageY);
			x=x-( $(".fullscreen .photo").offset().left );
			y=y-( $(".fullscreen .photo").offset().top );
			width=$(".fullscreen .photo").width();
			height=$(".fullscreen .photo").height();
			if (width*0.3>x) 
			{
				position="left";
				$(".fullscreen .photo_left").trigger('click');
			} 
			else 
			{
				position="right";
				$(".fullscreen .photo_right").trigger('click');
			}		
		});
		//////ΚΛΕΙΣΙΜΟ ΤΗΣ ΜΕΓΑΛΗΣ ΕΙΚΟΝΑΣ
        $(".fullscreen .close_photo").live('click',function () {
            //$(this).fadeOut(500);
            $('.fullscreen').fadeOut(500);
       });		
		function resize_images()
		{
			//pictureSrc=$('.thumbs.active_photo').attr('src');
			//pictureSrcLarge=(pictureSrc).replace('/images_thumbs/','/images/');
			//$('.image_large_view').css("background-image", "url('"+pictureSrcLarge+"')");
			//$('.photo').attr('src',pictureSrcLarge);
			$('.close_photo').fadeIn(500);
			$('.fullscreen').fadeIn(500).css('display','inline-flex');
		}	
		$(window).resize(function() 
		{
			if ( $('.fullscreen:visible').length>0){resize_images();}
		});