var tableRef='';
$('.blackbox.date').live('mouseup',function (e)
{
	if ( $(e.target).is('.blackbox')  )
	{
		parent.$('.blackbox.date').remove();
	}
});

(function ($) {
   $.fn.liveColpick = function (opts) {
	  this.live("mouseover", function() {
		 if (!$(this).data("init")) {
			bg=( (typeof $(this).css('background-color')!='undefined')?$(this).css('background-color'):'rgb(255,255,255)' );
			bg=rgb2hex(bg) ;
			opts.color=bg;
			$(this).data("init", true).colpick(opts);
		 }
	  });
	  return this;
   };
}(jQuery));
$('.colorShow.colorProd,.colorPicker').liveColpick(
{
	colorScheme:'dark',
	flat:false,
	layout:'hex',
	onSubmit:function(hsb,hex,rgb,el)
	{
		$(el).closest('td').find("input").val('#'+hex);
		$(el).css('background-color', '#'+hex);
		$(el).colpickHide();
	}
});
$('.blackbox.date .nextMonth').live('click',function()
{
	day=parseInt( $('.blackbox.date .dateBox').attr('data-day') );
	month=parseInt( $('.blackbox.date .dateBox').attr('data-month') )+1;
	year=parseInt( $('.blackbox.date .dateBox').attr('data-year') );
	if ( month==13 ) { month=1;year+=1;}
	$.get('calendar.php',{day:day, month:month, year:year } )
	.done(function(data)
	{
		$('.dateBox').html( data );
		$('.blackbox.date .dateBox').attr('data-day',day);
		$('.blackbox.date .dateBox').attr('data-month',month);
		$('.blackbox.date .dateBox').attr('data-year',year);
	});
	
});
$('.blackbox.date .prevMonth').live('click',function()
{
	day=parseInt( $('.blackbox.date .dateBox').attr('data-day') );
	month=parseInt( $('.blackbox.date .dateBox').attr('data-month') )-1;
	year=parseInt( $('.blackbox.date .dateBox').attr('data-year') );
	if ( month==0 ) { month=12;year-=1;}
	$.get('calendar.php',{day:day, month:month, year:year } )
	.done(function(data)
	{
		$('.blackbox.date .dateBox').html( data );
		$('.blackbox.date .dateBox').attr('data-day',day);
		$('.blackbox.date .dateBox').attr('data-month',month);
		$('.blackbox.date .dateBox').attr('data-year',year);
	});	
});
$('.blackbox.date .dates').live('click',function()
{
	element=$('#'+$('.blackbox.date .dateBox').attr('data-return') );
	day=$(this).text();
	month=$('.blackbox.date .dateBox').attr('data-month');
	year= $('.blackbox.date .dateBox').attr('data-year');
	d=new Date( month+'-'+day+'-'+year );
	element.val( d.format("yyyy-MM-dd 00:00") );
	$('.blackbox.date').fadeOut(400,function() {$(this).remove();});
	
});
$('.dateButton').live('click',function()
{
	//console.log('-------dateButton click event ----------');
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	if ( $('.blackbox.date').length==0)
	{
		element=$('#'+$(this).attr('data-return') );
		dateSplit=( ( element.val() ).split(" "));
		dateArray=dateSplit[0]!=undefined?dateSplit[0].split("-"):'';
		year=dateArray[0]!=undefined&&dateArray[0]!='00'&&dateArray[0]!=''?dateArray[0]:yyyy;
		month=dateArray[1]!=undefined&&dateArray[1]!='00'&&dateArray[1]!=''?dateArray[1]:mm;
		day=dateArray[2]!=undefined&&dateArray[2]!='00'&&dateArray[2]!=''?dateArray[2]:dd;
		d=new Date( month+'-'+day+'-'+year );
		returnToElementId=$(this).attr('data-return');
		dateBox=$("<div class='dateBox' data-return='"+returnToElementId+"' data-day='"+day+"' data-month='"+month+"' data-year='"+year+"' ></div>");
		$.get('calendar.php',{day:day, month:month, year:year } )
		.done(function(data)
		{
			dateBox.html( data );
			blackbox=$("<div class='blackbox date'></div>").append( dateBox ).appendTo('body').fadeIn(800);
		});
	}
});
$('.clear_pic').live('click',function ()
{
	pictureBox=$(this).attr('data-picture-id');
	input=$(this).attr('data-input-id');
	$('#'+input).val('');
	$('#'+pictureBox).removeClass('hasPhoto').css('background-image','');					
});
function rgb2hex(rgb)
{
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	return "#" +
	("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
	("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
	("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
}
$('.foreignBox').live('click',function ()
{
	var referenceId=$(this).closest('.data_input').find('.foreign_value').attr('id');
	var url=$(this).attr('data-src');
	var foreignButton=$(this).find('.foreignButton');
	var foreignHtmlFrame=$(this).closest('tr').next('tr').find('.foreignHtmlBox');
	if (foreignHtmlFrame.length<=0)
	{
		tableRef=$(this).attr('data-ref-table');
		foreignSearchBox="";
		$("<tr class='foreignBoxTr' data-reference-id='"+referenceId+"'><td colspan='2'>"
		+"<div class='searchForeignBox' style='display:block;'><input class='searchForeign input right' placeholder='Εύρεση' style='margin:0px 0px 12px 0px;'/><div style='clear:both;'></div></div>"
		+"<div data-ref-table='"+tableRef+"' class='foreignHtmlBox'></div></td></tr>").insertAfter( $(this).closest('tr') );
		foreignHtmlFrame=$(this).closest('tr').next('tr').find('.foreignHtmlBox');
		$.get( url )
		.done( function ( html )
		{
			foreignHtmlFrame.html( html ).stop( true,false ).show();
		});
	}
	else
	{
		foreignHtmlFrame.closest('tr').stop( true,false ).toggle();
	}
	if (foreignButton.html()=='+') foreignButton.html('-'); else foreignButton.html('+');
});
var inverter='';
$('.foreignHtmlBox .click').live('click',function ()
{
	tableRef=$(this).closest('.foreignHtmlBox').attr('data-ref-table');
	console.log('tableRef:'+tableRef);
	var html='';
	if (tableRef=='collector_hydraulics')
	{
		html=$(this).closest('tr').find('.foreignKey').next('td').html()+' '+$(this).closest('tr').find('.foreignKey').next('td').next('td').html();
	}
	else
	{
		html=$(this).closest('tr').find('.foreignHtml').html();
		// html=$(this).closest('tr').find('.foreignKey').next('td').html();
	}
	if (tableRef=='pumps')
	{
		inverter=$(this).closest('tr').find('td').eq(9).text();
		windowBox=$('.allWindowBox:visible').find('.windowBox').last();
		windowBox.find('.inverter').text(inverter);
		initializeGraphPumpSpeed();
		//$('#frame_graph').attr('src','graphs.php?inverter='+inverter);
	}
	value=$(this).closest('tr').find('.foreignKey').html();
	foreignBox=$(this).closest('.foreignHtmlBox').closest('tr').prev('tr').find('input').val( value )
	foreignBox=$(this).closest('.foreignHtmlBox').closest('tr').prev('tr').find('.foreignBox span').html( html );
	foreignBox.trigger('click');
});
function clearTinyMCE(){
    for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
            var ed_id = tinymce.editors[i].id;
            tinyMCE.execCommand("mceRemoveEditor", true, ed_id);
        }
}
function mediumTextMCE(selector)
{
	tinymce.init({
		theme : "modern",
		// mode: "specific_textareas",
		//selector: selector,
		// theme_advanced_buttons1 : "bold , italic , underline , strikethrough , | , justifyleft , justifycenter , justifyright , justifyfull , | , styleselect , formatselect , fontselect , fontsizeselect",
		// theme_advanced_buttons2 : "forecolor,backcolor,sub,sup,|,bullist,numlist,|,outdent,indent,blockquote,|,hr,charmap,|,removeformat",
		// theme_advanced_buttons3 : ",myPicture,image,|,myAttach,link,unlink,|,code",
		// theme_advanced_buttons4 : "undo,redo,|,cut,copy,paste,pastetext,pasteword",
		menubar: false,
		toolbar: [
			"bold  italic  underline  strikethrough  |  justifyleft  justifycenter  justifyright  justifyfull  |  formatselect  fontsizeselect"
			," sub sup | bullist numlist | outdent indent | removeformat"
			," | code"
			,"undo redo | cut copy paste pastetext pasteword"
			], 
			plugins: [
		  'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
		  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
		  'save table contextmenu directionality emoticons template paste textcolor'
		],
		//inline: true,
		// plugins : "table",
		//theme_advanced_buttons4_add : "tablecontrols",
		// table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
		// table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
		// table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
		// theme_advanced_toolbar_location : "top",
		// theme_advanced_toolbar_align : "left",
		// theme_advanced_statusbar_location : "bottom",
		//plugins : 'inlinepopups',
        // menubar: false,
        // toolbar_items_size: 'large',
        // toolbar: 'Επιλογή εικόνας',
		target : selector,
		setup: function(editor) {
			// editor.addButton('myPicture', {
			// 	title : 'Επιλογή εικόνας',
			// 	text: 'Επιλογή εικόνας',
			// 	image : 'webImages/select_pic.png',
			// 	icon: false,
			// 	onclick: function()
			// 	{
			// 		textarea=$(this);
			// 		//console.log( textarea.length )
			// 		if (textarea.closest('td').length>0) textarea.closest('td').find('.dropzoneButton').first().trigger('click');
			// 		if (textarea.closest('.rtBox').closest('.discountBlackBox').length>0) textarea.closest('.discountBlackBox').find('.dropzoneButton').first().trigger('click');
			// 	}
			// });
			// editor.addButton('myAttach', {
			// 	title : 'Επιλογή συνημμένου',
			// 	text: 'Επιλογή συνημμένου',
			// 	image : 'webImages/select_attachment.png',
			// 	icon: false,
			// 	onclick: function()
			// 	{
			// 		textarea=$(this);
			// 		if (textarea.closest('td').length>0) textarea.closest('td').find('.dropzoneButton').eq(1).trigger('click');
			// 		if (textarea.closest('.rtBox').closest('.discountBlackBox').length>0) textarea.closest('.discountBlackBox').find('.dropzoneButton').eq(1).trigger('click');
			// 	}
			// });
		},
		height:"200px",
		width:'600px'
	});
}
function isProduct(tableRef)
{
	switch(tableRef)
	{
		case 'antifreeze':
		case 'collectors':
		case 'pumpstation':
		case 'pipes':
			return true;
	}
	return false;
}
////menu buttons////////
$('#saveas').live('click',function ()
{
	if (tableRef=='pumps')
	{  	
		$('#phases').removeAttr('disabled');
		$('#type').removeAttr('disabled');
	}
	if (isProduct(tableRef))
	{
		$('#form1').attr('action', 'dublicate.php');
	}
	else
	{
		$('#form1').attr('action', 'save.php');
	}
	$('#form1').submit();
});
Date.prototype.format = function(format) //author: meizz
{
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length==1 ? o[k] :
        ("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}
$('#title_el').live('keyup',function (){
	$('#name_of_product').text( $(this).val() );
});
$('#title_el').live('change',function (){
	$('#name_of_product').text( $(this).val() );
});
//////ΓΙΑ ΦΟΡΤΩΜΑ ΕΝΔΕΙΞΗΣ ΧΡΗΣΤΩΝ NEWSLETTER
$('.usersCount').live('click',function ()
{
	$('.userList').html("<img class='loading' style='width:20px;height:20px;dislpay:inline-block;margin-right:10px;' src='../webImages/loading.gif' /><span class='loading' >Φόρτωση!!</span>");
	news_id=$('.news_id', $(this).parents('tr')).text() ;
	//usersCount=$('.usersCount img', $(this).parents('tr')).attr('data-pop');
	if ( $('.blackBox').length>0 ) $('.blackBox').remove();
	blackBox=$("<div></div>");
	closeButton=$("<div class='closeButton'>&#10005;</div>");
	userList=$("<div class='userList table_datain'></div>");
	blackBox
	.append( closeButton )
	.append( userList )
	.addClass('blackBox')
	.appendTo('body')
	.fadeIn(800);
	$('.userList .loading').show();
	var jqxhr = $.post( "newsletterUser.php",{ news_id: news_id } )
	.done(function( data )
	{
		$('.userList .loading').hide();
		$('.userList').append( data );
	})
	.fail(function()
	{
		$('.userList .loading').hide();
		$('.userList').append( "<span>Πρόβλημα server. Προσπαθήστε ξανά.</span>" );
	});
});
$('.blackbox').live('mouseup',function (e)
{
	// if ( $(e.target).is('.blackbox') )
	// $('.blackbox .closeButton').trigger('click');
});
$('.blackbox .closeButton').live('click',function ()
{
	$('.blackbox').fadeOut(500);
});

$('.mediumTextField').live('click',function ()
{
	if ( $('.blackbox').length>0 ) $('.blackbox').remove();
	dataBox=$("<div class='blackbox'>");
	closeButton=$("<div class='closeButton'>");
	userFieldAddButtons=$("<div class='userFieldAddButtons'>");
	userFieldAdd=$("<div class='userFieldAdd'>");
	$("<p>Επιλογή στοιχείου πελάτη</p>").appendTo(userFieldAddButtons);
	userFieldAdd.html("Όνομα<img src='webImages/addBlack.png' width='15' height='15'/>").attr('data-field-name','name').clone().appendTo(userFieldAddButtons);
	userFieldAdd.html("Επώνυμο<img src='webImages/addBlack.png' width='15' height='15'/>").attr('data-field-name','surname').clone().appendTo(userFieldAddButtons);
	userFieldAdd.html("Email<img src='webImages/addBlack.png' width='15' height='15'/>").attr('data-field-name','email').clone().appendTo(userFieldAddButtons);
	userFieldAdd.html("Εταιρία<img src='webImages/addBlack.png' width='15' height='15'/>").attr('data-field-name','company').clone().appendTo(userFieldAddButtons);
	userFieldAddButtons.appendTo(dataBox);
	closeButton.appendTo(dataBox);
	dataBox.appendTo('body').fadeIn(800);
});
$('.userFieldAdd').live('click',function ()
{
	fieldName=$(this).attr('data-field-name');
	img_html="&nbsp;<span class='userField' data-field-name='"+fieldName+"' >"+fieldName+"<img src='webImages/delete.png' width='15' height='15'/></span>&nbsp;" ;
	tinyMCE.get('emailBody').selection.setContent(img_html); 
	$('.blackbox').first().trigger('mouseup');
	$('#emailBody').next('span').find('iframe').contents().find('.userField').on('click',function () { $(this).remove(); });
});
$('.addFilter').live('click',function ()
{
	var div=$("<div class='filter'></div>");
	deleteButton=$("<div class='deleteButton'>X</div>");
	div.append( deleteButton );
	div.insertAfter( $(this) );
	$.get('newsletterUserFilter.php')
	.done( function( data )
	{
		$('.filter').first().append( $( data ) );
		filterShow();
		checkLogical();
		userLoad( $('.userUpdate .allReceivers') );
		//$(this).closest('.userUpdate').attr('action','usersAll.php').attr('target','iframeUser').submit();
	})
});
function userLoad( element )
{
	dataPost=element.closest('.userUpdate').serialize();
	$.post('usersAll.php',dataPost)
	.done(function (data)
	{
		element.html( data );
		$('.receiversCount').html( $('input.cust_id:checked').length );
	});
}
function filterShow()
{
	$('.filter').each(function ()
	{
		fieldName=$(this).find('.fieldName');
		value=fieldName.val();
		fieldValues=$(this).find(".allValues .fieldValues").attr('name','').hide();
		fieldValues=$(this).find(".allValues .fieldValues[data-name='"+value+"']").attr('name','fieldvalue[]').show();
	});
}
$('.fieldName').live('change',function ()
{
	filterShow();
	checkLogical();
	userLoad( $('.userUpdate .allReceivers') );
});
$('.equalitation,.fieldValues,.logical').live('change',function ()
{
	checkLogical();
	userLoad( $('.userUpdate .allReceivers') );
});
$('.fieldValues').live('keyup',function ()
{
	checkLogical();
	userLoad( $('.userUpdate .allReceivers') );
});
$('.filter .deleteButton').live('click',function ()
{
	$(this).closest('.filter').remove();
	checkLogical();
	userLoad( $('.userUpdate .allReceivers') );
});
function checkLogical()
{
	$('.filter .logical').each(function () 
	{
		$(this).show();
		attrName=$(this).attr('name');
		//alert(attrName);
		allWithSameName=$(".logical[name=\""+attrName+"\"]");
		if ( allWithSameName.length==1) {$(this).hide();} else 
		{
			allWithSameName.last().hide();
		}
	});
}
var shifted;
var lastChecked=0;
$('input.cust_id').live('change',function()
{
	$('.receiversCount').html( $('input.cust_id:checked').length );
});
$('.selectAll').live('change',function()
{
	var checkboxes = $(this).closest('form').find('input:checkbox');
	if($(this).is(':checked'))
	{
		checkboxes.attr('checked', 'checked');
	}
	else
	{
		checkboxes.removeAttr('checked');
	}
	$('.selectedCount').html( $(this).closest('form').find('input:checkbox:checked.itemsSelected').length );
	$('.totalCount').html( $(this).closest('form').find('input:checkbox.itemsSelected').length );
});
$('.selectAllInstalled').live('change',function()
{
	$(this).closest('form').find('input:checkbox.uninstalled').removeAttr('checked');
	$('.selectAll').removeAttr('checked');
	$('.selectAllUninstalled').removeAttr('checked');
	var checkboxes = $(this).closest('form').find('input:checkbox.installed');
	if($(this).is(':checked'))
	{
		checkboxes.attr('checked', 'checked');
	}
	else
	{
		checkboxes.removeAttr('checked');
	}
	$('.selectedCount').html( $(this).closest('form').find('input:checkbox:checked.itemsSelected').length );
	$('.totalCount').html( $(this).closest('form').find('input:checkbox.itemsSelected').length );
});
$('.selectAllUninstalled').live('change',function()
{
	$('.selectAll').removeAttr('checked');
	$('.selectAllInstalled').removeAttr('checked');
	$(this).closest('form').find('input:checkbox.installed').removeAttr('checked');
	var checkboxes = $(this).closest('form').find('input:checkbox.uninstalled');
	if($(this).is(':checked'))
	{
		checkboxes.attr('checked', 'checked');
	}
	else
	{
		checkboxes.removeAttr('checked');
	}
	$('.selectedCount').html( $(this).closest('form').find('input:checkbox:checked.itemsSelected').length );
	$('.totalCount').html( $(this).closest('form').find('input:checkbox.itemsSelected').length );
});
$('.userSettings tr td:not(.selection)').live('click',function()
{
	var checkboxes = $(this).closest('tr').find(':checkbox');
	if(checkboxes.is(':checked'))
	{
		checkboxes.removeAttr('checked');
	}
	else
	{
		checkboxes.attr('checked', 'checked');
	}
});

$('.table .product_tables .tableName').live('click',function ()
{
	$('.product_tables.active').removeClass('active');
	$(this).closest('.product_tables').addClass('active');
	table_name_eng_rel=$(this).attr('data-table_name_eng_rel');
	table_name_eng=$(this).attr('data-table_name_eng');
	prod_ID=$(this).attr('data-prod_ID');
	$('#table_name_eng_rel').val( table_name_eng_rel );
	$.post( 'select_view_rel.php' , { table_name_eng_rel : table_name_eng_rel , table_name_eng : table_name_eng , prod_ID : prod_ID } )
	.done(function (data)
	{
		$('.allProduct').html('').append( data );
	});
});
$('.table .allProduct .relation_select').live('click',function (){
	value=$(this).children(".rel_ID").html();
	$('#prod_ID_rel').val( value );
	$('.relation_select').removeClass('active');
	$(this).addClass('active');
});
(function($)
{
$.fn.valGreek=function ()
{
	if ( $(this).attr('value') !== undefined) 
		message=( $(this).val() );
	else
		message=( $(this).text() );
	arrayFrom='άέίήύόώϊϋΐΰ';
	arrayΤο='αειηυοωιυιυ';
	for (i=0;i<arrayFrom.split('').length;i++)
	{
		//alert(arrayFrom.split('')[i]+' ' +arrayΤο.split('')[i] );
		message=message.replace(new RegExp(arrayFrom.split('')[i], 'g'), arrayΤο.split('')[i] );
	}
	//alert(message);
	return message;
};
})(jQuery);
$('.searchForeign').live('keyup',function ()
{
	searchVal=$(this).valGreek();
	$.each( $(this).closest('.searchForeignBox').next('.foreignHtmlBox').find('.table').find("tr:not(.heading):not(.hidden)"), function ()
	{
		if ( $(this).valGreek().toLowerCase().replace(/\s+/g, '').indexOf(searchVal.replace(/\s+/g, '').toLowerCase()) == -1)
			$(this).hide();
		else
			$(this).show();
	});
});