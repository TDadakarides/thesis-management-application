function toDate(dStr)
{
	var now = new Date();
	pmam= ( dStr.substr(dStr.indexOf(" ")) ).replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	hours= dStr.substr(0,dStr.indexOf(":"));
	minutes=dStr.substr(dStr.indexOf(":")+1,(dStr.indexOf(" "))-(dStr.indexOf(":")+1) );
	if ( pmam=='pm') { hours=parseInt(hours)+12; } 
	now.setMinutes( minutes );
	now.setHours( hours );
	now.setSeconds( 0 );
	return now;
}
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

Date.prototype.format = function(format) //author: meizz
{
  var o = {
	"M+" : this.getMonth()+1, //month
	"d+" : this.getDate(),    //day
	"h+" : this.getHours(),   //hour
	"m+" : this.getMinutes(), //minute
	"s+" : this.getSeconds(), //second
	"q+" : Math.floor((this.getMonth()+3)/3),  //quarter
	"S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
	(this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
	format = format.replace(RegExp.$1,
	  RegExp.$1.length==1 ? o[k] :
		("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}
function convertAtoH(hexstring,size)
{
	hexstring = hexstring.substring(0,size);
	return parseInt(hexstring,16);
}
function convertAtoHSwap(hexstring,size,step)
{
	hexstring = hexstring.substring(0,size);
	//msg+=('hexstring',hexstring);
	temp='';
	temp2='';
	for (i=size-step;i>=0;i-=step)
	{
		temp=temp+hexstring.substring(i,i+step);
		//msg+=('temp',temp);
	}
	return parseInt(temp,16);
}
function generateColorString(r,g,b)
{
	var string = "#";
	if(r < 16)
	{
		string += "0";
	}
	string += r.toString(16);
	if(g < 16)
	{
		string += "0";
	}
	string += g.toString(16);
	if(b < 16)
	{
		string += "0";
	}
	string += b.toString(16);
	return string;
}
function returnStep(max,min,maxSteps)
{
	var steps=[0.1,0.2,0.25,0.5];
	index=0;
	log=1;
	step=0.1;
	do
	{
		step=steps[index]*log;
		if (index==steps.length-1) {log=log*10;index=0;} else {index++;}		
	} while ( ((max-min)/step)>maxSteps )
	return step;
}