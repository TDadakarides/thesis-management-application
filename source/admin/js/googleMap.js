 jQuery.fn.extend({
	googleMapRun : function(  LatLng )
    {
		LatLngPost=LatLng;
		element=$(this);
		elementId=$(this).attr('id');
		if ( typeof LatLng=='undefined' || LatLng=='' ) LatLng='37.975554, 23.735463';
		LatLngArray = LatLng.split(",");
		latitude=parseFloat( LatLngArray[0] );
		longitude=parseFloat( LatLngArray[1] );
		var newLocation = new google.maps.LatLng( latitude , longitude );
		var centerMap = new google.maps.LatLng( latitude , longitude );
		var mapOptions = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: centerMap,
			zoom: 6,
			center: newLocation
		};
		map = new google.maps.Map( element.get(0) ,mapOptions);
		map.markers=new Array();
		if ( typeof LatLngPost!='undefined' || LatLngPost!='' ) {  addMarker( map, newLocation ,elementId);}
		google.maps.event.addListener(map, 'click', function(event) { addMarker( map, event.latLng, elementId ); });


		// Create the search box and link it to the UI element.
		var input = $("<input class='pac-input input' placeholder='Αναζήτηση' />");
		var inputDom = input.get(0);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push( inputDom );
		var searchBox = new google.maps.places.SearchBox( (inputDom) );
        var googleMapViewTd=$(this).closest("td.data_input");
        var googleMapViewSearch=$(".googleMapViewSearch",googleMapViewTd);
        var googleMapViewSearchInput=$(".googleMapViewSearchInput",googleMapViewTd);
        googleMapViewSearch.click(function(){
            $('.pac-input',googleMapViewTd).val(googleMapViewSearchInput.val()).focus();
        });
		google.maps.event.addListener(searchBox, 'places_changed', function()
		{
			//console.log('click');
			var places = searchBox.getPlaces();
			//console.log(places.length);
			if (places.length == 0) { return; }			
			var bounds = new google.maps.LatLngBounds();
			for (var i = 0, place; place = places[i]; i++)
			{
				var image = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};
			}
			//console.log(arr);
			var arr = $.map( places[0],function(k,v){ return k; });

			bounds.extend(places[0].geometry.location);
			map.fitBounds(bounds);
		});
		google.maps.event.addListener(map, 'bounds_changed', function()
		{
			//console.log('bounds');
			var bounds = map.getBounds();
			searchBox.setBounds(bounds);
		});
		return this;
	},
    googleMapSet : function( LatLng )
    {
		element=$(this);
		if ( LatLng=='' ) LatLng='37.975554, 23.735463';
		LatLngArray = LatLng.split(",");
		latitude=parseFloat( LatLngArray[0] );
		longitude=parseFloat( LatLngArray[1] );
		var athensSyntagma = new google.maps.LatLng( LatLng );
		var mapOptions = {
			zoom: 12,
			center: athensSyntagma
		};
		map = new google.maps.Map( element.get(0) ,mapOptions);
		// This event listener will call addMarker() when the map is clicked.
		google.maps.event.addListener(map, 'click', function(event) { addMarker( map, event.latLng,elementId); });
		//return map;
	}
});
// Add a marker to the map and push to the array.
function addMarker(map,location,elementId)
{
	if ( map.markers )
	{
		for( i=0;i<map.markers.length; i++ )
		{
			map.markers[i].setMap(null);
		}
	}
	marker = new google.maps.Marker({
		position: location,
		map: map
	});
	element=$('#'+elementId);
	map.markers.push( marker );
	//console.log( 'id:'+ element.attr('id') );
	//console.log( 'data-input:'+ element.attr('data-input') );
	locationValue=String(location).substring(0, String(location).length - 1).substring(1);
	$( '#'+element.attr('data-input') ).val( locationValue );
	$( '#'+element.attr('data-view') ).text( locationValue );
}
