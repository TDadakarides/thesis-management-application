//// data-button-type='window , action' -> window: άνοιγμα νέου παραθύρου, action:εκτέλεση με ajax
//// data-window-target='_self _this _blank'
//// data-post-url='< file >'
//// data-post-data-type='form , data' -> form:συνδιάζεται με data-post-form, data:συνδιάζεται με data-post-data-value
//// data-post-data-value='< value >' -> δεδομένα σε serialized μορφή
//// data-post-form='< form id >'
function reloadWindows(indexWindow)
{
	var reloadWindowsIndex=typeof indexWindow !== 'undefined' ? $(".allWindowBox .windowBox[window-index='"+indexWindow+"']") : $('.allWindowBox .windowBox');
	reloadWindowsIndex.each(function ()
	{
		windowBox=$(this);
		var index=windowBox.attr('window-index')
		if ( typeof ( windowBox.attr('data-post-url') )!== 'undefined' )
		{
			url=windowBox.attr('window-index');
			url=windowBox.attr('data-post-url');
			dataValue=windowBox.attr('data-post-data-value');
			if ( dataValue!='' ) postData=$.parseJSON ( dataValue ); else postData='';			
			tableName="";
			if ( (url=='edit.php')||(url=='new.php') )
			{
				if(postData.table_name_eng !== undefined) tableName=postData.table_name_eng;
			}
			$.ajax({ type: 'POST', url: url, data: postData, async:false })
			//$.post( url, postData )
			.done(function ( data )
			{
				//console.log('------------');
				//console.log('window-index:'+ windowBox.attr('window-index') );
				//console.log('url:'+ url );
				//console.log('postData:'+ postData );
				//console.log('---data---' );
				//console.log(data);
				inline=windowBox.find('.inline');
				dataEl=( $('<html></html>').append(data) ).clone();
				windowTitle=( dataEl.find('.windowTitle').length>0 )?dataEl.find('.windowTitle').text()+" | ":"";
				productName=( dataEl.find('input.theName').length>0 )?"<span class='theName'>"+dataEl.find('input.theName').val()+" | </span>":"";
				if ( dataEl.find('form').length>0 ) { dataBox=dataEl.find('form').clone(); }
				else if ( dataEl.find('.table').length>0 ) { dataBox=dataEl.find('.table').clone(); }
				else if ( dataEl.find('.discountTable').length>0 ) { dataBox=dataEl.find('.discountTable').clone(); }
				productCategory=( typeof postData.table_comment =='undefined' )?"":postData.table_comment+' | ';
				reloadButton="<span class='reload-button' >Reload window</span>";
				dataTitle=$("<div class='windowTitle'>"+windowTitle+productCategory+productName+"<span class='windowTitleUrl'> | "+url+"</span>"+reloadButton+"</div>");
				inline.html( '' ).append( dataBox ).prepend( dataTitle );
				resizeWindowBoxes( true );
				
                clearTinyMCE();
				windowBox.find('.mceEditor').each(function(){
					var mceClass="mceEditor_"+index+"_"+(Date.now());
					var mceId=$(this).attr("id");
					$(this).addClass(mceClass);
					mediumTextMCE($(this).get(0));
					//mediumTextMCE("."+mceClass+"#"+mceId);
				});
				//mediumTextMCE();
				runDrop();
				dropzoneInit( serverHost );
				if ( windowBox.find('.reload-button').length>0 ) reloadInit(windowBox);	
				if ( $('.hosting').length>0 ) conexioLoad();
				if ( $('.pictureBox').length>0 ) pictureInit();
				if ( $('.pager-box').length>0 ) pagerInit(windowBox);
				if ( tableName=="pump_speed" ) initializeGraphPumpSpeed();
				if ( tableName=="pump_stations" ) {initializeGraphPumpSt();initializeGraphPumpStHeater();}
				if ( tableName=="antifreeze" ) initializeGraphAntifreeze();
				//if ( $('.systemElements').length>0 ) conexio200.interfaceInit();
				//console.log( '$.fn.googleMapRun :'+$.fn.googleMapRun  );
				if ( windowBox.find('.googleMap').length>0 )
				{
					//console.log('googleMap find');
					windowBox.find('.googleMap').each( function ()
					{
						LatLong=$('#'+$(this).attr('data-input')).val();
						$(this).googleMapRun( LatLong );
					});
				}
				if ( windowBox.find('.userUpdate .allReceivers').length>0 ) userLoad( $('.userUpdate .allReceivers') );
				windowBox.find(".window-loader-box").hide();
				if ( indexWindow != undefined) isLoading=false;
			});
		}
	});
}

isLoading=false;
$('.submenu .button,.buttonLogin.button,.titleGeneral .button,.mainMenu .button,.table .jsbutton,.subcategory .button,.foreign-button,.popUpBox').live('click',function ()
{
	if (!isLoading)
	{
		//tinyMCE.triggerSave();
		// tinyMCE.each(function(e){
			
		// });
		isLoading=true;
		//console.log( '-------------------------------' );
		//console.log( 'button click' );
		var $activeWindowBox=$(this).closest('.windowBox');
		var reloadAll,windowReference,windowInputReference;
		if ($activeWindowBox.length>0){
			reloadAll=typeof ( $activeWindowBox.attr('window-reload-all') ) !== 'undefined' ? $activeWindowBox.attr('window-reload-all')=='true' :true ;
			windowReference=( typeof ( $activeWindowBox.attr('window-reference-index') ) !== 'undefined' )?( $activeWindowBox.attr('window-reference-index') ):'';
			windowInputReference=( typeof ( $activeWindowBox.attr('window-input-index') ) !== 'undefined' )?( $activeWindowBox.attr('window-input-index') ):'';
		}
		var closeSidemenu=( typeof ( $(this).attr('data-close-sidemenu') ) !== 'undefined' )?( $(this).attr('data-close-sidemenu') ):'false';
		var windowGroupName=( typeof ( $(this).attr('data-window-group-name') ) !== 'undefined' )?( $(this).attr('data-window-group-name') ):'';
		var openStyle=( typeof ( $(this).attr('data-button-type') ) !== 'undefined' )?( $(this).attr('data-button-type') ):'';
		var postDataType=( typeof ( $(this).attr('data-post-data-type') ) !== 'undefined' )?$(this).attr('data-post-data-type'):'data';
		var postDataValue=( typeof ( $(this).attr('data-post-data-value') ) !== 'undefined' )?$.parseJSON( $(this).attr('data-post-data-value') ):'';
		var postDataJson=( typeof ( $(this).attr('data-post-data-value') ) !== 'undefined' )?$.parseJSON( $(this).attr('data-post-data-value') ):'';
		var postForm=( typeof ( $(this).attr('data-post-form') ) !== 'undefined' )? $('#'+ $(this).attr('data-post-form') ): $('form',$activeWindowBox);
		var dataFunction=( typeof ( $(this).attr('data-function') ) !== 'undefined' )? $(this).attr('data-function'):false;
		var triggerEmailSend=typeof ( $(this).attr('data-newsletterallCount') ) !== 'undefined' ? true :false ;
		var $allWindowBox=windowGroupName!='' ? $(".allWindowBox[data-group-name='"+windowGroupName+"']") : $(this).closest('.allWindowBox');
		//console.log( 'openStyle:'+openStyle );
		//console.log( 'postDataType:'+postDataType );
		//console.log( 'postDataJson:'+postDataJson );
		//console.log( 'postForm:'+postForm );
        
	tinyMCE.triggerSave();
		if ( dataFunction )
		{
			if ( dataFunction=='updateDiscountPostData' )
			{ postData=updateDiscountPostData(); }
			else { var postData={}; }
		}
		else if ( postDataType=='form' )
		{
            $('.multi-select:not(:checked)',postForm).each(function(){
                multiRow=$(this).closest('.multi-select-tr');
                $('input',multiRow).attr('name','');
                $('select',multiRow).attr('name','');
            });
			postData=postForm.serialize();
		}
		////POST ΑΠΟ ΔΕΔΟΜΕΝΑ
		else if ( postDataType=='data' )
		{
			postData=( postDataJson );
		}
		////ΔΕΝ ΚΑΝΕΙ POST ΚΑΠΟΙΑ ΔΕΔΟΜΕΝΑ ΕΚΤΕΛΕΙ ΜΙΑ ΕΝΕΡΓΕΙΑ
		else 
		{
			var postData={};
		}
		url=( typeof ( $(this).attr('data-post-url') ) !== 'undefined' )?$(this).attr('data-post-url'):'';
		windowTarget=( typeof ( $(this).attr('data-window-target') ) !== 'undefined' )?( $(this).attr('data-window-target') ):'_self';
		//console.log('data-button-type:'+openStyle);
		switch(openStyle)
		{
			////ΤΟ ΚΟΥΜΠΙ ΔΟΥΛΕΥΕΙ ΚΛΕΙΣΙΜΟ
			case 'scrollTop':
				$('html, body').animate( { scrollTop: 0 }, 300);
				isLoading=false;
				break;
			////ΤΟ ΚΟΥΜΠΙ ΔΟΥΛΕΥΕΙ ΚΛΕΙΣΙΜΟ
			case 'back':
				$activeWindowBox.prev('.windowBox').addClass('active');
				$activeWindowBox.remove();
				if( $('.windowBox',$allWindowBox ).length == 0 ){
					$(".mainMenu .button[data-window-group-name='"+$allWindowBox.attr('data-group-name')+"']").removeClass('active');
					$allWindowBox.remove();
				}
				if ($('.bgblack').length>0){$('.bgblack').remove();}
				resizeWindowBoxes( true );
				$('html, body').animate(
				{
					scrollTop: 0
				}, 300);
			break;
			////ΤΟ ΚΟΥΜΠΙ ΔΟΥΛΕΥΕΙ ΩΣ ΕΝΕΡΓΕΙΑ (ΠΧ ΑΠΟΘΗΚΕΥΣΗ, ΕΝΗΜΕΡΩΣΗ ΚΑ)
			case 'action':
				if (! validateForm($activeWindowBox) ){
					isLoading=false;
					return;
				}
				confirmValue=true;
				if( $(this).hasClass('delete') )
				{
					confirmValue=confirm("Θα πραγματοποιηθεί διαγραφή εγγραφής.\nΣυνέχεια;");
				}
				if (confirmValue)
				{
					//console.log('click');
					//console.log('postData:'+postData);
					if ( typeof ( $(this).attr('data-button-pictureSaveFirst') ) !== 'undefined' )
					{
						if ( $(this).closest('table.table').find('.thumb_container').find('.thumbs.active_photo').length>0 )
						picId=( $(this).closest('table.table').find('.thumb_container').find('.thumbs.active_photo').attr('id') ).replace('thumbs','');
						else if ( $(this).closest('table.table').find('.thumb_container').find('.thumbs').length>0 )
						picId=( $(this).closest('table.table').find('.thumb_container').find('.thumbs').attr('id') ).replace('thumbs','');
						else
						picId=false;
						postData['pic_ID']=picId;
					}
					if ( $('.dataMsg').length>0)  $('.dataMsg').remove();
					var dataMsg=$("<div><div class='msgBox'><div class='loading'>Φόρτωση</div></div></div>").addClass('dataMsg');
					dataMsg.appendTo('body');
					$.post( url, postData )
					.done(function ( dataResponse )
					{
						var dataMsg="Server error;";
						if (dataResponse === null) {
							dataMsg="Nullable Error";
						}
						else if (dataResponse === undefined) {
							dataMsg="Undefined Error";
						}
						else if (dataResponse.constructor === "test".constructor) {							
							try {
								var dataResponseJson=$.parseJSON(dataResponse);
								var isSuccessed=(dataResponseJson.success!=null);
								if(isSuccessed){
									dataMsg=dataResponseJson.success;
								}else{
									dataMsg=dataResponseJson.error;
								}
							} catch (e) {
								dataMsg=dataResponse;
							}
						}
						else if (dataResponse.constructor === objectConstructor) {
							var dataResponseJson=$.parseJSON(dataResponse);
							var isSuccessed=(dataResponseJson.success!=null);
							if(isSuccessed){
								dataMsg=dataResponseJson.success;
							}else{
								dataMsg=dataResponseJson.error;
							}
						}
						if ($('.bgblack').length>0){$('.bgblack').remove();}
						$('.dataMsg .loading').remove();
						$('.dataMsg .msgBox').html( "<p>"+dataMsg+"</p>" ).prepend("<div class='closeButton'>X</div>");
						if ( triggerEmailSend ) { newsletterSend(); }
						if ( reloadAll ) {
							reloadWindows();
						}
						if ( windowInputReference!='' && windowReference!='' )
						{
							var refValue=dataResponseJson.new_id;
							var refValuetext=dataResponseJson.reference_field;
							reloadForeign(windowReference,windowInputReference,refValue,refValuetext);
						}
						if(isSuccessed) {
							$('.dataMsg').fadeOut(6000,function () { $('.dataMsg').remove(); });
						}
						if ( $('.userUpdate .allReceivers',$activeWindowBox).length>0 ) userLoad( $('.userUpdate .allReceivers') );
					})
					.error(function (data)
					{
						if ($('.bgblack').length>0){$('.bgblack').remove();}
						$('.dataMsg .loading').remove();
						$('.dataMsg .msgBox').html( data ).prepend("<div class='closeButton'></div>");
					});
				}
				isLoading=false;
			break;
			////ΤΟ ΚΟΥΜΠΙ ΔΟΥΛΕΥΕΙ ΩΣ POST ΚΑΙ ΕΜΦΑΝΙΖΕΙ ΤΑ ΑΠΟΤΕΛΕΣΜΑΤΑ ΣΕ ΚΑΠΟΙΟ ΠΑΡΑΘΥΡΟ
			case 'window':
				////POSTING ΣΕ ΝΕΟ ΠΑΡΑΘΥΡΟ Η ΣΤΟ ΙΔΙΟ ΠΑΡΑΘΥΡΟ
				if ( windowTarget=='_self' || windowTarget=='_blank' )
				{
					if (postDataType=='form')
					{
						postForm.attr('target',windowTarget).attr('method','post').attr('action',url).submit();
					}
					else if (postDataType=='data')
					{
						_form=$("<form></form>").attr('target',windowTarget).attr('method','post').attr('action',url);
						$.each( postData,function(key, value)
						{
							$("<input/>").attr("name",key).attr("id",key).val( value ).appendTo(_form);
						});		
						_form.hide().appendTo('body');
						_form.submit();			
					}
					isLoading=false;
				}
				////POSTING ΚΑΙ ΕΜΦΑΝΙΣΗ ΑΠΟΤΕΛΕΣΜΑΤΩΝ ΜΕ ΔΗΜΙΟΥΡΓΙΑ WINDOWS ΣΤΟ ΙΔΙΟ WINDOWS
				else if (windowTarget=='_this') 
				{					
					if(closeSidemenu=="true") {
						$('.allwindowBoxes').removeClass("active-side-menu");
						$(".sidemenubox").hide();
						}
					var activeWindowGroupName = $(".allWindowBox.active").data("group-name");
					if (windowGroupName!="")
					{	
						$(".allWindowBox").removeClass('active');
						if ( $(".allWindowBox[data-group-name='"+windowGroupName+"']").length>0 ) 
						{
							$(".allWindowBox[data-group-name='"+windowGroupName+"']").addClass('active').show();
							// allWindowBox=$(".allWindowBox[data-group-name='"+windowGroupName+"']");
							// allWindowBox.clone().appendTo('.allwindowBoxes');
							// allWindowBox.remove();
						}
						else
						{					
							$allWindowBox=$('<div></div>')
								.addClass('allWindowBox')
								.addClass('active')
								.attr('data-group-name',windowGroupName)
								.appendTo( $('.allwindowBoxes') );
						}
					}
					//allWindowBox=$('.allWindowBox').last().show();
					$('.windowBox',$allWindowBox).removeClass('active');
					if ( windowGroupName!="" && $('.windowBox',$allWindowBox).length>0 && activeWindowGroupName!=windowGroupName )
					{
						$activeWindowBox=$('.windowBox',$allWindowBox).last().addClass('active');
						$activeWindowBox.find('.mceEditor').each(function(){
							var mceClass="mceEditor_"+index+"_"+(Date.now());
							var mceId=$(this).attr("id");
							$(this).addClass(mceClass);
							mediumTextMCE($(this).get(0));
						});
						isLoading=false;
						return;
					}
					else
					{
						$activeWindowBox=$('<div></div>');
						$activeWindowBox
							.attr('data-post-url',url);
						var index=$('.windowBox',$allWindowBox).last().attr('window-index');
						if (index==null) index=1; else index=parseInt(index)+1;
						$activeWindowBox
							.attr('window-index',index);
						if (postData!='') 
							$activeWindowBox.attr('data-post-data-value',JSON.stringify(postData) ); 
						else 
							$activeWindowBox.attr('data-post-data-value','' );
						loadingBox=$("<div><div class='loading'></div></div>");
						var loaderBox=$("<div class='window-loader-box'></div>");
						$activeWindowBox
							.addClass('windowBox')
							.addClass('active')
							.addClass('loadingWindow')
							.append(loadingBox)
							.appendTo($allWindowBox);//////////ΘΑ ΠΡΕΠΕΙ ΝΑ ΦΤΙΑΞΩ ΕΝΑ BOX ΠΟΥ ΘΑ ΕΙΣΑΓΩ ΜΕΣΑ ΤΑ ΠΑΡΑΘΥΡΑ - ΜΕ ΚΙΝΗΣΗ
					}
					$.post( url, postData )
					.done(function ( data )
					{
						dataEl=( $('<html></html>').append(data) ).clone();
						windowTitle=( dataEl.find('.windowTitle').length>0 )?dataEl.find('.windowTitle').text()+" | ":"";
						productName=( dataEl.find('input.theName').length>0 )?"<span class='theName'>"+dataEl.find('input.theName').val()+" | </span>":"";
						if ( dataEl.find('form').length>0 ) { dataBox=dataEl.find('form').clone(); }
						else if ( dataEl.find('.table').length>0 ) { dataBox=dataEl.find('.table').clone(); }
						else if ( dataEl.find('.discountTable').length>0 ) { dataBox=dataEl.find('.discountTable').clone(); }
						else { dataBox=dataEl.clone(); }
						tableName="";
						if ( (url=='edit.php')||(url=='new.php') )
						{
							if(postData.table_name_eng !== undefined) tableName=postData.table_name_eng;
						}
						productCategory=( typeof postData.table_comment =='undefined' )?"":postData.table_comment+' | ';
						reloadButton="<span class='reload-button' >Reload window</span>";
						dataTitle=$("<div class='windowTitle'>"+windowTitle+productCategory+productName+"<span class='windowTitleUrl'>"+url+"</span>"+reloadButton+"</div>");
						newBox=$("<div class='inline'></div>");
						newBox.append( dataTitle );
						dataBox.htmlClean();
						//dataBox.html( ( dataBox.html() ).replace(/ /g,'')  );
						newBox.append( dataBox );
						//var windowBox=allWindowBox.find('.windowBox.loadingWindow');
						$activeWindowBox.html('');
						if ( dataEl.find('.submenu').length>0 )
						{
							submenu=dataEl.find('.submenu').clone();
							submenu.find('span').each(function ()
							{
								button=$(this).closest('.button');
								$(this).closest('.button').find('.innerbutton').remove();
								cloneSpan=$(this).clone();
								//cloneSpan.insertAfter( button );
								cloneSpan.appendTo( button );
								$(this).remove();
							});
							upBox=$("<div class='upBox'></div>");
							upRightBox=$("<div class='upRightBox'></div>");
							bottomBox=$("<div class='bottomBox'></div>");
							scrollTop=$("<div class='button scrollTop' data-button-type='scrollTop'><div class='image'></div><span>Top</span></div>");
							//scrollTop=$("<div class='button scrollTop' data-button-type='scrollTop'><div class='innerbutton'></div><div class='image'></div><span>Top</span></div>");
							submenu.append( upBox ).append( upRightBox ).append( bottomBox ).append( scrollTop );
							$activeWindowBox.append( submenu );
						}
						var loaderBox=$("<div class='window-loader-box'></div>");
						$activeWindowBox
							.append( newBox )
							.append( loaderBox )
							.removeClass('loadingWindow');
						var index=$activeWindowBox.attr('window-index');
                        clearTinyMCE();
						$('.mceEditor',$activeWindowBox).each(function(){
							var mceClass="mceEditor_"+index+"_"+(Date.now());
							var mceId=$(this).attr("id");
							$(this).addClass(mceClass);
							mediumTextMCE($(this).get(0));
						});
						//mediumTextMCE();
						runDrop();
						dropzoneInit( serverHost );
						if ( $('.hosting').length>0 ) conexioLoad();
						if ( $('.pager-box').length>0 ) pagerInit($activeWindowBox);
						if ( $('.reload-button',$activeWindowBox).length>0 ) reloadInit($activeWindowBox);						
						//console( 'tableName :'+tableName  );
						if ( tableName=="pump_speed" ) initializeGraphPumpSpeed();
						if ( tableName=="pump_stations" ) {initializeGraphPumpSt();initializeGraphPumpStHeater();}
						if ( tableName=="antifreeze" ) initializeGraphAntifreeze();
						resizeWindowBoxes( true );
						//if ( $('.systemElements').length>0 ) conexio200.interfaceInit();
						//console.log( '$.fn.googleMapRun :'+$.fn.googleMapRun  );
						if ( $('.googleMap',$activeWindowBox).length>0 )
						{
							//console.log('googleMap find');
							$('.googleMap',$activeWindowBox).each( function ()
							{
								LatLong=$('#'+$(this).attr('data-input')).val();
								$(this).googleMapRun( LatLong );
							});
						}
						if ( $('.userUpdate .allReceivers',$activeWindowBox).length>0 ) userLoad( $('.userUpdate .allReceivers') );
					});
					$('html, body').animate(
					{
						scrollTop: 0
					}, 300);
				}
				else if (windowTarget=='_sideCategory')
				{
					$('.sidemenubox').hide();
					$(".allWindowBox").removeClass('active');
					$allWindowBoxes=$('.allWindowBoxes');
					$allWindowBoxes.addClass("active-side-menu");
					
					if ($(".sidemenubox[data-group-name='"+windowGroupName+"']").length>0)
					{
						$sideMenuBox=$(".sidemenubox[data-group-name='"+windowGroupName+"']");
						$sideMenuBox
						//.setActive(postDataValue.categoryId)
						.show();
					}
					else
					{
						$.post(url,postData)
						.done(function ( data )
						{
							$data=( $('<html></html>').append(data) ).clone();
							$sideMenuBox=$("<div class='sidemenubox data-group-name='"+windowGroupName+"'></div>");
							if($data.find(".category-ul").length>0)
							{
								$sideMenuBox.append($data);
							}
							else
							{
								$sideMenuBox.append("<p>No data found</p>");
							}
							$sideMenuBox.appendTo($allWindowBoxes).show();
							fixAllHeights();
							isLoading=false;
						});
					}
				}
				else if (windowTarget=='_pop_up')
				{
					if ( $('.bgBlack').length>0 ) { $('.bgBlack').remove(); }
					bgBlack=$("<div></div>");
					bgBlack
					.addClass('bgBlack');
					bgBlack.appendTo('body').fadeIn(800);
                    tableName="";
					dataTitle=$("<div class='windowTitle'><span class='title'>"+tableName+"</span><span class='filename'> | "+url+"</span><div class='closeButton'>X</div></div>");
					viewWindow=$("<div class='viewWindow'></div>");
					viewWindow.prepend( dataTitle );
					table=postData.table;
					//console.log(table);
					$activeWindowBox=$('<div></div>');
					$activeWindowBox
						.addClass( table )
						.addClass('active')
						.addClass('windowBox')
						.addClass('windowBoxPopUp')
						.addClass('loadingWindow')
						.attr('data-post-url',url);
					index=1;
					$('.windowBox').each( function () 
					{ 
						index=( parseInt( $(this).attr('window-index') )>index )? parseInt( $(this).attr('window-index') ):index; 
					});
					index=index+1;
					$activeWindowBox
						.attr('window-index',index);
					var windowReference=$(this).closest('.windowBox').attr('window-index');
					var inputReference=$(this).data('input-reference');
					$activeWindowBox
						.attr('window-reload-all',false)
						.attr('window-input-index',inputReference)
						.attr('window-reference-index',windowReference);
					if (postData!='') 
						$activeWindowBox.attr('data-post-data-value',JSON.stringify(postData) ); 
					else 
						$activeWindowBox.attr('data-post-data-value','' );
					loadingBox=$("<div><div class='loading'></div></div>");
					$activeWindowBox
						.append( loadingBox )
						.prepend( viewWindow )
						.appendTo( bgBlack );
					loadWindow( index );
					//$('html, body').animate( { scrollTop: 0 }, 300);				
				}
			break;	
		}
	}
	
});
/////φορτωση νεου παραθυρου
function loadWindow( index )
{
	var windowBox=$(".windowBox[window-index='"+index+"']");
	url=windowBox.attr('data-post-url');
	postData=$.parseJSON( windowBox.attr('data-post-data-value') );
	if ( windowBox.find('.table').find('.table_name.active').length>0 )
	{
		activeField=windowBox.find('.table .table_name.active').find('.click.reload.aiCode').text();
		windowBox.attr('data-table-active-field',activeField );
	}
	else
	{
		windowBox.attr('data-table-active-field','' );
	}
	$.post( url, postData )
	.done(function ( data )
	{
		returnData=( $('<html></html>').append(data) ).clone();
		if ( returnData.find('form').length>0 ) { dataBox=returnData.find('form').clone(); }
		else if ( returnData.find('.table').length>0 ) { dataBox=returnData.find('.table').clone(); }
		else if ( returnData.find('.discountTable').length>0 ) { dataBox=returnData.find('.discountTable').clone(); }
		else { dataBox=returnData.clone(); }
		viewWindow=windowBox.find(".viewWindow");
		viewWindow.find('.table').remove( );
		viewWindow.append( dataBox );
		windowBox.find('.loading').remove();
		tableName=viewWindow.find('.title').text();
		activeField=windowBox.attr( 'data-table-active-field' );
		if ( activeField!='' ) var activeFields=windowBox.find('.aiCode').filter(function() {return $(this).text() === activeField;});
		if (activeFields) activeFields.each(function (){ if (($(this).text()).length==activeField.length) $(this).closest('tr.table_name').addClass('active');});
		windowBox.attr('data-table-active-field','' );
		if ( returnData.find('.submenu').length>0 )
		{
			windowBox.find('.submenu').remove();
			submenu=returnData.find('.submenu').clone();
			scrollTop=$("<div class='button scrollTop' data-button-type='scrollTop'><div class='innerbutton'></div><div class='image'></div><span>Top</span></div>");
			submenu.find('.jsbutton.edit,.button.new,.jsbutton.photo').attr('data-table-name',tableName);
			if ( activeField!='' ) submenu.find('.button.edit,.button.delete').addClass('enable');
			windowBox.append( submenu );
		}
		windowBox.removeClass('loadingWindow');
		///// ALL MY PLUGIN/////////////////
		//resizeWindowBoxes( true );
        clearTinyMCE();
		windowBox.find('.mceEditor').each(function(){
			var mceClass="mceEditor_"+index+"_"+(Date.now());
			var mceId=$(this).attr("id");
			$(this).addClass(mceClass);
			mediumTextMCE($(this).get(0));
		});
		//mediumTextMCE();
		runDrop();
		dropzoneInit( serverHost );
		// sorting( serverHost );
		if ( windowBox.find('.reload-button').length>0 ) reloadInit(windowBox);	
		if ( windowBox.find('.pictureBox').length>0 ) pictureInit();
		if ( windowBox.find('.pager-box').length>0 ) pagerInit(windowBox);
		if ( windowBox.find('.googleMap').length>0 )
			windowBox.find('div.googleMap').each(function ()
			{
				dataInput=$('#'+$(this).attr('data-input') ).val();
				$(this).googleMapRun( dataInput );
			});
		if ( windowBox.find('.googleMapMulty').length>0 ) windowBox.find('.googleMapMulty').googleMapMultyRun( );
		////////////////////////////////////		
		// isLoadedWindowNumber++;
		// if (isLoadedWindowNumber==loadingWindowNum)
		// {
			// isLoadedWindowNumber=0;
			// loadingWindowNum=1;
		// }
		isLoading=false;
	});
}
function resizeWindowBoxes(move)
{
	move = typeof move !== 'undefined' ? move : false;
	$allWindowBox = $('.allWindowBox.active');
	//$('.windowBox:not(:last-child)',$allWindowBox).hide();
	//$('.windowBox:last-child',allWindowBoxActive).show();
	
	// allWindowBox=$('.allWindowBox').last();
	// var totalWidth = 0;
	// //allWindowBox.find('.windowBox:not(:last-child)').each(function(){ totalWidth += ( $(this).outerWidth( true ) );});
	// allWindowBox.find('.windowBox').each(function(){ totalWidth += ( $(this).outerWidth( true ) );});
	// var windowBox=allWindowBox.find('.windowBox').last();
	// windowBoxWidth=windowBox.outerWidth( true );
	// //windowBoxWidth=2;
	// newTotalWidth=totalWidth;
	// //allWindowBox.width( newTotalWidth );
	// //console.log('windows width without new window:'+totalWidth);
	// //console.log('new windowBox length:'+$('.allWindowBox .windowBox:last-child').length);
	// //console.log('new windowBox Width:'+windowBoxWidth);
	// //console.log('new Total Width:'+newTotalWidth );
	// //console.log('------------------------------' );
	isLoading=false;
	if (move)
	{
		// //maxHeight = Math.max.apply(null, $(".windowBox").map(function () { return $(this).height(); }).get());
		// maxHeight = allWindowBox.find(".windowBox").last().height();
		// //console.log('allWindowBox height:'+maxHeight );
		// var left=(-1)*totalWidth+windowBoxWidth;
		// allWindowBox
		// //.height( maxHeight )
		// //.animate({marginLeft: left+'px'}, 1200,function () { isLoading=false; });
		// .css({marginLeft: left+'px'});isLoading=false;
		// //console.log('allWindowBox left:'+$('.allWindowBox').offset().left);
		// //console.log('allWindowBox new left:'+left);
		
	}
	else
	{
		//isLoading=false;
	}
	//console.log('new windowBox img length:'+windowBox.find('img').length);
}
$('.dataMsg .closeButton').live('click',function () {  $('.dataMsg').fadeOut(600,function () { $('.dataMsg').remove(); })  });
$('input.theName').live('change keyup',function () {  $(this).closest('.windowBox').find('span.theName').first().text( $(this).val() ); }) 
$('.mainMenu .button').live('click',function ()
{
	$('.mainMenu .button.active').removeClass('active');
	$(this).addClass('active');
}) 
jQuery.fn.htmlClean = function() {
	//console.log('htmlClean running:');
    this.contents().filter(function() {
        if (this.nodeType != 3) {
            //$(this).htmlClean();
            return false;
        }
        else {
            this.textContent = $.trim(this.textContent);
            return !/\S/.test(this.nodeValue);
        }
    }).remove();
    return this;
}
$('.menuButton').live('click',function()
{
	$('.mainMenu > .button').toggle();
});
$(window).resize(function ()
{
	fixAllHeights();
	
});
$(window).ready(function ()
{
	fixAllHeights();
});
function fixAllHeights()
{	
	menuHeight=$('.mainMenu').first().height();
	$('.allwindowBoxes').css('padding-top',parseInt(menuHeight+22)+'px');
	windowHeight=$(window).height();
	$('.sidemenubox').height(windowHeight-parseInt(menuHeight+22)).css('top',parseInt(menuHeight+22)+'px');
}
function reloadForeign(windowReference,windowInputReference,refValue,refValuetext){
	var windowBox=$(".windowBox[window-index='"+windowReference+"']");
	var foreignInput=windowBox.find('#'+windowInputReference);
	var foreignBox=foreignInput.closest('tr').find('.foreignBox');
	var popupBox=foreignInput.closest('tr').find('.popUpBox');
	var foreignBoxTr=windowBox.find(".foreignBoxTr[data-reference-id='"+windowInputReference+"']");
	
	foreignInput.val(refValue);
	foreignBox.find('span').text(refValuetext);
	popupBox.find('span').text(refValuetext);
	popupBox.attr('data-post-url','edit.php');
	var jsonPostDataValue=popupBox.attr('data-post-data-value');
	var jsonPostDataValueJson=JSON.parse(jsonPostDataValue);
	jsonPostDataValueJson.field_val=refValue;
	popupBox.attr('data-post-data-value',JSON.stringify(jsonPostDataValueJson) );
	foreignBoxTr.remove();
}
function validateForm($windowBox){
	tinyMCE.triggerSave();
	var isValid=true;
	$("*[data-required]",$windowBox).each(function(){
		var value=$(this).val();
		if(value==""){
			isValid=false;
			$(this).closest("tr").find(".field-name").addClass("invalid");
		}else{
			$(this).closest("tr").find(".field-name").removeClass("invalid");
		}
			
	});
	return isValid;
}
function  pagerInit(windowBox){
	windowBox.find(".page-index:not(.active)")
		.off("click")
		.on("click",function(){
		if (!isLoading){
			isLoading=true;
			windowBox.find('.window-loader-box').show();
			page=$(this).data("page");
			var url=windowBox.attr('data-post-url');
			var jsonPostDataValue=windowBox.attr('data-post-data-value');
			var jsonPostDataValueJson=JSON.parse(jsonPostDataValue);
			jsonPostDataValueJson.page=page;
			windowBox.attr('data-post-data-value',JSON.stringify(jsonPostDataValueJson) );
			windowIndex=windowBox.attr('window-index');
			setTimeout( function(){ reloadWindows( windowIndex );},100);
		}
	});
	windowBox.find(".page-select")
		.off("change")
		.on("change",function(){
		if (!isLoading){
			isLoading=true;
			windowBox.find('.window-loader-box').show();
			rowsPerPage=$(this).val();
			var url=windowBox.attr('data-post-url');
			var jsonPostDataValue=windowBox.attr('data-post-data-value');
			var jsonPostDataValueJson=JSON.parse(jsonPostDataValue);
			jsonPostDataValueJson.rowsPerPage=rowsPerPage;
			jsonPostDataValueJson.page=1;
			windowBox.attr('data-post-data-value',JSON.stringify(jsonPostDataValueJson) );
			windowIndex=windowBox.attr('window-index');
			setTimeout( function(){ reloadWindows( windowIndex );},100);
		}
	});
	windowBox.find(".sorted")
		.off("click")
		.on("click",function(){
		if (!isLoading){
			isLoading=true;
			windowBox.find('.window-loader-box').show();
			var sorted=$(this).data("field-sort");
			var sortedBy=$(this).data("sort-by");
			var url=windowBox.attr('data-post-url');
			var jsonPostDataValue=windowBox.attr('data-post-data-value');
			var jsonPostDataValueJson=JSON.parse(jsonPostDataValue);
			jsonPostDataValueJson.sorted=sorted;
			jsonPostDataValueJson.sort_asc=sortedBy;
			jsonPostDataValueJson.page=1;
			windowBox.attr('data-post-data-value',JSON.stringify(jsonPostDataValueJson) );
			windowIndex=windowBox.attr('window-index');
			setTimeout( function(){ reloadWindows( windowIndex );},100);
		}
	});
}
function reloadInit(windowBox){
	windowBox.find('.clear-attachment')
	.on('click',function(){
		var $this=$(this);
		var input= $('#'+$this.data('inputid'));
		input.val('');
		var filename = $('.'+$this.data('filename'));
		$('input',filename).val('');
		$this.addClass('hidden');
	});

	windowBox.find('.reload-button')
	//.off('click')
	.on('click',function(){
		if (!isLoading){
			isLoading=true;
			windowBox.find('.window-loader-box').show();
			var windowIndex=$(this).closest(".windowBox").attr("window-index");
			setTimeout( function(){ reloadWindows( windowIndex );},100);
		}
	});
}
