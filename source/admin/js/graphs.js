var x_pub, y_pub, stepx_pub, stepy_pub,graphTableRef;
var graphValue={};
function clearCanvas(context, canvas) {
	context.clearRect(0, 0, canvas.width, canvas.height);
	var w = canvas.width;
	canvas.width = 1;
	canvas.width = w;
}
function keySort(key_,array_) {
  return function(a,b){
   return array_ ? ~~(a[key_] < b[key_]) : ~~(a[key_] > b[key_]);
  }
}
function makegraph(canv, graphMax_x, graphMax_y,graphMin_y,stepx, stepy) {
	console.log("-----------------------------------");
	///////ΔΗΜΙΟΥΡΓΙΑ ΚΑΜΒΑ//////////////////
	var canvas_dom = document.getElementById(canv);
	$('#'+canv).next('.canvasBox').remove();
	canvas_div = $("<div class='canvasBox'></div>");
	canvas_div.css({'top':canvas_dom.offsetTop+'px','left':$('#'+canv).css('padding-left'),'width':canvas_dom.offsetWidth+'px','height':canvas_dom.offsetHeight+'px'})
	.insertAfter('#'+canv);
	var canvas = canvas_dom.getContext("2d");
	clearCanvas(canvas, canvas_dom);
	//////ΑΡΧΙΚΕΣ ΤΙΜΕΣ ΠΑΡΑΜΕΤΡΩΝ//////
	if (!graphMax_x && graphMax_y<0) graphMax_x = 0;
	if (graphMax_y<0) graphMax_y = 0;
	if (!stepx) stepx = 1;
	if (!stepy) stepy = 1;
	if (graphMax_y==0 && isNaN(graphMin_y) ) graphMin_y = -10;
	if (isNaN(graphMin_y) || graphMin_y>0) graphMin_y = 0;
	/////ΣΤΡΟΓΓΥΛΟΠΟΙΗΣΗ ΤΩΝ ΑΚΡΩΝ ΤΟΥ ΑΞΟΝΑ Υ//////////////
	total_y=graphMax_y-graphMin_y;
	var numOfDigits = Math.floor(Math.log(Math.abs(total_y))/Math.LN10);numOfDigits=(isNaN(numOfDigits)||!isFinite(numOfDigits))?1:numOfDigits;
	graphMax_y=Math.ceil( graphMax_y/Math.pow(10,numOfDigits-1) )*Math.pow(10,numOfDigits-1);
	graphMin_y=Math.floor( graphMin_y/Math.pow(10,numOfDigits-1) )*Math.pow(10,numOfDigits-1);
	console.log("stepy 1:"+stepy);////<<<<<------------
	maxDigitsValue=(Math.abs(graphMax_y)>Math.abs(graphMin_y))?Math.abs(graphMax_y):Math.abs(graphMin_y);
	stepy=Math.ceil( maxDigitsValue/Math.pow(10,numOfDigits) )*Math.pow(10,numOfDigits);
	console.log("numOfDigits:"+numOfDigits);////<<<<<------------	
	console.log("graphMax_y:"+graphMax_y);////<<<<<------------	
	console.log("stepy 2:"+stepy);////<<<<<------------	
	if (stepy==0) stepy=1;
	numOfSteps=Math.ceil(total_y/stepy);
	var fixedDigits=(numOfDigits<0)?Math.abs(numOfDigits)+1:0;
	if (numOfSteps<2) {stepy=stepy/10;fixedDigits++;}
	else if (numOfSteps==2) {stepy=stepy/10;}
	else if (numOfSteps<5) {stepy=stepy/4;fixedDigits++;}
	else if (numOfSteps==5) {stepy=stepy/2;fixedDigits++;}
	console.log("stepy 3:"+stepy);////<<<<<------------	
	if ((graphMax_y%stepy)!=0) graphMax_y=graphMax_y-(graphMax_y%stepy)+stepy;
	console.log("stepy 4:"+stepy);////<<<<<------------	
	if ((graphMin_y%stepy)!=0) graphMin_y=graphMin_y-(graphMin_y%stepy)-stepy;
	console.log("stepy 5:"+stepy);////<<<<<------------	
	total_y=graphMax_y-graphMin_y;
	///////////////////
	var stringDigits=Math.abs(numOfDigits)+2;
	var padding=6;
	var left = 10*stringDigits+padding;
	var topValue = 30;
	var width = $("#" + canv).width() - left;
	var height = $("#" + canv).height() - topValue*2;
	if (total_y!=0) {scaley = height * (stepy/ total_y);} else {scaley=1;}
	///////////	
	var i = graphMin_y;
	for (tempy = topValue + height; tempy > topValue; tempy = tempy - scaley) {
		canvas.beginPath();
		canvas.strokeStyle = '#C8C8C8';
		canvas.lineWidth = 0.5;
		canvas.moveTo(left, tempy);
		canvas.lineTo(left + width, tempy);
		canvas.stroke();
		///αρχη μετρησης
		spanTag=$('<span></span>');
		spanTag.css({'top':(tempy - (scaley / 2)) + 'px','height':(scaley) + 'px','left':padding+'px','width':'16px','text-align':'right','line-height':scaley + 'px'})
		.addClass('dynamicSpanSide')
		.text(i.toFixed(fixedDigits))
		.appendTo(canvas_div);
		i = i + stepy;
	}
	scalex = width * (stepx / graphMax_x);
	var len = Math.floor(Math.log(Math.abs(stepx))/Math.LN10);
	len=(isNaN(len)||!isFinite(len))?1:len;
	//console.log("len graphMax_x:"+len);
	fixedDigits=(len<0)?(-len)+1:0;	
	//console.log("fixedDigits graphMax_x:"+fixedDigits);
	stepCounter=(graphMax_x/stepx);
	if (stepCounter>40) {stepCounter=stepCounter/5;scalex*=5;stepx*=5;}
	if (stepCounter>20) {stepCounter=stepCounter/4;scalex*=4;stepx*=4;}
	if (stepCounter>12) {stepCounter=stepCounter/2;scalex*=2;stepx*=2;}
	if (stepCounter<2) {stepCounter=stepCounter*5;scalex/=5;stepx/=5;}
	if (stepCounter<5) {stepCounter=stepCounter*2;scalex/=2;stepx/=2;}
	var i = 0;
	if(scalex==0) scalex=1;
	for (tempx = left; tempx <= left + width; tempx = tempx + scalex) {
		canvas.beginPath();
		canvas.strokeStyle = '#C8C8C8';
		canvas.lineWidth = 0.5;
		canvas.moveTo(tempx, topValue);
		canvas.lineTo(tempx, topValue + height);
		canvas.stroke();
		///αρχη μετρησης
		spanTag=$('<span></span>');
		spanTag.css({'top':topValue + height + 2 + 'px' + 'px','height':'18px','left':(tempx - (scalex / 2)) + 'px','width':(scalex) + 'px','text-align':'center','line-height':'18px'})
		.addClass('dynamicSpanBottom')
		.text(i.toFixed(fixedDigits))
		.appendTo(canvas_div);
		i = i + stepx;
	}
	metatopisi_y=height*(Math.abs(graphMin_y)/total_y);
	//////ΕΙΣΑΓΩΓΗ ΣΤΙΣ PUBLIC ΤΙΜΕΣ
	graphValue.graphMax_x=graphMax_x;
	graphValue.graphMin_y=graphMin_y;
	graphValue.graphMax_y=graphMax_y;
	graphValue.left=left;
	graphValue.topValue=topValue;
	graphValue.width=width;
	graphValue.height=height;
	graphValue.padding=padding;
	graphValue.total_y=total_y;
	graphValue.scalex=scalex;
	graphValue.scaley=scaley;
	graphValue.stepx=stepx;
	graphValue.stepy=stepy;
	graphValue.metatopisi_y=metatopisi_y;
	////////////////////////
	stepx_pub = stepx;
	stepy_pub = stepy;
	canvas.beginPath();
	canvas.strokeStyle = '#808080';
	canvas.lineWidth = 2;
	canvas.moveTo(left, topValue);
	canvas.lineTo(left, topValue + height);
	canvas.stroke();
	canvas.beginPath();
	canvas.strokeStyle = '#808080';
	canvas.lineWidth = 2;
	canvas.moveTo(left, topValue + height-metatopisi_y );
	canvas.lineTo(left + width, topValue + height-metatopisi_y );
	canvas.stroke();
}
function drawgraph(canv, a, b, c, minValue_x,min_y, max_x,max_y) {
	graphMax_x=graphValue.graphMax_x;
	graphMin_y=graphValue.graphMin_y;
	graphMax_y=graphValue.graphMax_y;
	left=graphValue.left;
	topValue=parseInt(graphValue.topValue);
	width=graphValue.width;
	height=graphValue.height;
	padding=graphValue.padding;
	max_y=graphMax_y;
	total_y=graphValue.total_y;
	scalex=graphValue.scalex;
	scaley=graphValue.scaley;
	stepx=graphValue.stepx;
	stepy=graphValue.stepy;
	metatopisi_y=graphValue.metatopisi_y;
	var canvas_dom = document.getElementById(canv);
	var canvas = canvas_dom.getContext("2d");
	if (!max_x) max_x = graphMax_x;
	if (!minValue_x) minValue_x = 0;
	canvas.beginPath();
	temp_x = minValue_x;
	temp_y = (a * temp_x * temp_x + b * temp_x + c);
	step_draw = graphMax_x / 100;
	if (step_draw==0) step_draw=1;
	canvas.stroke();
	if (total_y!=0) {scaley = height/total_y;} else {scaley=1;}
	if (graphMax_x!=0) {scalex = width/graphMax_x;} else {scalex=1;}
	console.log("a:"+a);////<<<<<------------
	console.log("b:"+b);////<<<<<------------
	console.log("c:"+c);////<<<<<------------
	console.log("max_x:"+max_x);////<<<<<------------	
	console.log("graph x:[ "+minValue_x+" ~ "+graphMax_x+" ]");////<<<<<------------	
	console.log("graph y:[ "+graphMin_y+" ~ "+graphMax_y+" ]");////<<<<<------------	
	console.log("x:[ "+minValue_x+" ~ "+max_x+" ]");////<<<<<------------	
	console.log("y:[ "+min_y+" ~ "+max_y+" ]");////<<<<<------------	
	console.log("y dt:"+total_y);////<<<<<------------	
	console.log("scalex:"+scalex);////<<<<<------------	
	console.log("scaley:"+scaley);////<<<<<------------	
	console.log("stepx:"+stepx);////<<<<<------------	
	console.log("stepy:"+stepy);////<<<<<------------	
	console.log("width:"+width);////<<<<<------------
	console.log("height:"+height);////<<<<<------------
	console.log("padding:"+padding);////<<<<<------------
	console.log("top:"+topValue);////<<<<<------------
	console.log("left:"+left);////<<<<<------------
	console.log("metatopisi_y:"+metatopisi_y);////<<<<<------------	
	console.log("----------------------------");////<<<<<------------	
	canvas.moveTo(temp_x*scalex + left, topValue+height-(metatopisi_y)-(temp_y*scaley));
	for (i = minValue_x; (i <= graphMax_x) && (i <= max_x); i = i + ( step_draw)) {
		temp_x = i;
		temp_y = (a * temp_x * temp_x + b * temp_x + c);
		canvas.strokeStyle = '#f00';
		canvas.lineWidth = 2;
		canvas.lineTo(temp_x*scalex + left, topValue+height-(metatopisi_y)-(temp_y*scaley));
		canvas.stroke();
	}
	temp_x = max_x;
	temp_y = (a * temp_x * temp_x + b * temp_x + c);
	canvas.strokeStyle = '#f00';
	canvas.lineWidth = 2;
	canvas.lineTo(temp_x*scalex +left, topValue+height-(metatopisi_y)-(temp_y*scaley));
	canvas.stroke();

}
function initArray() {
	this.length = initArray.arguments.length;
	for (var i = 0; i < this.length; i++)
		this[i + 1] = initArray.arguments[i];
}

function solve(myCanvas) {

	//console.log( $('#'+myCanvas).length );
	table=$('#'+myCanvas).nextAll('table.graphValue');
	windowBox=$('.allWindowBox:visible').find('.windowBox').last();
	a12=table.find('#a12').val();
	a14=table.find('#a14').val();
	a22=table.find('#a22').val();
	a24=table.find('#a24').val();
	a32=table.find('#a32').val();
	a34=table.find('#a34').val();
	a42=table.find('#a42').val();
	a44=table.find('#a44').val();
	a52=table.find('#a52').val();
	a54=table.find('#a54').val();
	//console.log(a12+" "+a14);
	//console.log(a22+" "+a24);
	//console.log(a32+" "+a34);
	//console.log(a42+" "+a44);
	//console.log(a52+" "+a54);
	/////////////////Πρώτη καμπύλη//////////////////////////////
	var a1 = new initArray(0, parseFloat(a12, 10), 0, parseFloat(a14, 10));
	var a2 = new initArray(0, parseFloat(a22, 10), 0, parseFloat(a24, 10));
	var a3 = new initArray(0, parseFloat(a32, 10), 0, parseFloat(a34, 10));

	var t1 = a1;
	var t2 = a2;
	var t3 = a3;
	max_x=Math.max(a1[2],a2[2],a3[2]);
	max_y=Math.max(a1[4],a2[4],a3[4]);
	min_y=Math.min(a1[4],a2[4],a3[4]);
	min_x=Math.min(a1[2],a2[2],a3[2]);
	this_min_x_1=Math.min(a1[2],a2[2],a3[2]);
	this_max_x_1=Math.max(a1[2],a2[2],a3[2]);
	a1[1] = a1[2] * a1[2];
	a2[1] = a2[2] * a2[2];
	a3[1] = a3[2] * a3[2];
	a1[3] = 1;
	a2[3] = 1;
	a3[3] = 1;
	if (a1[1] == 0) {
		temp1 = a1[1];
		temp2 = a1[2];
		temp3 = a1[3];
		temp4 = a1[4];
		a1[1] = a3[1];
		a1[2] = a3[2];
		a1[3] = a3[3];
		a1[4] = a3[4];
		a3[1] = temp1;
		a3[2] = temp2;
		a3[3] = temp3;
		a3[4] = temp4;
	}
	if (a1[1] != 0) {
		var temp = a1[1];
		for (var i = 1; i <= 4; i++)
		{ a1[i] = a1[i] / temp; }
	}
	var temp = -a2[1];
	for (var i = 1; i <= 4; i++) {
		a2[i] = a2[i] + (a1[i] * temp);
	}
	var temp = -a3[1];
	for (var i = 1; i <= 4; i++) {
		a3[i] = a3[i] + (a1[i] * temp);
	}
	if (a2[2] == 0) {
		for (var i = 2; i <= 4; i++) {
			var temp = a2[i];
			a2[i] = a3[i];
			a3[i] = temp;
		}
	}
	if (a2[2] != 0) {
		var temp = a2[2];
		a2[2] = a2[2] / temp;
		a2[3] = a2[3] / temp;
		a2[4] = a2[4] / temp;
		var temp = -a3[2];
		for (var i = 2; i <= 4; i++) {
			a3[i] = a3[i] + (a2[i] * temp);
		}
		var temp = -a1[2];
		for (var i = 2; i <= 4; i++) {
			a1[i] = a1[i] + (a2[i] * temp);
		}
	}
	if (a3[3] != 0) {
		var temp = a3[3];
		a3[3] = a3[3] / temp;
		a3[4] = a3[4] / temp;
		var temp = -a2[3];
		a2[3] = a2[3] + (a3[3] * temp);
		a2[4] = a2[4] + (a3[4] * temp);
		var temp = -a1[3];
		a1[3] = a1[3] + (a3[3] * temp);
		a1[4] = a1[4] + (a3[4] * temp);
	}
	windowBox.find("#a").val(a1[4]);
	windowBox.find("#b").val(a2[4]);
	windowBox.find("#c").val(a3[4]);
	//m.a.value = a1[4];
	//m.b.value = a2[4];
	//m.c.value = a3[4];
	graph_a=a1[4];
	graph_b=a2[4];
	graph_c=a3[4];	
	////////////////////////////////////////////////////////////////////////////////////
	//////////////                 δευτερη καμπυλη         /////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	a1 = new initArray(0, parseFloat(a32, 10), 0, parseFloat(a34, 10));
	a2 = new initArray(0, parseFloat(a42, 10), 0, parseFloat(a44, 10));
	a3 = new initArray(0, parseFloat(a52, 10), 0, parseFloat(a54, 10));
	if (!a2[2]) a2[2]=0;
	if (!a3[2]) a3[2]=0;
	if (!a2[4]) a2[4]=0;
	if (!a3[4]) a3[4]=0;	
	var t1 = a1;
	var t2 = a2;
	var t3 = a3;
	max_x=Math.max(a1[2],a2[2],a3[2],max_x,0);
	max_y=Math.max(a1[4],a2[4],a3[4],max_y);
	min_y=Math.min(a1[4],a2[4],a3[4],min_y);
	min_x=Math.min(a1[2],a2[2],a3[2],min_x);
	this_max_x=Math.max(a1[2],a2[2],a3[2]);
	this_min_x=Math.min(a1[2],a2[2],a3[2]);
	a1[1] = a1[2] * a1[2];
	a2[1] = a2[2] * a2[2];
	a3[1] = a3[2] * a3[2];
	a1[3] = 1;
	a2[3] = 1;
	a3[3] = 1;
	if (a1[1] == 0) {
		temp1 = a1[1];
		temp2 = a1[2];
		temp3 = a1[3];
		temp4 = a1[4];
		a1[1] = a3[1];
		a1[2] = a3[2];
		a1[3] = a3[3];
		a1[4] = a3[4];
		a3[1] = temp1;
		a3[2] = temp2;
		a3[3] = temp3;
		a3[4] = temp4;
	}
	if (a1[1] != 0) {
		var temp = a1[1];
		for (var i = 1; i <= 4; i++)
		{ a1[i] = a1[i] / temp; }
	}
	var temp = -a2[1];
	for (var i = 1; i <= 4; i++) {
		a2[i] = a2[i] + (a1[i] * temp);
	}
	var temp = -a3[1];
	for (var i = 1; i <= 4; i++) {
		a3[i] = a3[i] + (a1[i] * temp);
	}
	if (a2[2] == 0) {
		for (var i = 2; i <= 4; i++) {
			var temp = a2[i];
			a2[i] = a3[i];
			a3[i] = temp;
		}
	}

	if (a2[2] != 0) {
		var temp = a2[2];
		a2[2] = a2[2] / temp;
		a2[3] = a2[3] / temp;
		a2[4] = a2[4] / temp;
		var temp = -a3[2];
		for (var i = 2; i <= 4; i++) {
			a3[i] = a3[i] + (a2[i] * temp);
		}
		var temp = -a1[2];
		for (var i = 2; i <= 4; i++) {
			a1[i] = a1[i] + (a2[i] * temp);
		}
	}
	if (a3[3] != 0) {
		var temp = a3[3];
		a3[3] = a3[3] / temp;
		a3[4] = a3[4] / temp;
		var temp = -a2[3];
		a2[3] = a2[3] + (a3[3] * temp);
		a2[4] = a2[4] + (a3[4] * temp);
		var temp = -a1[3];
		a1[3] = a1[3] + (a3[3] * temp);
		a1[4] = a1[4] + (a3[4] * temp);
	}
	
	windowBox.find("#a2").val(a1[4]);
	windowBox.find("#b2").val(a2[4]);
	windowBox.find("#c2").val(a3[4]);
	//m.a2.value = a1[4];
	//m.b2.value = a2[4];
	//m.c2.value = a3[4];
	graph_a2=a1[4];
	graph_b2=a2[4];
	graph_c2=a3[4];
	stepx = Math.pow(10, Math.floor(Math.log(max_x) / Math.log(10)))/10;
	//console.log("max_y:"+max_y);
	stepy = Math.pow(10, Math.floor(Math.log(max_y) / Math.log(10)))/10;
	makegraph(myCanvas, (max_x), (max_y),min_y, stepx, stepy);
	//makegraph("myCanvas", parseInt(max_x)+1, parseInt(max_y)+1, 1, 1);
	//var len = Math.floor(Math.log(Math.abs(max_y))/Math.LN10) + 1;
	//var digits = Math.floor( Math.log(this_max_x_1) ) + 1;
	//console.log("-----------drawgraph------------");
	drawgraph(myCanvas, graph_a, graph_b, graph_c, this_min_x_1,min_y, this_max_x_1,(max_y));
	if ((graph_b2!=0)||(graph_c2!=0)) drawgraph(myCanvas, graph_a2, graph_b2, graph_c2, this_min_x,min_y, this_max_x,(max_y));
	if (myCanvas=='canvasPumpSpeed')
	{
		if (inverter== 'Inverter') {
			windowBox.find("#q1").val(a22).closest('tr').addClass('hidden');
			windowBox.find("#q3").val(a42).closest('tr').addClass('hidden');
			windowBox.find("#a").val(graph_a).closest('tr').addClass('hidden');
			windowBox.find("#b").val(graph_b).closest('tr').addClass('hidden');
			windowBox.find("#c").val(graph_c).closest('tr').addClass('hidden');
			windowBox.find("#a2").val(graph_a2).closest('tr').addClass('hidden');
			windowBox.find("#b2").val(graph_b2).closest('tr').addClass('hidden');
			windowBox.find("#c2").val(graph_c2).closest('tr').addClass('hidden');
			windowBox.find("#tomi").val(parseFloat(a32, 10)).closest('tr').addClass('hidden');
			windowBox.find("#endchart").val(max_x).closest('tr').addClass('hidden');
		}else{
			windowBox.find("#q1").val(a22).closest('tr').addClass('hidden');
			windowBox.find("#q3").val(0).closest('tr').addClass('hidden');
			windowBox.find("#a").val(graph_a).closest('tr').addClass('hidden');
			windowBox.find("#b").val(graph_b).closest('tr').addClass('hidden');
			windowBox.find("#c").val(graph_c).closest('tr').addClass('hidden');
			windowBox.find("#a2").val(0).closest('tr').addClass('hidden');
			windowBox.find("#b2").val(0).closest('tr').addClass('hidden');
			windowBox.find("#c2").val(0).closest('tr').addClass('hidden');
			windowBox.find("#tomi").val(parseFloat(a32, 10)).closest('tr').addClass('hidden');
			windowBox.find("#endchart").val(max_x).closest('tr').addClass('hidden');      
		}
	}
	if (myCanvas=='canvasPumpSt')
	{
		windowBox.find("#q0").val(a12).closest('tr').addClass('hidden');
		windowBox.find("#q1").val(a22).closest('tr').addClass('hidden');
		windowBox.find("#q2").val(a32).closest('tr').addClass('hidden');
		windowBox.find("#a").val(graph_a).closest('tr').addClass('hidden');
		windowBox.find("#b").val(graph_b).closest('tr').addClass('hidden');
		windowBox.find("#c").val(graph_c).closest('tr').addClass('hidden');		
	}
	if (myCanvas=='canvasPumpStHeater')
	{
		windowBox.find("#q0_he").val(a12).closest('tr').addClass('hidden');
		windowBox.find("#q1_he").val(a22).closest('tr').addClass('hidden');
		windowBox.find("#q2_he").val(a32).closest('tr').addClass('hidden');
		windowBox.find("#a_he").val(graph_a).closest('tr').addClass('hidden');
		windowBox.find("#b_he").val(graph_b).closest('tr').addClass('hidden');
		windowBox.find("#c_he").val(graph_c).closest('tr').addClass('hidden');		
	}
	if (myCanvas=='canvasAntifreeze')
	{
		windowBox.find("#q0").val(a12).closest('tr').addClass('hidden');
		windowBox.find("#q1").val(a22).closest('tr').addClass('hidden');
		windowBox.find("#q2").val(a32).closest('tr').addClass('hidden');
		windowBox.find("#a").val(graph_a).closest('tr').addClass('hidden');
		windowBox.find("#b").val(graph_b).closest('tr').addClass('hidden');
		windowBox.find("#c").val(graph_c).closest('tr').addClass('hidden');		
	}
}
function initializeGraphPumpSpeed()
{
	graphTableRef='pumpSpeed';
	myCanvas="canvasPumpSpeed";
	//return;
	windowBox=$('.allWindowBox:visible').find('.windowBox').last();
	tomi=(windowBox.find('#tomi').val()!='')?windowBox.find('#tomi').val():0;
	endchart=(windowBox.find('#endchart').val()!='')?windowBox.find('#endchart').val():0;
	q1=(windowBox.find('#q1').val()!='')?windowBox.find('#q1').val():0;
	q3=(windowBox.find('#q3').val()!='')?windowBox.find('#q3').val():0;
	a=(windowBox.find('#a').val()!='')?windowBox.find('#a').val():0;
	b=(windowBox.find('#b').val()!='')?windowBox.find('#b').val():0;
	c=(windowBox.find('#c').val()!='')?windowBox.find('#c').val():0;
	a2=(windowBox.find('#a2').val()!='')?windowBox.find('#a2').val():0;
	b2=(windowBox.find('#b2').val()!='')?windowBox.find('#b2').val():0;
	c2=(windowBox.find('#c2').val()!='')?windowBox.find('#c2').val():0;
	if (windowBox.find('#q1').closest('tr').prev('tr').find('.matrix').length<1)
	{
		closestRow=windowBox.find('#q1').closest('tr');
		row=$("<tr></tr>");
		column=$("<td colspan='2'></td>");
		matrix=$("<div class='matrix' id='matrix' style='position:relative;'></div>");
		table=$("<table>");table.addClass('graphValue');
		tr=$("<tr></tr>");
		td=$("<td>Q(l/h)</td>");td.appendTo(tr);
		td=$("<td>H(bar)</td>");td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a12'/>");td=$("<td></td>");input.val(0).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a14'/>");td=$("<td></td>");input.val(c).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a22'/>");td=$("<td></td>");input.val(q1).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a24'/>");td=$("<td></td>");input.val((Math.round((a*q1*q1+b*q1+parseFloat(c))*100)/100)).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a32'/>");td=$("<td></td>");input.val(tomi).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a34'/>");td=$("<td></td>");input.val((Math.round((a*tomi*tomi+b*tomi+parseFloat(c))*100)/100)).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a42'/>");td=$("<td></td>");input.val(q3).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a44'/>");td=$("<td></td>");input.val((Math.round((a2*q3*q3+b2*q3+parseFloat(c2))*100)/100)).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a52'/>");td=$("<td></td>");input.val(endchart).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a54'/>");td=$("<td></td>");input.val((Math.round((a2*endchart*endchart+b2*endchart+parseFloat(c2))*100)/100)).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		table.addClass('relative').appendTo(matrix);
		canvas=$("<canvas class='myCanvas' id='"+myCanvas+"'></canvas>").attr('width','400').attr('height','400');
		canvas.prependTo(matrix);
		matrix.appendTo(column);
		column.appendTo(row);
		$("<tr><td colspan='2' class='tdHeading' >Καμπύλη Αντλίας</td></tr>").insertBefore( closestRow );
		row.insertBefore(closestRow);
	}

	inverter=windowBox.find('.inverter').text();
	if (inverter!= 'Inverter') 
	{
		windowBox.find('#a42').val('').hide();
		windowBox.find('#a44').val('').hide();
		windowBox.find('#a52').val('').hide();
		windowBox.find('#a54').val('').hide();
	}
	else
	{
		windowBox.find('#a42').show();
		windowBox.find('#a44').show();
		windowBox.find('#a52').show();
		windowBox.find('#a54').show();	
	}
	//console.log('myCanvas:'+myCanvas);
    solve(myCanvas);
}
function initializeGraphPumpSt()
{	
	graphTableRef='pumpSt';
	myCanvas="canvasPumpSt";
	windowBox=$('.allWindowBox:visible').find('.windowBox').last();
	q0=(windowBox.find('#q0').val()!='')?windowBox.find('#q0').val():0;
	q1=(windowBox.find('#q1').val()!='')?windowBox.find('#q1').val():0;
	q2=(windowBox.find('#q2').val()!='')?windowBox.find('#q2').val():0;
	a=(windowBox.find('#a').val()!='')?windowBox.find('#a').val():0;
	b=(windowBox.find('#b').val()!='')?windowBox.find('#b').val():0;
	c=(windowBox.find('#c').val()!='')?windowBox.find('#c').val():0;
	if (windowBox.find('#q0').closest('tr').prev('tr').find('.matrix').length<1)
	{
		closestRow=windowBox.find('#q0').closest('tr');
		row=$("<tr></tr>");
		column=$("<td colspan='2'></td>");
		matrix=$("<div class='matrix' id='matrix' style='position:relative;'></div>");
		table=$("<table>");table.addClass('graphValue');
		tr=$("<tr></tr>");
		td=$("<td>Q(l/h)</td>");td.appendTo(tr);
		td=$("<td>H(bar)</td>");td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a12'/>");td=$("<td></td>");input.val(q0).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a14'/>");td=$("<td></td>");input.val(Math.round((a * q0 * q0 + b * q0 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a22'/>");td=$("<td></td>");input.val(q1).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a24'/>");td=$("<td></td>");input.val(Math.round((a * q1 * q1 + b * q1 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a32'/>");td=$("<td></td>");input.val(q2).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a34'/>");td=$("<td></td>");input.val(Math.round((a * q2 * q2 + b * q2 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		table.addClass('relative').appendTo(matrix);
		canvas=$("<canvas class='myCanvas' id='"+myCanvas+"'></canvas>").attr('width','400').attr('height','400');
		canvas.prependTo(matrix);
		matrix.appendTo(column);
		column.appendTo(row);
		$("<tr><td colspan='2' class='tdHeading' >Καμπύλη Αντλιοστασίου</td></tr>").insertBefore( closestRow );
		row.insertBefore(closestRow);
	}
    solve(myCanvas);
}
function initializeGraphPumpStHeater()
{	
	graphTableRef='pumpSt';
	myCanvas="canvasPumpStHeater";
	windowBox=$('.allWindowBox:visible').find('.windowBox').last();
	q0=(windowBox.find('#q0_he').val()!='')?windowBox.find('#q0_he').val():0;
	q1=(windowBox.find('#q1_he').val()!='')?windowBox.find('#q1_he').val():0;
	q2=(windowBox.find('#q2_he').val()!='')?windowBox.find('#q2_he').val():0;
	a=(windowBox.find('#a_he').val()!='')?windowBox.find('#a_he').val():0;
	b=(windowBox.find('#b_he').val()!='')?windowBox.find('#b_he').val():0;
	c=(windowBox.find('#c_he').val()!='')?windowBox.find('#c_he').val():0;
	if (windowBox.find('#q0_he').closest('tr').prev('tr').find('#matrix').length<1)
	{
		closestRow=windowBox.find('#q0_he').closest('tr');
		row=$("<tr></tr>");
		column=$("<td colspan='2'></td>");
		matrix=$("<div class='matrix' id='matrix' style='position:relative;'></div>");
		table=$("<table>");table.addClass('graphValue');
		tr=$("<tr></tr>");
		td=$("<td>Q(l/h)</td>");td.appendTo(tr);
		td=$("<td>H(bar)</td>");td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a12'/>");td=$("<td></td>");input.val(q0).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a14'/>");td=$("<td></td>");input.val(Math.round((a * q0 * q0 + b * q0 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a22'/>");td=$("<td></td>");input.val(q1).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a24'/>");td=$("<td></td>");input.val(Math.round((a * q1 * q1 + b * q1 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a32'/>");td=$("<td></td>");input.val(q2).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a34'/>");td=$("<td></td>");input.val(Math.round((a * q2 * q2 + b * q2 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		table.addClass('relative').appendTo(matrix);
		canvas=$("<canvas class='myCanvas' id='"+myCanvas+"'></canvas>").attr('width','400').attr('height','400');
		canvas.prependTo(matrix);
		matrix.appendTo(column);
		column.appendTo(row);
		$("<tr><td colspan='2' class='tdHeading' >Καμπύλη Εναλλάκτη Αντλιοστασίου</td></tr>").insertBefore( closestRow );
		row.insertBefore(closestRow);
	}
    solve(myCanvas);
}
function initializeGraphAntifreeze()
{
	graphTableRef='antifreeze';
	myCanvas="canvasAntifreeze";
	windowBox=$('.allWindowBox:visible').find('.windowBox').last();
	q0=(windowBox.find('#q0').val()!='')?windowBox.find('#q0').val():0;
	q1=(windowBox.find('#q1').val()!='')?windowBox.find('#q1').val():0;
	q2=(windowBox.find('#q2').val()!='')?windowBox.find('#q2').val():0;
	a=(windowBox.find('#a').val()!='')?windowBox.find('#a').val():0;
	b=(windowBox.find('#b').val()!='')?windowBox.find('#b').val():0;
	c=(windowBox.find('#c').val()!='')?windowBox.find('#c').val():0;
	if (windowBox.find('#q0').closest('tr').prev('tr').find('.matrix').length<1)
	{
		closestRow=windowBox.find('#q0').closest('tr');
		row=$("<tr></tr>");
		column=$("<td colspan='2'></td>");
		matrix=$("<div class='matrix' id='matrix' style='position:relative;'></div>");
		table=$("<table>");table.addClass('graphValue');
		tr=$("<tr></tr>");
		td=$("<td>ANTIFREEZE(°C)</td>");td.appendTo(tr);
		td=$("<td>WATER MIX(%)</td>");td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a12'/>");td=$("<td></td>");input.val(q0).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a14'/>");td=$("<td></td>");input.val(Math.round((a * q0 * q0 + b * q0 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a22'/>");td=$("<td></td>");input.val(q1).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a24'/>");td=$("<td></td>");input.val(Math.round((a * q1 * q1 + b * q1 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		tr=$("<tr></tr>");
		input=$("<input class='input' id='a32'/>");td=$("<td></td>");input.val(q2).appendTo(td);td.appendTo(tr);
		input=$("<input class='input' id='a34'/>");td=$("<td></td>");input.val(Math.round((a * q2 * q2 + b * q2 + parseFloat(c)) * 100) / 100).appendTo(td);
		td.appendTo(tr);tr.appendTo(table);
		table.addClass('relative').appendTo(matrix);
		canvas=$("<canvas class='myCanvas' id='"+myCanvas+"'></canvas>").attr('width','400').attr('height','400');
		canvas.prependTo(matrix);
		matrix.appendTo(column);
		column.appendTo(row);
		$("<tr><td colspan='2' class='tdHeading' >Καμπύλη Αντιψυκτικού</td></tr>").insertBefore( closestRow );
		row.insertBefore(closestRow);
	}
    solve(myCanvas);
}
$('.matrix table tr td input').live('keyup',function (){myCanvas=$(this).closest('.matrix').find('.myCanvas').attr('id');solve(myCanvas);});
