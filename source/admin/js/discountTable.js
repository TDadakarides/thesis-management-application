/////ΕΚΠΤΩΣΕΙΣ////////////////////
////////////////////////////////////
////////////////////////////
$('table.discount .input').live('focusout',function (){ $(this).closest('td').find('.close').trigger('click');	});
$('table.discount .textareaTD').live({
	mouseenter: function ()
	{
		hoverButton=$("<div></div>");
		img=$("<img />");
		img.attr('src','webImages/edit.png');
		hoverButton.addClass("textareaButton").append(img);
		$(this).append(hoverButton);
		hoverButton.fadeIn();
	},
	mouseleave: function ()
	{
		$('.textareaButton').remove();
	}
});
$('table.discount .textareaButton').live('click',function ()
{
	textareaTD=$(this).closest("td.textareaTD");
	textareaTD.addClass('edit');
	valueTextarea=textareaTD.find('.textareaDiv').html();
	$('body,html').css({"min-height":"1200px","position":"relative"});
	discountBlackBox=$("<div></div>");
	dropzonePhoto=$(
		"<div "+
		"data-upload-to-element-id='tempIdTextarea' "+
		"data-upload-to-element-what='img' "+
		"data-upload-accepted-files='.png,.jpg,.bmp' "+
		"data-upload-url='uploadFile.php' "+
		"data-upload-dir='../data_interface/images/' "+
		"data-upload-dirServer='../data_interface/images/' "+
		"data-upload-create-thumbs='true' "+
		"data-upload-maxFilesize='2' "+
		">Επιλογή εικόνας</div>");
		dropzonePhoto.addClass('dropzoneButton').addClass('dropzoneGeneralButton').addClass('hidden');
	dropzoneAtt=$(
		"<div "+
		"data-upload-to-element-id='tempIdTextarea' "+
		"data-upload-to-element-what='href' "+
		"data-upload-accepted-files='.pub, .xlsx, .xls, .csv, .pdf, .doc, .txt, .dxf, .vsd, .dwg, .docx' "+
		"data-upload-url='uploadFile.php' "+
		"data-upload-dir='../data_interface/attachments/' "+
		"data-upload-dirServer='../data_interface/attachments/' "+
		"data-upload-create-thumbs='true' "+
		"data-upload-maxFilesize='5' "+
		">Επιλογή συννημένου</div>"
		);
		dropzoneAtt.addClass('dropzoneButton').addClass('dropzoneGeneralButton').addClass('hidden');
	discountBlackBox.addClass("discountBlackBox");
	$('body').append(discountBlackBox);
	closeButton=$('<div></div');
	rtBox=$("<div></div>");
	rtBox.addClass('rtBox');
	closeButton.text('X').addClass('close');
	textarea=$('<textarea></textarea>');
	textarea.val( valueTextarea ).attr('id','tempIdTextarea').addClass('textarea mceEditor tinymce');
	rtBox.append(textarea);
	discountBlackBox.append(rtBox).append(closeButton).append(dropzonePhoto).append(dropzoneAtt);
	closeButton.fadeIn();
	textarea.fadeIn();
	discountBlackBox.fadeIn(800);
	dropzoneInit( serverHost );
	mediumTextMCE();
});
$('.discountBlackBox').live('mouseup',function (e)
{
	if ( $(e.target).is('.discountBlackBox') )
	$('.discountBlackBox .close').trigger('click');
});
$('.discountBlackBox .close').live('click',function ()
{
	close=$('.discountBlackBox .close');
	textareaTD=$("td.textareaTD.edit");
	textarea=textareaTD.find(".textarea");
	valueTextarea=tinyMCE.activeEditor.getContent();
	textareaTD.find('.textareaDiv').html(valueTextarea);
	discountBlackBox=$('.discountBlackBox');
	close.remove();
	textarea.remove();
	discountBlackBox.remove();				
	$('td.edit').removeClass('edit');
	$('body,html').css({"min-height":"","position":""});
});
$('table.discount .inputTD').live({
	mouseenter: function ()
	{
		hoverButton=$("<div></div>");
		img=$("<img />");
		img.attr('src','webImages/edit.png');
		hoverButton.addClass("inputButton").append(img);
		$(this).append(hoverButton);
		hoverButton.fadeIn();
	},
	mouseleave: function ()
	{
		$('.inputButton').remove();
	}
});
$('table.discount .inputButton').live('click',function ()
{
	log=0;
	inputTD=$(this).closest("td.inputTD");
	valueInput=inputTD.text();
	closeButton=$('<div></div');
	closeButton.text('X').addClass('close');
	input=$('<input />');
	input.val( valueInput ).addClass('input');
	inputTD.append(input).append(closeButton);
	closeButton.fadeIn();
	input.fadeIn(500,function (){ $(this).focus(); } );
});
$('table.discount .inputTD .close').live('click',function ()
{
	close=$(this);
	inputTD=$(this).closest("td.inputTD");
	input=$(this).prev(".input");
	valueInput=input.val();
	inputTD.text(valueInput);
	close.remove();
	input.remove();
});
$('table.discount tr .inputTD,table.discount tr .textareaTD,table.discount tr .color').live(
{
	mouseenter: function ()
	{
		var t = parseInt($(this).index()) + 1;
		$(this).parents('table').find('tr:not(:first-child)').find('td:nth-child(' + t + ')').addClass('highlightedTD');
		$(this).closest('tr').find('td:not(:first-child)').addClass('highlightedTD');
	},
	mouseleave: function ()
	{
		var t = parseInt($(this).index()) + 1;
		$(this).parents('table').find('td:nth-child(' + t + ')').removeClass('highlightedTD');
		$(this).closest('tr').find('td').removeClass('highlightedTD');
	}
});
$('table.discount tr .deleteCust').live(
{
	mouseenter: function() {
		var t = parseInt($(this).closest('td').index()) + 1;
		$(this).parents('table').find('tr:not(:first-child)').find('td:nth-child(' + t + ')').addClass('deleteTD');
	},
	mouseleave: function() {
		var t = parseInt($(this).closest('td').index()) + 1;
		$(this).parents('table').find('td:nth-child(' + t + ')').removeClass('deleteTD');
	}
});
$('table.discount tr .deleteProd').live(
{
	mouseenter: function() {
		var t = parseInt($(this).closest('td').index()) + 1;
		$(this).closest('tr').find('td:not(:first-child)').addClass('deleteTD');
	},
	mouseleave: function() {
		var t = parseInt($(this).closest('td').index()) + 1;
		$(this).closest('tr').find('td').removeClass('deleteTD');
	}
});
newProdNum=0;
newCustNum=0;
newDiscountNum=0;
(function ($) {
   $.fn.liveColpick = function (opts) {
	  this.live("mouseover", function() {
		 if (!$(this).data("init")) {
			bg=( (typeof $(this).css('background-color')!='undefined')?$(this).css('background-color'):'rgb(255,255,255)' );
			bg=rgb2hex(bg) ;
			opts.color=bg;
			$(this).data("init", true).colpick(opts);
		 }
	  });
	  return this;
   };
}(jQuery));
	$('.colorShow.colorProd,.colorPicker').liveColpick(
	{
		colorScheme:'dark',
		flat:false,
		layout:'hex',
		onSubmit:function(hsb,hex,rgb,el)
		{
			//$(el).closest('td div').text('#'+hex);
			$(el).css('background-color', '#'+hex);
			$(el).colpickHide();
		}
	});

$('table.discount .addProd').live('click',function ()
{
	cloneTr=$(this).closest('tr').prevAll('tr').first().clone();
	cloneTr.find('.inputTD').text('0');
	cloneTr.find('.colorShow').css('background-color','#ffffff');
	cloneTr.find('.colorShow').attr('id','color_new_'+newProdNum);
	cloneTr.find('.discountCustProd').each(function (){ $(this).attr('id','new_'+newDiscountNum);newDiscountNum++;});
	cloneTr.insertBefore( $(this).closest('tr') );
	newProdNum++;
});
$('table.discount .addCust').live('click',function ()
{
	$('.discount td:last-child').each(function ()
	{
		cloneTd=$(this).prevAll('td').first().clone();
		if (cloneTd.hasClass('inputTD')) cloneTd.text('0');
		cloneTd.find('.textareaDiv').html('').attr('id','terms_new_'+newCustNum);
		if ( cloneTd.hasClass('discountCustProd') ) { cloneTd.attr('id','new_'+newDiscountNum);newDiscountNum++;}
		cloneTd.insertBefore( $(this) );					
	});
	newCustNum++;
});
//////////////	ΔΙΑΓΡΑΦΗ ΔΕΔΟΜΕΝΩΝ /////////////////
$('table.discount .deleteCust').live('click',function ()
{
	ok = confirm("Είστε σίγουροι ότι θέλετε να διαγράψετε την κατηγορία πελάτη;");
	if (ok == true)
	{
		if ( $('form').length>0 ) $('form').remove();
		thesi = parseInt($(this).closest('td').index()) + 1;
		discCustId=( $(this).parents('table').find('td.textareaTD:nth-child(' + thesi + ')').find('.termsCust').attr('id') ).replace("terms_","");
		if ( discCustId.indexOf("new")<0 )
		{
			form=$("<form method='post' name='deleteForm' id='deleteForm' action='deleteDiscount.php' ></form>");
			form.append( $("<input name='discCustId'  value='"+discCustId+"' />") ) ;
			form.submit();
			$('body').append(form);
		}
		else
		{
			$(this).parents('table').find('td:nth-child(' + thesi + ')').remove();
		}
	}
});
//////////////	ΔΙΑΓΡΑΦΗ ΔΕΔΟΜΕΝΩΝ /////////////////
$('table.discount .deleteProd').live('click',function ()
{	
	ok = confirm("Είστε σίγουροι ότι θέλετε να διαγράψετε την κατηγορία προϊόντος;");
	if (ok == true)
	{
		if ( $('form').length>0 ) $('form').remove();
		discIdProd=($(this).closest('tr').find('.colorProd').attr('id')).replace("color_","");
		if ( discIdProd.indexOf("new")<0 )
		{
			form=$("<form method='post' name='deleteForm' id='deleteForm' action='deleteDiscount.php' ></form>");
			form.append( $("<input name='discIdProd'  value='"+discIdProd+"' />") ) ;
			form.submit();
			$('body').append(form);			
		}
		else
		{					
			$(this).closest('tr').remove();
		}
	}
});

//////////////	ΑΠΟΘΗΚΕΥΣΗ ΔΕΔΟΜΕΝΩΝ /////////////////
function updateDiscountPostData()
{
	$('.inputTD .close, .discountBlackBox .close').each( function () { $(this).trigger('click'); });
	postData={};
	$('tr.discount').each(function ()
	{
		discIdProd=($(this).find('.colorProd').attr('id')).replace("color_","");
		codeProd=$(this).find('.codeProd ').text();
		title_elProd=$(this).find('.title_elProd ').text();
		colorProd=rgb2hex($(this).find('.colorProd ').css('background-color'));
		
		postData[ "discIdProd["+discIdProd+"]" ]=discIdProd;
		postData[ "codeProd["+discIdProd+"]" ]=codeProd;
		postData[ "title_elProd["+discIdProd+"]" ]=title_elProd;
		postData[ "colorProd["+discIdProd+"]" ]=colorProd;
		//form.append( $("<input name='discIdProd["+discIdProd+"]'  value='"+discIdProd+"' />") ) ;
		//form.append( $("<input name='codeProd["+discIdProd+"]'  value='"+codeProd+"' />") ) ;
		//form.append( $("<input name='title_elProd["+discIdProd+"]'  value='"+title_elProd+"' />") ) ;
		//form.append( $("<input name='colorProd["+discIdProd+"]'  value='"+colorProd+"' />") ) ;
		//form.append( $("<br/>") ) ;
	});				
		//form.append( $("<hr/>") ) ;
	$('tr:first-child td.discountCustom').each(function ()
	{
		var thesi = parseInt($(this).index()) + 1;
		codeCust=$(this).parents('table').find('td.codeCust:nth-child(' + thesi + ')').text();
		title_Cust=$(this).parents('table').find('td.title_Cust:nth-child(' + thesi + ')').text();
		min_order_priceCust=$(this).parents('table').find('td.min_order_priceCust:nth-child(' + thesi + ')').text();
		delivery_chargesCust=$(this).parents('table').find('td.delivery_chargesCust:nth-child(' + thesi + ')').text();
		termsCust=$(this).parents('table').find('td.textareaTD:nth-child(' + thesi + ')').find('.termsCust').html();
		discIdCust=( $(this).parents('table').find('td.textareaTD:nth-child(' + thesi + ')').find('.termsCust').attr('id') ).replace("terms_","");	
		
		postData[ "discIdCust["+discIdCust+"]" ]=discIdCust;
		postData[ "codeCust["+discIdCust+"]" ]=codeCust;
		postData[ "title_Cust["+discIdCust+"]" ]=title_Cust;
		postData[ "min_order_priceCust["+discIdCust+"]" ]=min_order_priceCust;
		postData[ "delivery_chargesCust["+discIdCust+"]" ]=delivery_chargesCust;
		postData[ "termsCust["+discIdCust+"]" ]=termsCust;
		
		//form.append( $("<input name='discIdCust["+discIdCust+"]'  value='"+discIdCust+"' />") ) ;
		//form.append( $("<input name='codeCust["+discIdCust+"]'  value='"+codeCust+"' />") ) ;
		//form.append( $("<input name='title_Cust["+discIdCust+"]'  value='"+title_Cust+"' />") ) ;
		//form.append( $("<input name='min_order_priceCust["+discIdCust+"]'  value='"+min_order_priceCust+"' />") ) ;
		//form.append( $("<input name='delivery_chargesCust["+discIdCust+"]'  value='"+delivery_chargesCust+"' />") ) ;
		//form.append( $("<textarea name='termsCust["+discIdCust+"]'  >"+termsCust+"</textarea>") ) ;
		//form.append( $("<br/>") ) ;
	});				
		//form.append( $("<hr/>") ) ;
	$('tr.discount').each(function ()
	{
		discountTr=$(this);
		$('tr:first-child td.discountCustom').each(function ()
		{						
			var thesi = parseInt($(this).index()) + 1;
			discountCustProdId=( discountTr.find('td.discountCustProd:nth-child(' + thesi + ')').attr('id') ).replace("code_","");
			discIdCustCombine=( $(this).parents('table').find('td.textareaTD:nth-child(' + thesi + ')').find('.termsCust').attr('id') ).replace("terms_","");
			discIdProdCombine=( discountTr.find('.colorProd').attr('id')).replace("color_","");
			discountCustProd=discountTr.find('td.discountCustProd:nth-child(' + thesi + ')').text();
						
			postData[ "discountCustProdId["+discountCustProdId+"]" ]=discountCustProdId;			
			postData[ "discIdCustCombine["+discountCustProdId+"]" ]=discIdCustCombine;			
			postData[ "discIdProdCombine["+discountCustProdId+"]" ]=discIdProdCombine;			
			postData[ "discountCustProd["+discountCustProdId+"]" ]=discountCustProd;			
			//form.append( $("<input name='discountCustProdId["+discountCustProdId+"]'  value='"+discountCustProdId+"' />") ) ;
			//form.append( $("<input name='discIdCustCombine["+discountCustProdId+"]'  value='"+discIdCustCombine+"' />") ) ;
			//form.append( $("<input name='discIdProdCombine["+discountCustProdId+"]'  value='"+discIdProdCombine+"' />") ) ;
			//form.append( $("<input name='discountCustProd["+discountCustProdId+"]'  value='"+discountCustProd+"' />") ) ;
			//form.append( $("<br/>") ) ;
		});
	});
	//console.log( 'fun updateDiscountPostData postData'+postData );
	return postData;
	//form.submit();
	//$('body').append(form);

}
$('#update').live('click',function ()
{

});

