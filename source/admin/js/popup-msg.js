function popupMsgDeclaration()
{
	$(".title-help").css("cursor","pointer");
	popup=0;
	$(".title-help").live('mouseover',function ()
	{
		helpIcon=$(this);
		_text=$(this).attr("data-title");
		_height=parseInt($(this).outerHeight());
		_width=parseInt($(this).outerWidth());
		thisEl=$(this);
		var offset = $(this).offset();
		var _vertical="left";
		popup=setTimeout(function ()
		{
			if ($(".popup-msg").length>0)
			{
				$('body').remove(".popup-msg");
			}
			popupMsg=$("<div></div>");
			popupMsg.addClass("popup-msg")
			.html(_text )
			.appendTo('body');
			if ( thisEl.attr('data-title-align')=='right' ) {_left=offset.left-popupMsg.width()-10+_width;}else{_left=offset.left+40;}
			_top=parseFloat(offset.top)+_height+2;
			popupMsg.css({ "left":_left, "top":_top});
			popupMsg.fadeIn(400);
		},500);
	});
	$(".title-help").live('mouseout',function ()
	{
		clearTimeout(popup);
		if ($(".popup-msg").length>0)
		{
			$(".popup-msg").remove();
		}
	});
}