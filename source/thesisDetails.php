<!DOCTYPE html>
<html>
<?php 
	include "header.php";
	include "admin/database_include.php";

	$siteroot=(!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' .$_SERVER['HTTP_HOST'];

	$thesisid =  isset($_GET["thesisid"]) ? $_GET["thesisid"] : "";

    $thesis_details= ""
        ."SELECT t.id,
            t.title,
            t.intro,
            t.description,
            t.abstractEL,
            t.abstractEN,
            t.category,
            c.title as categoryTitle,
            t.professor,
            u.username as professorName,
            t.minstudent,
            t.maxstudent,
            DATE_FORMAT(t.datePublished, '%Y-%m-%d') as datePublished,
            DATE_FORMAT(t.dateStart, '%Y-%m-%d') as dateStart,
            DATE_FORMAT(t.datePresentation, '%Y-%m-%d') as datePresentation,
            DATE_FORMAT(t.dateDelivery, '%Y-%m-%d') as dateDelivery,
            t.has_congres,
            t.congres_title,
            DATE_FORMAT(t.dateCreate, '%Y-%m-%d') as dateCreate,
            DATE_FORMAT(t.dateUpdate, '%Y-%m-%d') as dateUpdate,
            t.isActive,
            t.isCompleted,
            t.contactPerson,
            t.thesisDbReference,
            t.assignedTo,
            pi.path as filePath,
            pi.userid as fileUserId
            FROM thesis t 
            left join category c on c.id=t.category
            left join users u on u.id=t.professor
            left join pictureinfo pi on pi.id=t.attachmentid
            where t.id ='{$thesisid}' and t.isActive=1" ;

    $thesis_images=""
    	."SELECT 	p.id,
		pi.path
		FROM pictureinfo pi
		INNER JOIN pictures p on pi.id = p.picture
		INNER JOIN thesis t on p.itemId=t.id
		WHERE p.tableName='thesis' and t.id='{$thesisid}'
		Order By p.first DESC";

	$thesis_images_results=mysql_query($thesis_images);
    $thesis_details_result = mysql_query($thesis_details);
?>
<body>
	<?php
		include "mainMenu.php";
	?>
	<img class="background" src="/uploads/images/front_Bg.jpg">
	<div class="center-region">
		<?php 
			while($row = mysql_fetch_assoc($thesis_details_result)){
				
				echo ""
				. "<header class='masthead'>
		            <div class='overlay'></div>
		            <div class='container'>
		              <div class='row'>
		                <div class='col-lg-8 col-md-10 mx-auto'>
		                  <div class='post-heading'>
		                    <h1>".$row['title']."</h1>
		                    <h2 class='subheading'>".$row['intro']."</h2>
		                    <ul class='list-group list-group-flush'>
		                          <li class='list-group-item thesis-info-item' ><span class='meta'> Lead Professor: <b>".$row['professorName']."</b></span></li>";
	                        if (!empty($row['minstudent'])) {
	                        	echo "<li class='list-group-item thesis-info-item' ><span class='meta'> Min Number Of Students: <b>".$row['minstudent']."</b></span></li>";
	                        }
	                        if (!empty($row['maxstudent'])) {
	                        	echo "<li class='list-group-item thesis-info-item' ><span class='meta'> Max Number Of Students: <b>".$row['maxstudent']."</b></span></li>";
	                        }
	                        if ($row['datePublished']) {
	                        echo  "<li class='list-group-item thesis-info-item' ><span class='meta'> Date Of Publish: <b>".$row['datePublished']."</b></span></li>";	
	                        }
	                        if (!empty($row['dateStart']) && !empty($row['assignedTo']) ) {
	                        	echo  "<li class='list-group-item thesis-info-item' ><span class='meta'> Start Date: <b>".$row['dateStart']."</b></span></li>";	
	                        	echo  "<li class='list-group-item thesis-info-item' ><span class='meta'> Assigned To: <b>".$row['assignedTo']."</b></span></li>";	
	                        }
	                          	if (!empty($row['contactPerson'])) {
		                          	echo "<li class='list-group-item thesis-info-item' ><span class='meta'> Contact Person: 
										<b>
		                          			<a href='mailto:".$row['contactPerson']."' target='_top'>".$row['contactPerson']."</a>
		                          		</b>
		                          	</span>
		                          	</li>";
		                          }
		                          if ($row['has_congres'] ==1) {
		                          	echo "<li class='list-group-item thesis-info-item' ><span class='meta'> Desired Link With Publication: <b>".$row['congres_title']."</b></span></li>";
		                          }
		                          if ($row['isCompleted'] ==1) {
		                          	echo "<li class='list-group-item thesis-info-item' ><span class='meta'><b> This Thessis is Completed </b></span></li>";
		                          	if (!empty($row['thesisDbReference'])) {
		                          		echo "<li class='list-group-item thesis-info-item' ><span class='meta'> Completed Thesis Link: 
										<b>
		                          			<a href='//".$row['thesisDbReference']."' target='_blank'>Click Here...</a>
		                          		</b>
										</span>
		                          	</li>";
		                          	}
		                          }
	                echo    "</ul>
		                  </div>";
	                if (mysql_num_rows($thesis_images_results)>1) {
            			echo"<div id='thesis-image-gallery' class='owl-carousel owl-theme'>";

	                	while($imgrow = mysql_fetch_assoc($thesis_images_results)){
	                		echo "<div class='image-wrapper'>"
	                			."<img src=".$siteroot."/uploads/images/".$imgrow['path']."  "."data-image-id=".$imgrow['id'].">"
	                		."</div>";
	                	}
	                  echo "</div>";
	                }
	                else
	                {
                		echo"<div id='thesis-image'>";

                		while($imgrow = mysql_fetch_assoc($thesis_images_results)){
	                		echo "<img src=".$siteroot."/uploads/images/".$imgrow['path']."  "."data-image-id=".$imgrow['id'].">";
                		}
                		echo "</div>";
                	}
		        echo  "</div>
		              </div>
		            </div>
		          </header>"
		          .""
		          ."<article class='thesis-description'>
				      <div class='container'>
				        <div class='row'>
				          <div class='col-lg-8 col-md-10 mx-auto'>";
				          if (!empty($row['abstractEN']) || !empty($row['abstractEL'])) {
				          	echo "<div class='dropdown'>
							  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							    Thesis Abstract
							  </button>
							  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
							    <a data-fancybox data-src='#abstract-en' href='javascript:;' class='dropdown-item'>Abstract EN</a >
							    <a data-fancybox data-src='#abstract-el' href='javascript:;' class='dropdown-item'>Abstract EL</a >
							  </div>
							</div>";
				          }
				           if (!empty($row['filePath']) && !empty($row['fileUserId'])) {
				           	echo "<p>
			           		<a  href='{$siteroot}/attachments/{$row['fileUserId']}/{$row['filePath']}'>Thesis Attachments</a>
				           	</p>";
				           }


			         echo 	"<h3>Description</h3>
				                ".$row['description']."
				          </div>
				        </div>
				      </div>
				<article>
				<div style='display: none;' id='abstract-en' class='animated-modal'>
		          <h2>Abstract</h2>
		          ".$row['abstractEN']."
		        </div>
		        <div style='display: none;' id='abstract-el' class='animated-modal'>
		          <h2>Abstract</h2>
		          ".$row['abstractEL']."
		        </div>
				";
			}
		?>
	</div>
</body>
</html>