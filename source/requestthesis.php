<!DOCTYPE html>
<html>
<?php
	include "header.php";
	include "admin/database_include.php";
	$message="";
	$thesis_title="";
	
	$display="block";	
	$indexRow=0;
	$thesis_sql= "SELECT id,title FROM thesis where isActive=1";
	$thesis_professor_mail= "";
	$thesis_professor_name= "";
	$thesis_results =mysql_query($thesis_sql);

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		$student_sql = "SELECT id FROM students WHERE code=" ."{$_POST["student-code"]}";
		$student_result = mysql_query($student_sql);
		$student_counter = mysql_num_rows($student_result);
		$result = mysql_fetch_array($student_result);
		$student_id = $result["id"];

		$thesis_id= $_POST["thesis-id"];
		$professor_info_sql="SELECT u.username, u.email, t.title From thesis t 
								INNER JOIN users u ON t.professor=u.id
								WHERE t.id= "."{$thesis_id}";

		$professor_info_query= mysql_query($professor_info_sql);
		$professor_info_results= mysql_fetch_array($professor_info_query);
		$professor_name=$professor_info_results["username"];
		$professor_email=$professor_info_results["email"];
		$thesis_title= $professor_info_results["title"];
		$student_name= $_POST["student-name"];
		$student_surname= $_POST["student-surname"];
		$student_email= $_POST["student-email"];
		$student_notes= $_POST["student-notes"];
		$student_department= $_POST["student-department"];
		$student_code =$_POST["student-code"];

		if ($student_counter > 0) {

			$sql="INSERT INTO student_comments(thesisId, studentId, comment, dateCreate, dateUpdate, isActive) 
			VALUES ('{$thesis_id}','{$student_id}','{$student_notes}',CURDATE(),CURDATE(),true)";
			mysql_query($sql);
			$display="none";
			include "mailSender.php";
			$message = "Thank you for your message!";
		} else {

			$student_insert ="INSERT INTO students(name, surname, department, code, email, dateCreate, dateUpdate, isActive) 
			VALUES ('{$student_name}','{$student_surname}','{$student_department}','{$student_code}','{$student_email}',CURDATE(),CURDATE(),true)";
			mysql_query($student_insert);

			$student_sql = "SELECT id FROM students WHERE code=" ."{$student_code}";
			$student_result = mysql_query($student_sql);
			$student_counter = mysql_num_rows($student_result);
			$result = mysql_fetch_array($student_result);
			$student_id = $result["id"];

			$sql="INSERT INTO student_comments(thesisId, studentId, comment, dateCreate, dateUpdate, isActive) 
			VALUES ('{$thesis_id}','{$student_id}','{$student_notes}',CURDATE(),CURDATE(),true)";
			mysql_query($sql);
			$display="none";
			include "mailSender.php";
			$message = "Thank you for your message!";
		}
	}
?>
<body>
	<img class="background" src="/uploads/images/front_Bg.jpg">
	<?php
		include "mainMenu.php";
	?>
	<div class="center-region">
			<h2 class="success-form"> <?=$message;?> </h2>
		<div class="col-sm-6 offset-sm-3" style="display: <?php echo $display;?>">
			<h1 class="h1">Send your interest for thesis</h1>
			<form method="post" class="needs-validation" novalidate action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
					<div class="form-group">
						<label class="thesis-title">Thesis</label>

						<?php
							echo '<select class="form-control" name="thesis-id" required>';
							echo '<option index="0" value="" >Select Thesis</option>';
							while($row = mysql_fetch_assoc($thesis_results)){
								$indexRow++;
								echo "<option index=".$indexRow." value=".$row['id'].">".$row['title']."</option>";
							}
							echo "</select>";
						?>
						 <div class="invalid-feedback">Please Select Thesis</div>
					</div>
					<div class="form-group">
						<label class="student-name">Your Name</label>
						<input class="form-control" type="text" name="student-name" id="validationCustom02" value="" autocomplete="off" required>
						<div class="invalid-feedback">
					        Please Enter Your Name.
				      	</div>
					</div>
					<div class="form-group">
						<label class="student-surname">Your Surname</label>
						<input class="form-control" type="text" id="validationCustom03" name="student-surname" value="" autocomplete="off" required>
						<div class="invalid-feedback">
					        Please Enter Your Surname.
				      	</div>
					</div>
					<div class="form-group">
						<label class="student-code">Your Registry Number</label>
						<input class="form-control" type="text" name="student-code" id="validationCustom04" value="" autocomplete="off" required>
						<div class="invalid-feedback">
					        Please Enter Your Registry Number.
				      	</div>
					</div>
					<div class="form-group">
						<label class="student-department">Your Department</label>
						<input class="form-control" type="text" name="student-department" id="validationCustom05" value="" autocomplete="off" required>
						<div class="invalid-feedback">
					        Please Enter Your Department.
				      	</div>
					</div>
					<div class="form-group">
						<label class="student-email">Your Email</label>
						<input class="form-control" type="text" name="student-email" id="validationCustom06" value="" autocomplete="off" required>
						<div class="invalid-feedback">
					        Please Enter Your Registry Email.
				      	</div>
					</div>
					<div class="form-group">
						<label class="student-notes">Your Notes</label>
						<textarea class="form-control" id="validationCustom06" name="student-notes" required></textarea>
						<div class="invalid-feedback">
					        Please Enter Your Notes.
				      	</div>
					</div>
					<p><i>*all fields are required</i></p>
				<div class="form-group">
					<button class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
	
</body>
</html>