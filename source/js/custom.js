(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function ConvertFormToJSON(form){
  var array = jQuery(form).serializeArray();
  var json = {};
    jQuery.each(array, function() {    
      if(typeof json[this.name] == 'undefined') {
        json[this.name] = this.value || '';
      }
      else {
      json[this.name] += "," + this.value ;
      }
  });
  return json
}

$(document).ready(function () {
  $(".test").hide();

  $(".f-search").on("click", function (event) {
      event.preventDefault();
      var myData=$(".first-step-search");
      $.ajax({
      type: "POST",
      url: "thesisresults.php",
      data: ConvertFormToJSON(myData),
      success: function (data) {
         $(".test").show();
         $(".first-step-search").hide();
         $('.test').html(data);
      }
    });
  });

  $('#thesis-image-gallery').owlCarousel({
    items:1,
    loop:false,
    autoWidth:true, 
    autoHeight:true
  })
});
