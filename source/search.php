<!DOCTYPE html>
<html>
<?php
	include "header.php";
	include "admin/database_include.php";
	$search =  isset($_GET["search"]) ? $_GET["search"] : "";
	
	$keywords_sql = "SELECT id, keyword FROM keywords";
	$keywords_result= mysql_query($keywords_sql);

	$category_sql= "SELECT * FROM category WHERE isActive=1";
	$category_result= mysql_query($category_sql);	

	$sql_fields= ""
			."SELECT DISTINCT	t.*
					FROM thesis t
					INNER JOIN keywords_thesis kt ON t.id = kt.thesisId
					INNER JOIN keywords k ON k.id = kt.keyword
					INNER JOIN category c ON c.id = t.category" ;

?>
<body>
	<?php
		include "mainMenu.php";
	?>
	<img class="background" src="/uploads/images/front_Bg.jpg">
	<div class="center-region">
		
		<h1 class="h1">Search Page</h1>
		<h2 class="h2">Find your subject</h2>
		
		<div class="search-page">
			<form class="first-step-search" method="post" action="/thesisresults.php">
				<p><i>*if you don't select any field, you will see all availabe theses</i></p>
				<div class="row justify-content-md-center">
					<div class="col-sm">
						<span class="search-title">Choose a Category</span>
						<?php
						while($row = mysql_fetch_assoc($category_result)){
			  			echo '<div class="checkbox">';
							  echo "<label><input type='checkbox' class='my-checkbox' name='category-ids' value='".$row['id']."'>".$row['title']."</label>";
			  			echo '</div>';
							}
						?>
					</div>
					<div class="col-sm">
						<span class="search-title" >Choose a Keyword</span>
						<?php
						while($row = mysql_fetch_assoc($keywords_result)){
			  			echo '<div class="checkbox">';
							  echo "<label><input type='checkbox' class='my-checkbox' name='keyword-ids' value='".$row['id']."'>".$row['keyword']."</label>";
			  			echo '</div>';
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<button class="btn btn-primary f-search">Search</button>
				</div>
			</form>

			<div class=test>
				
			</div>
		</div>
	</div>
</body>
</html>